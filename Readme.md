## Question-specific, Evidence-based medicine text summariser

This summariser is an implementation of the summarisation system described in:

Sarker A, Molla D, Paris C. Query-oriented evidence extraction to support evidence-based medicine practice. J Biomed Inform. 2016 Feb; 59:169-84. doi: 10.1016/j.jbi.2015.11.010. Epub 2015 Dec 2.
PMID: 26631762

Prerequisites:
The summariser uses UMLS associations. As such, some setup is required before running the summariser.

###Instructions:


1. In src/Resources/, Place a folder named 'External'.
1. In src/Resources/External/, download and place the files: RELDEF.txt, SRDEF.txt, SRSTRE2.txt. This files can be downloaded from the UMLS website.

##Inputs

1. A question file (sample file provided: s1853sample)
    1. First line should contains the question in plain text
    1. Second line contains the 'Question Type'. Each Type must be tab separated. Twelve types in total are possible. We will attempt to provide a classifier later on.
    1. The third line contains the UMLS semantic types. MetaMap can be used to generate them. If you don't have the semantic types, leave the line empty (i.e., newline character).

1. A 'Tagged Abstract' file (sample file provided: 11732129abssample). Each line of the tagged abstract file contains a sentence. 
    * The format is: sentence_number || sentence || [list of semantic types] || sentence PIBOSO classification
    * A classifier is required to perform the PIBOSO classification. Classifiers are available online.
    * MetaMap can be used to identify all the semantic types. If you don't have MetaMap, just place || [  ] || (i.e., a blank list). 

##Run commmand:

`python runSummarisationOnFile.py question_filename taggedabstract_filename [summary_output_folder]
`

Example: 

`python runSummarisationOnFile.py s1853sample.txt 11732129abssample.txt
`

##Personal Notes

The summariser was developed in partial fulfillment of my PhD project. The code still requires refactoring. I occasionally receive requests for help and suggestions via my email: abeed@pennmedicine.upenn.edu. Please give me time to respond (especially if your query is complicated).