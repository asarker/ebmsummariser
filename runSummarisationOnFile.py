
from __future__ import division
import string
from src import mainSummarisation
import sys

def parse_question_file(filename):
    '''
    Parses the question file provided by the user in the required format
    :param filename:
    :return:
    '''
    infile = open(filename)
    question = string.strip(infile.readline())
    qtypes = string.strip(infile.readline()).split('\t')
    qsemtypes = string.strip(infile.readline()).split('\t')
    return question, qtypes,qsemtypes

def read_and_set_tagged_abstract_file(tagged_abstract_file):
    infile = open(tagged_abstract_file)
    outfile = open('./src/tagged_abstract_sents/'+ tagged_abstract_file,'w')
    for line in infile:
        outfile.write(line)
    outfile.close()

if __name__ == '__main__':

    outputfoldername='summaryoutputs'
    question_file = sys.argv[1]
    tagged_abstract_file  = sys.argv[2]
    if len(sys.argv)>3:
        outputfoldername = sys.argv[3]
    #parse the question file to get all the necessary inforamtion
    q,qtypes,qsts = parse_question_file(question_file)
    read_and_set_tagged_abstract_file(tagged_abstract_file)

    r = mainSummarisation.mainSummarisation()
    r.loadQuestions()
    r.loadLongs()

    print 'Running summarisation main...'
    weights = [0.8, 0.5, 0.3, 0.2, 1.0, 0.6, 1.0, 1.0]

    r.runEBMSummariser(weights,tagged_abstract_file[:-4], q,qtypes,qsts,outputfoldername)

    print 'finished with summarisation..'