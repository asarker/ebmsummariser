'''
Created on May 24, 2012

@author: abeedsarker
'''

import SemanticType
import SemtypeRelation
import AllRelations
import AllSemanticTypes
import SemtypeAssociation
import string


class AllAssociations:
    '''
    classdocs
    '''
    associationslist= []


    def __init__(self):
        '''
        Constructor
        '''
        self.associationslist = []
        semtypeslist = AllSemanticTypes.AllSemanticTypes()
        relationslist = AllRelations.AllRelations()
        self.loadAllAssociations(semtypeslist,relationslist)
    
    def loadAllAssociations(self,semtypeslist,relationslist):
        infile = open('src/Resources/External/SRSTRE2.txt')
        for line in infile:
            assocs= line.split('| ')
            for assoc in assocs:
                elems = assoc.split('|')
                st1 = string.strip(elems[0])
                st2 = string.strip(elems[2])
                rel = string.strip(elems[1])
                
                semtype1 = semtypeslist.getSemanticTypeFromName(st1)
                semtype2 = semtypeslist.getSemanticTypeFromName(st2)
                relation = relationslist.getRelationFromType(rel)
                assoc = SemtypeAssociation.SemtypeAssociation(semtype1,semtype2,relation)
                self.associationslist.append(assoc)
    
    def display(self):
        for item in self.associationslist:
            item.semtype1.display()
            item.semtype2.display()
            item.relation.display()
            
    def displayShort(self):
        for item in self.associationslist:
            print item.semtype1.shortform + '  ' + item.relation.type + '  ' + item.semtype2.shortform 
            
    'Given two semantic type shortforms S1 and S2, returns the relation(s) (type)  between S1 and S2'
    def getRelation(self, s1, s2):
        rels = []
        for item in self.associationslist:
            s1shortform= item.semtype1.shortform
            s2shortform= item.semtype2.shortform
            if cmp(s1,item.semtype1.shortform)==0 and cmp(s2,item.semtype2.shortform)==0:
                #print item.semtype1.shortform
                #print item.semtype2.shortform
                rels.append(item.relation.type)
                #return item.relation.type
        return rels
        