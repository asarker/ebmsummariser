'''
Created on May 24, 2012

@author: abeedsarker
'''

import string
import SemtypeRelation

class AllRelations:
    '''
    classdocs
    '''
    relationslist = []

    def __init__(self):
        '''
        Constructor
        '''
        self.relationslist = []
        self.loadAllRelations()
    
    def loadAllRelations(self):
        infile = open('src/Resources/External/RELDEF.txt')
        for line in infile:
            tokens= line.split('RL|')
            #print len(tokens)
            for tk in tokens:
                items = tk.split('|')
                if len(items)>1:
                    id = string.strip(items[0])
                    type = string.strip(items[1])
                    idnum = string.strip(items[2])
                    desc = string.strip(items[3])
                    reverse = string.strip(items[-2])
                    self.relationslist.append(SemtypeRelation.SemtypeRelation(type,reverse,id,desc,idnum))
    
    def display(self):
        for item in self.relationslist:
            item.display()
    
    def getRelationFromType(self,type):
        for rel in self.relationslist:
            if cmp(type,rel.type)==0:
                return rel         