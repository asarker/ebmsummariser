'''
Created on May 24, 2012

@author: abeedsarker
'''
import string
import SemanticType

class AllSemanticTypes:
    '''
    classdocs
    '''
    semtypelist = []


    def __init__(self):
        '''
        Constructor
        '''
        self.semtypelist = []
        self.loadAllSemTypes()
        
    def loadAllSemTypes(self):
        #load the semantic types
        infile = open('src/Resources/External/SRDEF.txt')
        for line in infile:
            tokens = line.split('STY|')
            for tk in tokens:
                items =  tk.split('|')
                #print items 
                if len(items)>1:
                    id = string.strip(items[0])
                    name = string.strip(items[1])
                    idnum = string.strip(items[2])
                    desc = string.strip(items[3])
                    shortform = string.strip(items[-3])
                    self.semtypelist.append(SemanticType.SemanticType(shortform,id,name,desc,idnum))
                    #print shortform
        
        infile.close() 
    
    def display(self):
        for item in self.semtypelist:
            item.display()      
    
    '''
    takes the name of the semantic type and returns the semantic type object
    e.g. given Acquired Abnormality, returns the whole object
    '''
    def getSemanticTypeFromName(self, name):
        for semtype in self.semtypelist:
            if cmp(semtype.name,name)==0:
                return semtype   