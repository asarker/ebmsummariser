'''
Created on May 24, 2012

@author: abeedsarker
'''

import SemanticType
import SemtypeRelation

class SemtypeAssociation():
    semtype1=SemanticType.SemanticType()
    semtype2=SemanticType.SemanticType()
    relation = SemtypeRelation.SemtypeRelation()

    def __init__(self,s1,s2,rel):

        '''
        Constructor
        '''
        self.semtype1 = s1
        self.semtype2 = s2
        self.relation = rel
    
    def display(self):
        self.semtype1.display()
        self.semtype2.display()
        self.relation.display() 
        