'''
Created on May 24, 2012

@author: abeedsarker
'''

class SemtypeRelation:
    '''
    classdocs
    '''
    id = ''
    type = '' 
    desc = ''
    idnum = ''
    reverse = ''
    bidirectional = False
    
    def __init__(self,type='',reverse='',id='',desc='',idnum=''):
        '''
        Constructor
        '''
        self.id = id
        self.type = type
        self.desc = desc
        self.reverse = reverse
        self.idnum = idnum
        if cmp(type,reverse)==0:
            self.bidirectional = True
        else:
            self.bidirectional = False
            
    def display(self):
        print 'ID: ' + self.id
        print 'Type: ' + self.type 
        print 'Reverse Relationship: ' + self.reverse
        print 'Description: ' + self.desc
        print 'ID NUM: ' + self.idnum
        print 'Is Bidirectional?: ' + str(self.bidirectional)
        print '\n'
        
        