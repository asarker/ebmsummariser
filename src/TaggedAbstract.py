'''
Created on 03/08/2011

@author: asarker
'''
from __future__ import division
import TaggedSentence
import distributionFunctions

class TaggedAbstract(object):
    '''
    Class to hold a taggedabstract

    '''
    id = ""
    mSentences = []
    
    def __init__(self,pFilename):
        self.id = ""
        self.mSentences = []

        vInfile = open('src/tagged_abstract_sents/'+pFilename+'.txt')
        self.id = pFilename[:-4]
        for line in vInfile:
             vSent = TaggedSentence.TaggedSentence(line)
             self.mSentences.append(vSent)
        #print 'issue..'

    def display(self): 
        print 'ID: ' + self.id
        for s in self.mSentences:
            s.display()

    def summariseAbstractQuestionSpecific(self, recordid, qstypes, assocdict, allassocs, all_type_train_fds, normalisedsemtypefd, qType, pAbs, pWeights, pQuestion, fdfs, fdms, fdls, allpibfreq, bestpibfreq, firstpibfreq, lastpibfreq, midpibfreq, longtext):
        '''
        calls distributionFunctions to perform the summarization..
        :param recordid:
        :param qstypes:
        :param assocdict:
        :param allassocs:
        :param all_type_train_fds:
        :param normalisedsemtypefd:
        :param qType:
        :param pAbs:
        :param pWeights:
        :param pQuestion:
        :param fdfs:
        :param fdms:
        :param fdls:
        :param allpibfreq:
        :param bestpibfreq:
        :param firstpibfreq:
        :param lastpibfreq:
        :param midpibfreq:
        :param longtext:
        :return:
        '''
        print pAbs
        return distributionFunctions.summariseAbstractQuestionSpecific(recordid, qstypes, assocdict, allassocs, all_type_train_fds, normalisedsemtypefd, qType, 'src/tagged_abstract_sents/' + pAbs + '.txt', pWeights, pQuestion, fdfs, fdms, fdls, allpibfreq, bestpibfreq, firstpibfreq, lastpibfreq, midpibfreq, longtext)
    
    ##OTHER USABLE FUNCTIONS BELOW:
    
    def getDocLength(self):
        return len(self.mSentences)
    
    def sortListBasedOnSentenceLength(self, pList):
        '''

        :param pList:
        :return:
        '''
        for j in range(0,len(pList)):
            ind = self.findMaxIndex(j, pList)
            temp = pList[ind]
            pList[ind] = pList[j]
            pList[j] = temp
        return pList    
        
    def findMaxIndex(self, pStartIndex,pList):
        '''

        :param pStartIndex:
        :param pList:
        :return:
        '''
        maxLen = 0
        maxIndex = 0
        for i in range(pStartIndex,len(pList)):
            if len(pList[i])>maxLen:
                maxLen = len(pList)
                maxIndex = i
        return maxIndex