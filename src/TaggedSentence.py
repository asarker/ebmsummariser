'''
Created on 03/08/2011

@author: asarker
'''
import re
import string

class TaggedSentence(object):
    '''
    classdocs
    '''
    
    mNumber = 0
    mText = ""
    mSemanticTypes = []
    mPIBOSOTag = ""
    mSemtypeVec = []
    
    def __init__(self,pLine):
        self.mNumber = 0
        self.mText = ""
        self.mSemanticTypes = []
        self.mPIBOSOTag = ""
        self.mSemtypeVec = []
        
        '''
        Constructor
        '''
        
        tokens = pLine.split('||')
        self.mNumber = tokens[0]
        self.mText = tokens[1]
        self.mPIBOSOTag = tokens[3]
        #print self.mPIBOSOTag
        sToks = tokens[2].split(',')
        for s in sToks:
            self.mSemanticTypes.append(string.strip(re.sub('[%s]' % re.escape(string.punctuation), '', str(s))))
        allsemtypes= ['acab', 'acty', 'aggp', 'alga', 'amas', 'aapp', 'amph', 'anab', 'anst', 'anim', 'antb', 'arch', 'bact', 'bhvr', 'biof', 'bacs', 'bmod', 'bodm', 'bird', 'blor', 'bpoc', 'bsoj', 'bdsu', 'bdsy', 'carb', 'crbs', 'cell', 'celc', 'celf', 'comd', 'chem', 'chvf', 'chvs', 'clas', 'clna', 'clnd', 'cnce', 'cgab', 'dora', 'diap', 'dsyn', 'drdd', 'edac', 'eico', 'elii', 'emst', 'enty', 'eehu', 'enzy', 'evnt', 'emod', 'famg', 'fndg', 'fish', 'food', 'ffas', 'ftcn', 'fngs', 'gngp', 'gngm', 'genf', 'geoa', 'gora', 'grup', 'grpa', 'hops', 'hlca', 'hcro', 'horm', 'humn', 'hcpp', 'idcn', 'imft', 'irda', 'inbe', 'inpo', 'inch', 'inpr', 'invt', 'lbpr', 'lbtr', 'lang', 'lipd', 'mcha', 'mamm', 'mnob', 'medd', 'menp', 'mobd', 'mbrt', 'moft', 'mosq', 'npop', 'neop', 'nsba', 'nnon', 'nusq', 'objt', 'ocdi', 'ocac', 'ortf', 'orch', 'orgm', 'orga', 'orgf', 'orgt', 'opco', 'patf', 'podg', 'phsu', 'phpr', 'phob', 'phsf', 'plnt', 'popg', 'pros', 'prog', 'qlco', 'qnco', 'rcpt', 'rnlw', 'rept', 'resa', 'resd', 'rich', 'shro', 'sosy', 'socb', 'spco', 'strd', 'sbst', 'tmco', 'topp', 'tisu', 'vtbt', 'virs', 'vita']
        for st in allsemtypes:
            if st in self.mSemanticTypes:
                self.mSemtypeVec.append(1)
            else:
                self.mSemtypeVec.append(0)
        #print self.mSemanticTypes
       # print self.mSemtypeVec
        #print self.mSemanticTypes
    def display(self):
        print 'Sentence: ' + self.mNumber
        print 'Text: ' +self.mText
          
        
        