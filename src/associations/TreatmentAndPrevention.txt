['affects', 'causes', 'complicates', 'diagnoses', 'prevents', 'treats'] 	 963
['associated_with', 'result_of'] 	 583
['measures'] 	 520
['occurs_in'] 	 517
['affects', 'causes'] 	 474
['isa'] 	 474
['uses'] 	 439
['associated_with'] 	 430
['affects', 'complicates', 'prevents', 'treats'] 	 400
['interacts_with'] 	 386
['associated_with', 'evaluation_of', 'manifestation_of'] 	 265
['treats'] 	 254
['affects'] 	 216
['performs'] 	 187
['associated_with', 'occurs_in'] 	 175
['location_of'] 	 149
['diagnoses', 'evaluation_of', 'manifestation_of'] 	 100
['disrupts'] 	 88
['produces'] 	 36
['causes', 'complicates', 'treats'] 	 36
['co-occurs_with'] 	 29
['co-occurs_with', 'complicates', 'result_of'] 	 23
['location_of', 'manifestation_of', 'result_of'] 	 17
['causes'] 	 14
['associated_with', 'manifestation_of'] 	 11
['complicates', 'manifestation_of', 'occurs_in', 'result_of'] 	 6
['affects', 'complicates', 'degree_of', 'isa', 'manifestation_of', 'occurs_in', 'precedes', 'process_of', 'result_of'] 	 6
['affects', 'associated_with', 'co-occurs_with', 'complicates', 'degree_of', 'manifestation_of', 'occurs_in', 'precedes', 'process_of', 'result_of'] 	 6
['diagnoses', 'manifestation_of'] 	 5
['result_of'] 	 5
['associated_with', 'treats'] 	 4
['affects', 'causes', 'complicates'] 	 2