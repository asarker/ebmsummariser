import nltk
import numpy
import math
import collections
import string
from nltk.corpus import stopwords
from math import log
import re
import sys

stemmer = nltk.stem.porter.PorterStemmer()
    
def cosine_distance(u, v):
    """
    Returns the cosine of the angle between vectors v and u. This is equal to
    u.v / |u||v|.
    """
    return numpy.dot(u, v) / (math.sqrt(numpy.dot(u, u)) * math.sqrt(numpy.dot(v, v)))


def jaccard_similarity(u,v):
    """
    returns the jaccard similarity between two vectors which is:
    the number of common tokens/the total number of tokens
    """
    setu = set(u)
    setv = set(v)
    return len(setu.intersection(setv))/len(setu.union(setv))

'''
generates a list of tokenised sentences with semtypes
'''
def generate_sentlist_for_jaccard_similarity_with_semtype(pRecordId,pFilename,pQuestion):
    sentlist = []
    sents = []
    semtypes = []
    stemmer = nltk.stem.porter.PorterStemmer()
    sw = stopwords.words('english')
    infile = open(pFilename)
    for line in infile:
        tokens = line.split('||')
        stypes= string.strip(re.sub('[%s]' % re.escape(string.punctuation), ' ', tokens[2]))
        sents.append(string.strip(tokens[1])+' '+stypes)
        semtypes.append(string.strip(tokens[2]))
    qstypes = getSemTypesFromDocs(pRecordId,['Q'+pRecordId+'_umls.txt'])
    qstypes_string = ''
    for t in qstypes:
        qstypes_string+= t + ' '
    #add the question to sents
    sents.append(pQuestion+ ' ' + qstypes_string)
    
    for sent in sents:
        sentwords = []
        words = nltk.word_tokenize(sent)
        for word in words:
            if word not in sw:
                sentwords.append(stemmer.porter2stem(string.lower(word)))
        sentlist.append(sentwords)
    
    return sentlist
    
     
     
     
     
#this generates a document vector for mmr by incorporating semtype information along with 
#document terms

def generate_doc_vec_with_semtype(pRecordId, pFilename,pQuestion,qstypes = None):
    sents = []
    semtypes = []
    terms = []
    docvecs = []
    stemmer = nltk.stem.porter.PorterStemmer()
    sw = stopwords.words('english')
    infile = open(pFilename)
    print 'hereh ' + pFilename
    for line in infile:
        tokens = line.split('||')
        stypes= string.strip(re.sub('[%s]' % re.escape(string.punctuation), ' ', tokens[2]))
        
        #get the semtypes for this sentence
        #print tokens[2][1:-1].split(',')
        sents.append(string.strip(tokens[1])+' '+stypes)
        semtypes.append(string.strip(tokens[2]))
    if qstypes == None:
        qstypes = getSemTypesFromDocs(pRecordId,['Q'+pRecordId+'_umls.txt'])
    qstypes_string = ''
    for t in qstypes:
        qstypes_string+= t + ' '
    #add the question to sents
    sents.append(pQuestion+ ' ' + qstypes_string)
    reload(sys)
    sys.setdefaultencoding('utf8')
    #get all the terms in the document and the sentences
    #porter2stem, lower and remove stopwords
    
    for sent in sents:
        #print sent
        #if pos<len(sents)-1:
        #    stypes = string.strip(re.sub('[%s]' % re.escape(string.punctuation), ' ', semtypes[pos])).split()
        #    terms+=stypes
        words = nltk.word_tokenize(sent)
        for word in words:
            if word not in sw:
                try:
                    terms.append(stemmer.stem(string.lower(word.decode('utf-8','ignore').encode('utf-8','ignore'))))
                except UnicodeDecodeError:
                    terms.append('')


    #add the terms from the semtypes of the question
    #print terms
    
    #for each sentence, create a sentence vector and append that to docvec
    #remember, the last index of docvec contains the question            
    for sent in sents:
        #print sent
        sentvec = generateVector(sent,terms)
        docvecs.append(sentvec)
    #make an inverted index from the sentences and the terms
    #the inverted index will contain the terms as keys and the sentence numbers of the sentences which contain the
    #terms as values    
    invind = make_inverted_index(sents,terms)
    docvecs2 = []
    
    #go through all sentences again (last sentence is question)
    for sent in sents:
        #get the tfidfs
        alltfidfs = tfidfs(sents,invind,sent,terms)
       
        docvecs2.append(alltfidfs)
   # print docvecs2    
        
    return docvecs2

def generate_doc_vec(pFilename,pQuestion):
    sents = []
    terms = []
    docvecs = []
    stemmer = nltk.stem.porter.PorterStemmer()
    sw = stopwords.words('english')
    infile = open(pFilename)
    for line in infile:
        tokens = line.split('||')
        sents.append(string.strip(tokens[1]))
    sents.append(pQuestion)

    #get all the terms in the document and the sentences
    #porter2stem, lower and remove stopwords
    for sent in sents:
        words = nltk.word_tokenize(sent)
        for word in words:
            if word not in sw:
                terms.append(stemmer.stem(string.lower(word)))
    #for each sentence, create a sentence vector and append that to docvec
    #remember, the last index of docvec contains the question            
    for sent in sents:
        #print sent
        sentvec = generateVector(sent,terms)
        docvecs.append(sentvec)
    #make an inverted index from the sentences and the terms
    #the inverted index will contain the terms as keys and the sentence numbers of the sentences which contain the
    #terms as values    
    invind = make_inverted_index(sents,terms)
    docvecs2 = []
    
    #go through all sentences again (last sentence is question)
    for sent in sents:
        #get the tfidfs
        alltfidfs = tfidfs(stemmer,sents,invind,sent,terms)
       
        docvecs2.append(alltfidfs)
   # print docvecs2    
        
    return docvecs2
'''
makes an inverted index from a set of document terms and a set of sentences (last sentences is question)
'''

def make_inverted_index(pSents, pStemmedTerms):
    stemmer = nltk.stem.porter.PorterStemmer()
    inverted_index = {}
    sentnum =1
    #go through each sentence
    for sent in pSents:
        #tokenise the words and porter2stem
        words = nltk.word_tokenize(sent)
        stemmed_words = []
        for w in words:
            qw = w.decode('utf-8','ignore').encode('utf-8','ignore')
            try:
                qw = stemmer.stem(qw)
            except UnicodeDecodeError:
                qw = ''
        stemmed_words.append(qw)

        #go through all words in the sentence

        for sw in stemmed_words:
            #if word is already present in the inverted index,
            if sw in inverted_index:
                inverted_index[sw].add(sentnum)
            else:
                inverted_index[sw] = set((sentnum,))
        sentnum+=1
  #  print inverted_index
    return inverted_index

def tfs(sentence, terms):
    words = []
    for s in nltk.word_tokenize(sentence):
        ste = ''
        try:
            ste = stemmer.stem(s.decode('utf-8','ignore').encode('utf-8','ignore'))
        except UnicodeDecodeError:
            pass
        words.append(ste)
    return [words.count(term) for term in terms]

def dfs(inverted_index,terms):
    emptyset = set()
    return [len(inverted_index.get(term,emptyset)) for term in terms]

def tfidfs(sentences, inverted_index, sentence, terms):
    Tfs = tfs(sentence, terms)
    Dfs = dfs(inverted_index, terms)
    N = len(sentences)
    return [(TfDf[0]*log(N/(TfDf[1]+1e-100))) for TfDf in zip(Tfs,Dfs)]

'''
Generates a vector for a sentence using the terms in the sentence and all terms in the whole document
The vector holds the number of times each document term appears in the sentence
'''
def generateVector(pSent,pTerms):
    
    stemmer = nltk.stem.porter.PorterStemmer()
    sw = stopwords.words('english')
   
    #dictionary to contain each term as key and frequencies for the terms as values
    tfs = collections.defaultdict(int)
    
    words = nltk.word_tokenize(pSent)
    stemmedwords = []
    for word in words:
        if word not in sw:
          try:
            stemmedwords.append(stemmer.stem(word.decode('utf-8','ignore').encode('utf-8','ignore')))
          except UnicodeDecodeError:
            stemmedwords.append('')
    for term in pTerms:
        if term in stemmedwords:
            tfs[term]+=1
        else:
            tfs[term] = 0
    return tfs

def calculateMMR_jaccard(pSentlist, pSent1Num, pSent2NumList,pLambda):
    jaccardsim1 = jaccard_similarity(pSentlist[pSent1Num-1],pSentlist[-1])
    jaccardsim2 = 1.0
    for sentnum in pSent2NumList:
        jaccardsim2 *= jaccard_similarity(pSentlist, pSentlist[pSent1Num-1],pSentlist[sentnum-1] )
    mmr = (pLambda * cosinesim1) - ((1-pLambda) * cosinesim2) 
    return mmr    

def calculateMMR_cosine(pDocvec, pSent1Num, pSent2NumList, pLambda):
    '''
    lambda Sim(q,c) - (1-lambda) for all s,c :max Sim(s,c)
    '''
    #calculate similarity between sentence and question:
    cosinesim1 = generate_cosine_distance(pDocvec, pSent1Num, len(pDocvec))
    cosinesim2 = 1.0
    for sentnum in pSent2NumList:
        cosinesim2*= generate_cosine_distance(pDocvec, pSent1Num, sentnum)
    mmr = (pLambda * cosinesim1) - ((1-pLambda) * cosinesim2) 
    return mmr    

def generate_cosine_distance(pDocvec, pSent1Num, pSent2Num):

    if(pSent1Num>0):
        sent1 = pDocvec[pSent1Num-1]
        sent2 = pDocvec[pSent2Num-1]
    return cosine_distance(sent1, sent2)

#takes a list of file names containing the semantic types in
#text format. Collects all the semantic types
# and returns a list containing all the semantic types
# a list with a single entry can be passed in for a singlefile

def getSemTypesFromDocs(pRec,pList):
    semtypes = []
    print pList
    print pRec
    for fname in pList:
        infile = open('Resources/AllUMLS/'+pRec+'/'+fname,'r')
        for line in infile:
            phrases = line.split('||')
            for phrase in phrases:
                phrase_contents = []
                phrase_contents = [string.strip(re.sub('[%s]' % re.escape(string.punctuation), ' ', p)) for p in phrase.split(',')] 
                #print phrase_contents
                #print phrase_contents
                if len(phrase_contents) >2:
                    for item in phrase_contents[3:len(phrase_contents)-1]:
                        if len(item) > 0:
                           semtypes.append(string.strip(item))
                          # print semtypes
                #            umls_dict[phrase_contents[0]].append(item)
           #      st = token.split(',')
           #     if len(st)>3:
    
    return semtypes

if __name__=='__main__':
    filename = 'tagged_abstract_sents/10022110.txt'
    recordid = '4921'
    question = 'Do testosterone injections increase libido for elderly hypogonadal patients?'
    docvec = generate_doc_vec_with_semtype(recordid,filename,question)
    dist = generate_cosine_distance(docvec,6,8)
    print dist
        
    
