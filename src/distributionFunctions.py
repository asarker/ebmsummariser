'''
Created on Aug 15, 2011

@author: abeedsarker
'''
from __future__ import division
import string
import nltk
import cosinesimilarity
import re
from nltk.corpus import stopwords

stemmer = nltk.stem.porter.PorterStemmer()
sw = stopwords.words('english')

treatment_semtypes = ['orch','phsu','topp','ftcn','qlco','qnco']
diagnosis_semtypes = ['diap','dsyn','fndg','lbpr']

def rankSentBasedOnAssociations(qstypes,sentence_semtypes,assocdict,allassocs):
    '''
    Ranks a sentence based on the associations it has with the question (based on the semantic type present in the sentence and the question)
    '''
    score = 0.0
    qstypes = list(set(qstypes))
    sentence_semtypes = list(set(sentence_semtypes))
    for qst in qstypes:
        for sst in sentence_semtypes: 
            if cmp(qst,sst)!=0:
                assoc = str(allassocs.getRelation(qst, sst))
                assoc2 = str(allassocs.getRelation(sst, qst))
                if assoc in assocdict.keys():
                    score+=assocdict[assoc]
                if assoc2 in assocdict.keys():
                    score+=assocdict[assoc2]
    return score/(len(qstypes)+len(sentence_semtypes))            
                        

def rankSentBasedOnSemtypes(qtype,semtypelist):
    '''
    Given the frequency distribution of all semtypes in a sentence, ranks a sentence
    based on the type of the question
    '''
    fd = nltk.FreqDist(semtypelist)
    rank = 0
    if cmp(qtype,'treatment') == 0:
        for k in fd.keys():
            if k in treatment_semtypes:
                rank+=fd[k]
    if cmp(qtype,'diagnosis') == 0:
        for k in fd.keys():
            if k in diagnosis_semtypes:
                rank+=fd[k]
    rank = rank/len(semtypelist)
    return rank

def rankSentBasedOnSemtypes2(qtypes,semtypelist,all_type_train_fds,normalisedsemtypefd):
    '''
    This is the new function that attempts to score a sentence based on the semtypes it contains.

    For each type of question,
        - each semantic type has a score.
    In this version of scoring, only the 'presence' of semantic types is taken into account.
    For each semantic type present, its score is added.
    The sum gives the full score.
    This score is then returned.....

    '''

    rank = 0.0
    for qtype in qtypes:
        if len(qtype)>1:
         typescore = 0.0
         #for each type that the question belongs to:
         #create a dictionary that will contain new values for each semtype for that type:
         scoredict = {}
         #find the semtype fd for that type
         typefd = all_type_train_fds[qtype]
         #go through each key of the fd for this qtype
         for k in typefd.keys():
            if normalisedsemtypefd[k] == 0.0:
                scoredict[k]=0
            else:
                scoredict[k] = typefd[k]/normalisedsemtypefd[k]
         scoredict = normalize(scoredict)
        
         for st in list(set(semtypelist)):
            if len(st)>1 and st in scoredict.keys():
                typescore+=scoredict[st]
        
         rank+= typescore     
    if rank>1.0:
        print 'rank:' + str(rank)
    return rank    
    

def rankSentBasedOnRelativePosition(sentPos,freqDict):
    '''
    Ranks a sentence based on its relative position of the abstract.
    The ranking will depend on the frequency distribution passed into
    the function. For the first sentence of the abstract, the frequency
    distribution should be that of the relative positions of the first sentences.
    Similar for the second and third sentences.
    '''
    allkeys = []
    sum = 0
    for key in freqDict.keys():
        allkeys.append(int(10*float(key)))
        sum+= freqDict[key]
    allkeys.sort()
    upperrange=-1
    lowerrange=-1
    for i in range(0,9):
        if(sentPos*10>=9):
            upperrange=10
            lowerrange=9
            break
        if (sentPos*10 == allkeys[i]):
            upperrange = allkeys[i]
            lowerrange = allkeys[i]
            break
        if (float(allkeys[i])/10< sentPos) and (float(allkeys[i+1])/10>sentPos):
            upperrange = allkeys[i+1]
            lowerrange = allkeys[i]
            break
        upperrange = i+1
        lowerrange = i
    averageFrequency = (freqDict[str(upperrange/10)]+freqDict[str(lowerrange/10)])/2
    proportion = averageFrequency/sum
    weight = proportion
    return weight
    
def rankSentBasedOnLength(sentenceLength, averageSentenceLength, documentLength ):
    '''
    Ranks a sentence based on its length. The calculation can be performed using the
    average sentence lengths for the document and the full document lenght (*STILL TO DECIDE*)
    '''
    weight = (sentenceLength-averageSentenceLength)/(documentLength)
    return weight   

def rankSentBasedOnPIBOSOIndependent(pibosoCat, fdAll, fdBest):
    '''
    Ranks a sentence (independent of whether its the first sentence, second or third)
    based on its PIBOSO elements. It takes 2 frequency distributions,
    1: the frequency distribution of ALL the piboso elements
    2: the frequency distribution of the BEST piboso elements
    params:
        pibosoCat - piboso category of the sentence
        fdALL - the frequency distribution of all piboso elements
        fdBest - the frequency distribution of the best piboso elements
    '''

    sumAll = 0
    sumBest = 0
    for key in fdAll.keys():
        sumAll += fdAll[key]
    for key in fdBest.keys():
        sumBest += fdBest[key]
    allProportion = fdAll[pibosoCat]/sumAll
    bestProportion = fdBest[pibosoCat]/sumBest
    selectionRatio = bestProportion/allProportion
    sumAllProportions = 8.07594281966 #pre calculated from the code above
    
    return selectionRatio/sumAllProportions

def rankSentBasedOnPIBOSOAndPosition(pibosoCat, fdSentPosFreq, fdBest):
    '''
    Ranks a sentence based on its position and its PIBOSO category. It takes 2
    frequency distributions,
    1: the frequency distribution for piboso elements for all the first sentences in the best sentences
    2: the frequency distribution for piboso elements for all the all the best sentences
    '''
    sumPos = 0
    sumBest = 0
    for key in fdSentPosFreq.keys():
        sumPos += fdSentPosFreq[key]
    for key in fdBest.keys():
        sumBest += fdBest[key]
    sentProportion = fdSentPosFreq[pibosoCat]/sumPos
    bestProportion = fdBest[pibosoCat]/sumBest
    cats = ['outcome','other','background','study','intervention', 'population']
    sumAllProportions = 0
    for cat in cats:
        sumAllProportions += (fdSentPosFreq[cat]/sumPos)/(fdBest[cat]/sumBest)
    selectionRatio = (sentProportion/bestProportion)
    return selectionRatio/sumAllProportions

def chooseSentence(rankDict):
    '''
    Chooses the highest ranked sentence number from a dictionary
    '''
    maxRank = -500
    for key in rankDict.keys():
        if float(rankDict[key])>=maxRank:
            maxRank = float(rankDict[key])
    for key in rankDict.keys():
        if maxRank == float(rankDict[key]):
            return int(key)

def summariseAbstractQuestionSpecific(recordid, qstypes, assocdict, allassocs, all_type_train_fds, normalisedsemtypefd, qTypes, pFilename, pFSWeights, pQuestion, pFirstSentDistrib, pMidSentDistrib, pLastSentDistrib, all_piboso_freq, best_piboso_freq, first_sent_piboso_freq, last_sent_piboso_freq, mid_sent_piboso_freq, longtext):
      
       #the sentence rankings for the first sentence
       firstsentranks = {}
       #the sentence rankings for the middle sentence
       midsentranks = {}
       #the sentence rankings for the last sentence
       lastsentranks = {}
       chosensentnumbers = []

       
       fsrelposweight = pFSWeights[0]
       fslenweight = pFSWeights[3]
       fsindeppibosoweight = pFSWeights[1]
       fsdeppibosoweight = pFSWeights[2]
       fsmmrweight = pFSWeights[4]
       fsmmrlambda = pFSWeights[5]
       fsassocweight = pFSWeights[7]
       tmpsemtypeweight =pFSWeights[6]
       

       infile = open(pFilename)
       lines = []

       docvec = cosinesimilarity.generate_doc_vec_with_semtype(recordid,pFilename, pQuestion,qstypes)
       for line in infile:
           print line
           tokens= line.split('||')
           semtypestring = re.sub(r'[\'\[\]\s]', '', tokens[2])
           semtypes = semtypestring.split(',')
           lines.append((string.strip(tokens[0]),string.strip(tokens[1]),semtypes,string.strip(tokens[3])))
       doclen = 0
       numlines = 0
       for line in lines:
           doclen+=len(line[1])
           numlines +=1
       try:
           longt = nltk.word_tokenize(longtext.decode('utf-8','ignore').encode('utf-8','ignore'))
       except UnicodeDecodeError:
           longt = ''
       longtokens = []
       for unt in longt:
           try:
                stemmed_t = string.lower(stemmer.stem(unt))
                longtokens.append(stemmed_t)
           except UnicodeDecodeError:
                longtokens.append('')

       ################################################First sentence choosing######################
       linenumber = 1
       for line in lines:
           senttokens = []
           for w in nltk.word_tokenize(line[1]):
               stns = ''
               try:
                   stns = string.lower(stemmer.stem(w.decode('utf-8','ignore').encode('utf-8','ignore')))
               except UnicodeDecodeError:
                   pass
               senttokens.append(stns)
           sentence_semtypes = line[2]
           pibosoelem = line[3]
           if len(line[1])>1:
               assocscore = rankSentBasedOnAssociations(qstypes,sentence_semtypes,assocdict,allassocs)
               semtypescore =  rankSentBasedOnSemtypes2(qTypes,sentence_semtypes,all_type_train_fds,normalisedsemtypefd)
               questionsimilarity = cosinesimilarity.generate_cosine_distance(docvec, linenumber, len(docvec))
               relpos =  linenumber/numlines
               posscore = rankSentBasedOnRelativePosition(relpos,pFirstSentDistrib)
               lenscore = rankSentBasedOnLength(len(line[1]), doclen/numlines,doclen)
               indeppibososcore = rankSentBasedOnPIBOSOIndependent(pibosoelem, all_piboso_freq, best_piboso_freq)
               deppibososcore = rankSentBasedOnPIBOSOAndPosition(pibosoelem, first_sent_piboso_freq, best_piboso_freq)
               totalscore = (fsassocweight*assocscore + fsmmrweight * questionsimilarity + tmpsemtypeweight*semtypescore +fsrelposweight*posscore + fslenweight *lenscore + fsindeppibosoweight* indeppibososcore+ fsdeppibosoweight* deppibososcore)
               firstsentranks[linenumber] = totalscore
               print pFilename + ',' + str(linenumber) + ',' + str(posscore) + ',' + str(lenscore) + ',' + str(indeppibososcore) +',' + str(deppibososcore) + ',' + str(questionsimilarity) + ',' + str(semtypescore) + ',' +  str(assocscore)+','+str(totalscore)
           linenumber+=1

       firstsentnumber = chooseSentence(firstsentranks)
       chosensentnumbers.append(firstsentnumber)
       #####################END OF FIRST SENTENCE CHOOSING##############################
       #####################SECOND SENTENCE STUFF#####################################
       linenumber = 1
       for line in lines:
           senttokens = []
           for w in nltk.word_tokenize(line[1]):
               stns = ''
               try:
                   stns = string.lower(stemmer.stem(w.decode('utf-8','ignore').encode('utf-8','ignore')))
               except UnicodeDecodeError:
                   pass
               senttokens.append(stns)


           sentence_semtypes = line[2]
           pibosoelem = line[3]
           if not(int(line[0]) == firstsentnumber) and len(line[1])>1:
               assocscore = rankSentBasedOnAssociations(qstypes,sentence_semtypes,assocdict,allassocs)
               semtypescore = rankSentBasedOnSemtypes2(qTypes,sentence_semtypes,all_type_train_fds,normalisedsemtypefd)
               mmr = cosinesimilarity.calculateMMR_cosine(docvec, linenumber, chosensentnumbers, fsmmrlambda)
               relpos =  linenumber/numlines
               posscore = rankSentBasedOnRelativePosition(relpos,pMidSentDistrib)
               lenscore = rankSentBasedOnLength(len(line[1]), doclen/numlines,doclen)
               indeppibososcore = rankSentBasedOnPIBOSOIndependent(pibosoelem, all_piboso_freq, best_piboso_freq)
               deppibososcore = rankSentBasedOnPIBOSOAndPosition(pibosoelem, mid_sent_piboso_freq, best_piboso_freq)
               totalscore = (fsassocweight*assocscore+ fsmmrweight * mmr + tmpsemtypeweight*semtypescore +fsrelposweight*posscore + fslenweight *lenscore + fsindeppibosoweight* indeppibososcore+ fsdeppibosoweight* deppibososcore)
               midsentranks[linenumber] = totalscore
               linenumber+=1
           else:
               linenumber+=1
       #neutralise the chosen sentence
       midsentranks[firstsentnumber] = -50000
       midsentnumber = chooseSentence(midsentranks)
       chosensentnumbers.append(midsentnumber)
       ############################END SECOND SENTENCE##############################################
            ###############LAST SENTENCE SELECTION PROCEDURE######################
       linenumber =1

       for line in lines:
           senttokens = []
           for w in nltk.word_tokenize(line[1]):
               stns = ''
               try:
                   stns = string.lower(stemmer.stem(w.decode('utf-8','ignore').encode('utf-8','ignore')))
               except UnicodeDecodeError:
                   pass
               senttokens.append(stns)

           sentence_semtypes = line[2]
           pibosoelem = line[3]
           if not(int(line[0]) == firstsentnumber or int(line[0]) == midsentnumber) and len(line[1])>1:
               semtypescore = rankSentBasedOnSemtypes2(qTypes,sentence_semtypes,all_type_train_fds,normalisedsemtypefd)
               assocscore = rankSentBasedOnAssociations(qstypes,sentence_semtypes,assocdict,allassocs)
               pibosoelem = line[3]
               mmr = cosinesimilarity.calculateMMR_cosine(docvec, linenumber, chosensentnumbers, fsmmrlambda)
               relpos =  linenumber/numlines
               posscore = rankSentBasedOnRelativePosition(relpos,pLastSentDistrib)
               lenscore = rankSentBasedOnLength(len(line[1]), doclen/numlines,doclen)
               indeppibososcore = rankSentBasedOnPIBOSOIndependent(pibosoelem, all_piboso_freq, best_piboso_freq)
               deppibososcore = rankSentBasedOnPIBOSOAndPosition(pibosoelem, last_sent_piboso_freq, best_piboso_freq)
               totalscore = (fsassocweight*assocscore+ fsmmrweight * mmr + tmpsemtypeweight*semtypescore +fsrelposweight*posscore + fslenweight *lenscore + fsindeppibosoweight* indeppibososcore+ fsdeppibosoweight* deppibososcore)
               lastsentranks[linenumber] = totalscore
               linenumber+=1
           else:
               linenumber+=1
       lastsentranks[firstsentnumber] = -50000
       lastsentranks[midsentnumber] = -50000
       lastsentnumber = chooseSentence(lastsentranks)

       chosensentnumbers.append(lastsentnumber)
     #############################END LAST SENTENCE################################

       print 'Filename: ' + pFilename
       print 'Chosen Sentences: ' + str(chosensentnumbers)

       sentences = getSentences(pFilename,chosensentnumbers)
       #except IOError:
       #    sentences = []

       return sentences

def getSentences(pFilename,pSentNumbers):
    '''
    Given a list of sentence numbers and a filename, the function selects the
    chosen sentences, concatanates them as strings separated by newline and returns
    the string
    '''
    sents = ""
    infile = open(pFilename)
    for line in infile: 
        tokens = line.split('||')
        sentnum = int(string.strip(tokens[0]))
        if sentnum in pSentNumbers:
            sents += string.strip(tokens[1]) +'\n'
    return sents


def normalize(fd):
        sum = 0
        normalised_hist = {}
        for k in fd.keys():
            sum+= fd[k]
            
        for k in fd.keys():
            normalised_hist[k]=fd[k]/sum
        return normalised_hist
