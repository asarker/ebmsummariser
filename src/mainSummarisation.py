
from __future__ import division
import os, string, TaggedAbstract,re, nltk, AllAssociations, collections
from runSummariser import QSpec_EBMSummariser
import sys


class mainSummarisation(object):
    '''


    '''

    mLongs = []
    mAbstracts = []
    abco = 0
    mQuestions = {}
    mGenSummaryFile = ""
    semtypefd = {}
    normalisedsemtypefd = {}
    all_semtypes = []

    qts = ['Prognosis', 'Management', 'Pharmacological', 'PhysicalFinding', 'Epidemiology', 'Device', 'Etiology',
           'TreatmentAndPrevention', 'Diagnosis', 'Test', 'Procedure', 'History']

    def __init__(self):
        '''
        Constructor
        '''
        print 'Initializing the summarizer ... '
        self.mLongs = []
        self.mAbstracts = []
        self.mQuestions = {}

    def loadQuestions(self):
        '''
        load a given predefined set of questions
        there are a total of 256 questions..
        :return:
        '''
        print 'Loading all the questions ...'
        infile = open('src/Resources/Questions.txt')
        for line in infile:
            tokens = line.split('\t')
            self.mQuestions[string.strip(tokens[0])] = string.strip(tokens[1])

    def loadLongs(self):
        '''
        Load the detailed justifications..
        These files contain:
        Record ID: The dentifier for the question
        Snip ID: The identifier for the evidence-based, multi-document summariser
        Long ID: The ID of the detailed justification (single-document summary)
        Long text
        Abstract IDs: IDs of the abstracts associated with the question.

        :return:
        '''
        print 'Load the detailed single-document detailed justifications ...'
        vFiles = os.listdir('src/longs/')
        for f in vFiles:
            if not re.search(r'[A-Z]', f):
                infile = open('src/longs/' + f, 'r')
                self.mLongs.append(self.processLongFile(infile))
                infile.close()

    def processLongFile(self, pInfile):
        '''
        process the details justifications in the format provided in the /longs folder
        :param pInfile:
        :return: a dictionary containing the long information in pInfile
        '''

        vLines = []
        vLongDict = {}
        for line in pInfile:
            vLines.append(line)
        vLongDict['recordid'] = string.strip(vLines[0][10:])
        vLongDict['snipid'] = string.strip(vLines[1][8:])
        vLongDict['longid'] = string.strip(vLines[2][8:])
        vLongDict['text'] = string.strip(vLines[3])
        vLongDict['abstractids'] = list()
        vItems = vLines[4].split('\t')
        for item in vItems:
            if len(string.strip(item)) > 0:
                vLongDict['abstractids'].append(string.strip(item))
        return vLongDict

    def loadAbstractIDs(self):
        '''
           Loads the trainset and the testset abstract IDs
        '''
        trainabs = []
        testabs = []

        infile = open('src/Resources/trainsetabs.txt')
        for line in infile:
            trainabs.append(string.strip(line))
        infile = open('src/Resources/testsetabs.txt')
        for line in infile:
            testabs.append(string.strip(line))
        infile.close()
        return trainabs, testabs

    def loadSentencePositionFDs(self):
        '''
        Loads frequency distributions for the three sentence positions
        '''
        first_sent_pos = []
        last_sent_pos = []
        mid_sent_pos = []
        infile = open('src/Resources/trainingSentPositions.txt')
        for line in infile:
            tokens = line.split('\t')
            first_sent_pos.append(string.strip(tokens[0]))
            last_sent_pos.append(string.strip(tokens[1]))
            if len(string.strip(tokens[2])) > 1:
                mid_sent_pos.append(string.strip(tokens[2]))

        # get the frequency distributions of the sentence positions
        fdfs = nltk.FreqDist(first_sent_pos)
        fdms = nltk.FreqDist(mid_sent_pos)
        fdls = nltk.FreqDist(last_sent_pos)

        return fdfs, fdms, fdls

    def loadPIBOSOFDs(self):
        '''
        generate 5 types of piboso related frequencies (normalized)
        '''

        # piboso related stats
        # the PIBOSO frequencies for ALL the sentences in ALL the abstracts
        all_piboso_freq = {}
        infile = open('src/Resources/train_set_all_piboso_freq.txt')
        for line in infile:
            tokens = line.split('\t')
            all_piboso_freq[string.strip(tokens[0])] = int(string.strip(tokens[1]))
        # the PIBOSO frequencies for the sentences that gives best ROUGE scores
        best_piboso_freq = {}
        infile = open('src/Resources/train_set_best_piboso_freq.txt')
        for line in infile:
            tokens = line.split('\t')
            best_piboso_freq[string.strip(tokens[0])] = int(string.strip(tokens[1]))

        # the first sent PIBOSO frequencies (best sentences)
        first_sent_piboso_freq = {}
        infile = open('src/Resources/train_set_first_sent_piboso.txt')
        for line in infile:
            tokens = line.split('\t')
            first_sent_piboso_freq[string.strip(tokens[0])] = int(string.strip(tokens[1]))

        # the last sent PIBOSO frequencies (best sentences)
        last_sent_piboso_freq = {}
        infile = open('src/Resources/train_set_last_sent_piboso.txt')
        for line in infile:
            tokens = line.split('\t')
            last_sent_piboso_freq[string.strip(tokens[0])] = int(string.strip(tokens[1]))
        # the mid sent PIBOSO frequencies (best sentences)
        mid_sent_piboso_freq = {}
        infile = open('src/Resources/train_set_mid_sent_piboso.txt')
        for line in infile:
            tokens = line.split('\t')
            mid_sent_piboso_freq[string.strip(tokens[0])] = int(string.strip(tokens[1]))

        return self.normalize(all_piboso_freq), self.normalize(best_piboso_freq), self.normalize(
            first_sent_piboso_freq), self.normalize(last_sent_piboso_freq), self.normalize(mid_sent_piboso_freq)

    def loadRecordsAndUMLS(self,pFilename):
        '''

        :param pFilename:
        :return:
        '''
        semtypes = self.loadSemtypes()
        vRec = []
        vSemTypes = []
        # open the file containing all the record ids
        recfile = open(pFilename, 'r')
        for line in recfile:
            vRec.append(string.strip(line))

        # ***this is where the semtypes are combined together****

        # go through each record id
        for rec in vRec:
            # create a dictionary for this record
            semtype_list = collections.defaultdict(int)
            # add the id of the record
            semtype_list['id'] = rec
            # add all the semtypes as keys and zero the counts
            for st in semtypes:
                semtype_list[st] = 0
            # get the list of semtypes for this record
            s = self.getLongSemTypes(rec)

            # add the counts
            for st in s:
                semtype_list[st] += 1

            # concatanate the list of semtypes for this record to an overall list
            vSemTypes += s

            # add the dictionary for this record to a list of dictionaries
            self.all_semtypes.append(semtype_list)

        fd = nltk.FreqDist(vSemTypes)

        normalised_fd = {}
        sum = 0
        for k in semtypes:
            sum += fd[k]
        for k in semtypes:
            normalised_fd[k] = fd[k] / sum

        return fd, normalised_fd

    def loadSemtypes(self):
        stypes = []
        f = open('src/Resources/SemanticTypes.txt', 'r')
        for line in f:
            s = string.strip(line.split('\t')[-1])
            stypes.append(s)
        # print stypes
        return stypes

    def getLongSemTypes(self,pRec):
        '''
             given a record, it collects all the semantic types from
             returns a [dictionary or list] with the semantic types and their frequencies
        '''
        # get the long files for this record
        dirfiles = []
        longs = []
        dirfiles = os.listdir('src/Resources/AllUMLS/' + pRec + '/')
        for f in dirfiles:
            # longs = []
            if re.search('Q' + pRec + 'sn[1-9]+_[1-9]+_', f):
                longs.append(f)
        # currently returns a list with all the semtypes
        return self.getSemTypesFromDocs(pRec, longs)

    def loadQTypes(self, recordid):
        infile = open('src/questionclassifications/qtypesforrecords_modified.txt')
        for line in infile:
            tokens = line.split(' ')
            vRecId = tokens[0][:-1]
            if (cmp(vRecId, recordid) == 0):
                items = [string.strip(re.sub('[%s]' % re.escape(string.punctuation), ' ', p)) for p in tokens[1:]]
        if len(items) == 0:
            return ['']
        # print 'items' + ' ' + str(items)
        return items

    def loadAssociationDictionary(self, qtypes):
        '''
            given a list of question types, it loads all the important associations for that semtype and puts everything in a dictionary
        '''
        assocdict = collections.defaultdict(int)
        for qt in qtypes:
          try:
            infile = open('associations/' + qt + '.txt')
            for line in infile:
                tokens = line.split('\t')
                try:
                    assocdict[string.strip(tokens[0])] += int(string.strip(tokens[1]))
                except KeyError:
                    assocdict[str(string.strip(tokens[0]))] = 0
                    assocdict[str(string.strip(tokens[0]))] += int(string.strip(tokens[1]))
            infile.close()
          except:
              pass
        assocdict = self.normalize(assocdict)
        return assocdict

    def getSemTypesFromDocs(self,pRec, pList):
        '''
             takes a list of file names containing the semantic types in
             text format. Collects all the semantic types
             and returns a list containing all the semantic types
             a list with a single entry can be passed in for a singlefile
        '''
        semtypes = []

        for fname in pList:
            infile = open('src/Resources/AllUMLS/' + pRec + '/' + fname, 'r')
            for line in infile:
                phrases = line.split('||')
                for phrase in phrases:
                    phrase_contents = [string.strip(re.sub('[%s]' % re.escape(string.punctuation), ' ', p)) for p in
                                       phrase.split(',')]
                    if len(phrase_contents) > 2:
                        for item in phrase_contents[3:len(phrase_contents) - 1]:
                            if len(item) > 0:
                                semtypes.append(string.strip(item))
        return semtypes

    def loadTaggedAbstract(self, absid):
        # print absid
        vAbs = TaggedAbstract.TaggedAbstract(str(absid))
        return vAbs


    def writeGeneratedSummaryToFile(self, pSummary, pAbsId,outputfolder='summaryoutputs'):
        vOutfile = open(outputfolder+'/'+ pAbsId, 'w')
        vOutfile.write(str(pSummary))
        vOutfile.close()


    def normalize(self, fd):
        '''
            given a frequency distribution, normalizes it
        '''

        sum = 0
        normalised_hist = {}
        for k in fd.keys():
            sum += fd[k]

        for k in fd.keys():
            normalised_hist[k] = fd[k] / sum
        return normalised_hist

    def runEBMSummariser(self, weights, absid, question, question_types, question_semtypes,recordid="",outputfolder="summaryoutputs"):
        '''
        The main summarisation function.

        data includes:
        a summary
        an xml file describing the experiment
        algorithm:
        - go through each long
        - get the abstract ids for that long
        - summarise the abstract for each long based on some summarisation funcion
        - write the longtext to file, write the summary to file
        - write update the xml mapping
        '''
        expname = 'reimplementation'
        print 'Running function: --> runEBMSummariser  <-- ....'

        #processed_abs  = os.listdir('src/newsummaries/')



        #load the training and test set abstracts
        trainabs, testabs = self.loadAbstractIDs()
        # sentence position related stats
        fdfs, fdms, fdls = self.loadSentencePositionFDs()
        # piboso related stats
        allpibfreq, bestpibfreq, firstpibfreq, lastpibfreq, midpibfreq = self.loadPIBOSOFDs()
        # Question type related stats
        # frequency distributions for the longs of  ALL questions in the TRAINING set
        semtypefd, normalisedsemtypefd = self.loadRecordsAndUMLS('src/Resources/trainsetrecordids.txt')
        # the value for each key will be the semantic type fd in the training set for that qtype
        all_type_train_fds = {}
        # obtain all the semtype associations
        allassocs = AllAssociations.AllAssociations()

        for t in self.qts:
            stfd, normstfd = self.loadRecordsAndUMLS('src/questionclassifications/newrecordlist_train_' + t + '.txt')
            all_type_train_fds[t] = normstfd

        counter = 0
        qstypes = question_semtypes
        assocdict = self.loadAssociationDictionary(question_types)
        abs = absid
        # Go through each abstract of each long
        print 'Question:' + question
        print 'Question types: ' + str(question_types)
        print 'Question semantic types: ' +  str(question_semtypes)

        summ = self.summariseAbstract(recordid, question_semtypes, assocdict, allassocs, all_type_train_fds,
                                      normalisedsemtypefd, question_types, abs, weights,
                                      question, fdfs, fdms, fdls, allpibfreq,
                                      bestpibfreq, firstpibfreq, lastpibfreq, midpibfreq, "")
        if len(string.strip(str(summ))) > 0:
            print summ
            self.writeGeneratedSummaryToFile(summ, abs, outputfolder)
            counter += 1
    print 'Done'


    def summariseAbstract(self, recordid, qstypes, assocdict, allassocs, all_type_train_fds, normalisedsemtypefd,
                          qtypes, pAbstract, pWeights, pQuestion, fdfs, fdms, fdls, allpibfreq, bestpibfreq,
                          firstpibfreq, lastpibfreq, midpibfreq, longtext):
        '''
        Primary summarisation function

        :param recordid:
        :param qstypes:
        :param assocdict:
        :param allassocs:
        :param all_type_train_fds:
        :param normalisedsemtypefd:
        :param qtypes:
        :param pAbstract:
        :param pWeights:
        :param pQuestion:
        :param fdfs:
        :param fdms:
        :param fdls:
        :param allpibfreq:
        :param bestpibfreq:
        :param firstpibfreq:
        :param lastpibfreq:
        :param midpibfreq:
        :param longtext:
        :return:
        '''

        vAbs = self.loadTaggedAbstract(pAbstract)
        # *******************************************************#
        # Change this function call to apply other forms of summarisation

        vSummarisedText = vAbs.summariseAbstractQuestionSpecific(recordid, qstypes, assocdict, allassocs,
                                                                 all_type_train_fds, normalisedsemtypefd, qtypes,
                                                                 pAbstract, pWeights, pQuestion, fdfs, fdms, fdls,
                                                                 allpibfreq, bestpibfreq, firstpibfreq, lastpibfreq,
                                                                 midpibfreq, longtext)
        # *******************************************************#
        return vSummarisedText






