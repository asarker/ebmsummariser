1||OBJECTIVES.||[]||other
2||To determine whether children with severe acute asthma treated with large doses of inhaled salbutamol, inhaled ipratropium, and intravenous steroids are conferred any further benefits by the addition of aminophylline given intravenously.||['phsu', 'spco', 'dsyn', 'clna', 'prog', 'gngm', 'orgf', 'qlco', 'tmco', 'aggp', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr', 'strd']||background
3||STUDY DESIGN.||[]||other
4||Randomised, double blind, placebo controlled trial of 163 children admitted to hospital with asthma who were unresponsive to nebulised salbutamol.||['phsu', 'hcro', 'spco', 'resa', 'orch', 'dsyn', 'prog', 'gngm', 'topp', 'qlco', 'mamm', 'aggp', 'geoa', 'hlca', 'ftcn', 'mnob', 'inpr']||intervention
5||RESULTS.||[]||other
6||The placebo and treatment groups of children were similar at baseline.||['spco', 'popg', 'gngm', 'topp', 'idcn', 'qlco', 'bodm', 'aggp', 'prog', 'ftcn', 'inpr', 'chvf']||outcome
7||The 48 children in the aminophylline group had a greater improvement in spirometry at six hours and a higher oxygen saturation in the first 30 hours.||['phsu', 'spco', 'popg', 'prog', 'moft', 'gngm', 'elii', 'idcn', 'qlco', 'tmco', 'aggp', 'orch', 'qnco', 'geoa', 'inpr', 'diap']||outcome
8||Five subjects in the placebo group were intubated and ventilated after enrollment compared with none in the aminophylline group.||['phsu', 'acty', 'spco', 'popg', 'clna', 'dsyn', 'gngm', 'topp', 'idcn', 'geoa', 'grup', 'orch', 'qnco', 'hlca', 'ftcn', 'tmco']||outcome
9||CONCLUSIONS.||[]||outcome
10||Aminophylline continues to have a place in the management of severe acute asthma in children unresponsive to initial treatment.||['orga', 'popg', 'ocac', 'dsyn', 'prog', 'tmco', 'gngm', 'idcn', 'qlco', 'spco', 'mamm', 'aggp', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
