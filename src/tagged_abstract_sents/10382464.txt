1||INTRODUCTION.||[]||other
2||The immediate post-partum period is stressful for most parents.||['spco', 'dsyn', 'fndg', 'ftcn', 'tmco', 'famg']||background
3||The need to use a home apnea monitor may tax parental coping skills even further.||['cnce', 'medd', 'dsyn', 'qlco', 'spco', 'fndg', 'inbe', 'qnco', 'geoa', 'ftcn', 'famg']||background
4||Therefore, we conducted a study to assess the psychosocial consequences of apnea monitoring on parental emotional distress and family functioning.||['acty', 'clna', 'dsyn', 'gngm', 'menp', 'mobd', 'medd', 'hlca', 'phsf', 'famg', 'fndg', 'geoa', 'ftcn', 'resa', 'nusq']||other
5||METHOD.||[]||other
6||We studied 104 parents of infants at high risk for cardiopulmonary arrest.||['bpoc', 'clna', 'dsyn', 'qlco', 'fndg', 'dora', 'aggp', 'patf', 'famg']||population
7||Fifty-two parents had infants who used home apnea monitors, and 52 parents were age-matched and gender-matched control subjects.||['clna', 'cnce', 'medd', 'dsyn', 'orga', 'idcn', 'qlco', 'spco', 'grup', 'fndg', 'qnco', 'ftcn', 'famg', 'aggp']||other
8||Data were collected during the infant's hospitalization, and then at 2 weeks, 3 months, and 6 months after discharge.||['hcro', 'clna', 'dsyn', 'bdsu', 'idcn', 'aggp', 'geoa', 'mnob', 'tmco']||other
9||At 1 year, parents were interviewed about their attitudes toward using the apnea monitor.||['acty', 'medd', 'dsyn', 'menp', 'fndg', 'geoa', 'ftcn', 'tmco', 'famg']||other
10||RESULTS.||[]||other
11||Both groups experienced elevated levels of emotional distress, but the group with infants who used the monitors had significant increases in depression and hostility immediately following discharge from the hospital compared with baseline, whereas the non-monitored group had a significant increase in hostility at 6 months.||['acty', 'hcro', 'dsyn', 'medd', 'menp', 'mobd', 'idcn', 'qlco', 'fndg', 'bodm', 'bdsu', 'geoa', 'ftcn', 'mnob', 'tmco', 'chvf', 'aggp']||outcome
12||At 1-year follow-up, the majority of the parents reported that they used the monitor every night, felt more secure in using it, and judged it helpful.||['tmco', 'medd', 'dsyn', 'prog', 'idcn', 'qlco', 'socb', 'geoa', 'ftcn', 'inpr', 'famg']||outcome
13||DISCUSSION.||[]||outcome
14||The immediate period following hospital discharge of infants who need to use a home apnea monitor is characterized by significant emotional distress for the parents, which resolves over time.||['acty', 'hcro', 'spco', 'clna', 'dsyn', 'menp', 'bdsu', 'medd', 'idcn', 'qlco', 'cnce', 'famg', 'fndg', 'geoa', 'ftcn', 'mnob', 'tmco', 'aggp']||outcome
15||Anticipatory education and counseling of parents is recommended.||['phsu', 'spco', 'popg', 'dsyn', 'edac', 'gngm', 'idcn', 'orch', 'hlca', 'famg']||outcome
