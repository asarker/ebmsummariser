1||OBJECTIVES.||[]||other
2||The objective of this review is to identify and evaluate the evidence of efficacy for topical treatments for fungal infections of the skin and nails of the human foot.||['bpoc', 'spco', 'popg', 'clna', 'dsyn', 'orga', 'fngs', 'topp', 'humn', 'bdsu', 'geoa', 'ftcn', 'resa', 'inpr', 'gngm']||other
3||To establish the effectiveness of topical treatments in achieving a cured condition and in preventing recurrence SEARCH STRATEGY.||['phsu', 'spco', 'popg', 'dsyn', 'clna', 'qlco', 'topp', 'gora', 'tmco', 'geoa', 'ftcn', 'inpr', 'phpr', 'evnt']||other
4||Randomised controlled trials were identified from the MEDLINE, EMBASE and CINHAL databases, from the beginning of these databases to December 1997.||['inpr', 'dsyn', 'idcn', 'qlco', 'tmco', 'geoa', 'ftcn', 'resa', 'chvf']||other
5||Also we screened the Cochrane Controlled Trials Register, the Science Citation Index BIOSIS, CAB - Health, Healthstar and Economic databases.||['idcn', 'dsyn', 'gngm', 'lbpr', 'geoa', 'inpr', 'chvf']||other
6||References and unpublished studies were searched, podiatry journals handsearched and the pharmaceutical industry contacted.||['bpoc', 'acty', 'popg', 'ocac', 'clna', 'bmod', 'idcn', 'bodm', 'dsyn', 'qnco', 'geoa', 'mnob', 'inpr', 'bsoj']||other
7||SELECTION CRITERIA.||[]||other
8||Only randomised controlled trials (RCTs) using participants who have mycologically diagnosed fungal infections of the skin and nails of the human foot are included in the analysis.||['bpoc', 'lbpr', 'gngm', 'popg', 'clna', 'dsyn', 'orga', 'bdsu', 'idcn', 'qlco', 'fndg', 'humn', 'fngs', 'spco', 'geoa', 'ftcn', 'resa']||other
9||DATA COLLECTION AND ANALYSIS.||[]||other
10||Two reviewers independently summarised the included trials and appraised their quality of reporting using a structured data extraction tool which assessed 12 quality criteria.||['bpoc', 'acty', 'spco', 'medd', 'dsyn', 'geoa', 'clna', 'topp', 'idcn', 'qlco', 'inpr', 'qnco', 'hlca', 'ftcn', 'mnob', 'resa']||other
11||MAIN RESULTS.||[]||other
12||Of 126 trials identified in 121 papers, 72 met the inclusion criteria.||['inpr', 'dsyn', 'qlco', 'geoa', 'mnob', 'resa']||outcome
13||Placebo-controlled trials yielded the following pooled relative risks of failure to cure (RRFC) for skin infections: allylamines 0.30 (95% confidence interval 0.23 to 0.37); azoles 0.53 (0.42 to 0.68); undecenoic acid 0.28 (0.11 to 0.74); tolnaftate 0.46 (0.17 to 1.22).||['phsu', 'cnce', 'clna', 'dsyn', 'qlco', 'famg', 'gngm', 'bdsu', 'gora', 'tmco', 'chem', 'bacs', 'orch', 'qnco', 'geoa', 'ftcn', 'resa', 'lipd']||outcome
14||Though meta-analysis of 11 trials comparing allylamines and azoles showed an RRFC of 0.88 (0.78 to 0.99) in favour of allylamines, there was evidence of language bias.||['lang', 'idcn', 'gngm', 'resa', 'dsyn', 'euka', 'orga', 'lbpr', 'orch', 'geoa', 'ftcn', 'inpr']||outcome
15||Seven English language reports favoured allylamines (RRFC = 0.79; 0.68 to 0.93) but four foreign language reports showed no difference between the two drugs (RRFC = 1.00; 0.90 to 1.12).||['phsu', 'lang', 'dsyn', 'orga', 'qlco', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
16||The two trials of nail infections did not provide any evidence of benefit for topical treatments compared with placebo.||['bpoc', 'acty', 'spco', 'popg', 'clna', 'dsyn', 'food', 'gngm', 'topp', 'qnco', 'geoa', 'ftcn', 'resa']||outcome
17||REVIEWER'S CONCLUSIONS.||[]||outcome
18||CONCLUSIONS.||[]||outcome
19||In placebo-controlled trials allylamines, azoles and undecenoic acid were efficacious.||['phsu', 'spco', 'resa', 'lipd', 'dsyn', 'gngm', 'topp', 'qlco', 'bacs', 'orch', 'ftcn', 'chem']||outcome
20||There are sufficient comparative trials to judge relative efficacy only between allylamines and azoles.||['gngm', 'idcn', 'qlco', 'orch', 'prog', 'resa', 'famg']||outcome
21||Allylamines cure slightly more infections than azoles but are much more expensive.||['dsyn', 'clna', 'gngm', 'idcn', 'gora', 'orch', 'geoa']||outcome
22||The most cost-effective strategy is first to treat with azoles or undecenoic acid and to use allylamines only if that fails.||['phsu', 'acty', 'lipd', 'clna', 'gngm', 'menp', 'qlco', 'bacs', 'orch', 'qnco', 'geoa', 'ftcn', 'chem']||outcome
