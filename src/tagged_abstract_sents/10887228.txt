1||PURPOSE.||[]||other
2||To review a 26-year single-center clinical experience with inferior vena caval filters.||['bpoc', 'hcro', 'spco', 'popg', 'clna', 'gngm', 'menp', 'inpr', 'qlco', 'geoa', 'mnob', 'tmco']||background
3||MATERIALS AND METHODS.||[]||other
4||During 1973-1998, 1,765 filters were implanted in 1,731 patients.||['bpoc', 'spco', 'medd', 'dsyn', 'podg', 'gngm', 'menp', 'qlco', 'qnco', 'mnob', 'plnt']||other
5||Hospital files were reviewed, and data were collected about the indications, safety, effectiveness, numbers, and types of caval filters.||['bpoc', 'ocac', 'dsyn', 'clna', 'chvs', 'fish', 'gngm', 'elii', 'medd', 'idcn', 'qlco', 'spco', 'qnco', 'hcpp', 'geoa', 'ftcn', 'mnob', 'inpr', 'blor']||other
6||Fatal post-filter pulmonary embolism (PE) was considered the primary outcome.||['bpoc', 'spco', 'blor', 'dsyn', 'gngm', 'idcn', 'qlco', 'fndg', 'patf', 'geoa', 'ftcn', 'mnob', 'tmco']||other
7||Morbidity and mortality were determined as secondary outcomes.||['dsyn', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'genf']||other
8||Survival and morbidity-free survival curves were calculated.||['inpr', 'acty', 'spco', 'dsyn', 'idcn', 'qlco', 'qnco', 'ftcn', 'tmco']||other
9||RESULTS.||[]||outcome
10||The prevalence of observed post-filter PE was 5.6%.||['spco', 'clna', 'dsyn', 'gngm', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco', 'blor']||outcome
11||It was fatal in 3.7% of patients.||['bpoc', 'gngm', 'podg', 'orga', 'qlco', 'qnco']||outcome
12||In most patients, fatal PE occurred soon after filter insertion (median, 4.0 days; 95% CI: 2.2, 5.8 days).||['bpoc', 'cnce', 'clna', 'dsyn', 'podg', 'orga', 'gngm', 'elii', 'topp', 'inpr', 'qlco', 'spco', 'mamm', 'fndg', 'qnco', 'genf', 'ftcn', 'mnob', 'tmco', 'blor']||outcome
13||Major complications occurred in 0.3% of procedures.||['phsu', 'cnce', 'clna', 'dsyn', 'chvs', 'qnco', 'elii', 'topp', 'fndg', 'bacs', 'aapp', 'patf', 'geoa', 'inpr', 'gngm', 'orch', 'diap']||outcome
14||The prevalence of observed post-filter caval thrombosis was 2.7%.||['qnco', 'spco', 'clna', 'dsyn', 'gngm', 'patf', 'geoa', 'ftcn', 'mnob', 'tmco', 'blor']||outcome
15||The 30-day mortality rate was 17.0% overall, higher among patients with neoplasms (19.5%) as compared with those without neoplasms (14.3%; P =.004).||['phsu', 'acty', 'spco', 'medd', 'dsyn', 'podg', 'tmco', 'gngm', 'bpoc', 'qlco', 'neop', 'fndg', 'mamm', 'qnco', 'geoa', 'inpr', 'genf', 'blor']||outcome
16||Filter efficacy and associated morbidity were not different in 46 patients with suprarenal filters.||['bpoc', 'resa', 'dsyn', 'podg', 'gngm', 'topp', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco']||outcome
17||The rate of filters placed for prophylaxis was 4.7% overall and increased to 16.4% in 1998.||['bpoc', 'acty', 'spco', 'dsyn', 'gngm', 'tmco', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr']||outcome
18||From 1980 to 1996, there was a fivefold increase in the number of caval filter implants.||['bpoc', 'spco', 'medd', 'menp', 'qlco', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco', 'plnt']||outcome
19||In recent years, more filters were implanted in younger patients.||['bpoc', 'medd', 'dsyn', 'podg', 'gngm', 'menp', 'qlco', 'qnco', 'mnob', 'tmco', 'plnt']||outcome
20||CONCLUSION.||[]||outcome
21||Inferior vena caval filters provide protection from life-threatening PE, with minimal morbidity.||['bpoc', 'gngm', 'dsyn', 'clna', 'food', 'orga', 'elii', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'mnob', 'inpr']||outcome
