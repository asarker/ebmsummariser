1||STUDY OBJECTIVES.||[]||other
2||Overnight polysomnography (ONP) is the "gold standard" for the diagnosis of sleep-disordered breathing, but it is expensive and time-consuming.||['phsu', 'tmco', 'blor', 'dsyn', 'gngm', 'orgf', 'elii', 'idcn', 'qlco', 'phsf', 'mamm', 'fndg', 'qnco', 'geoa', 'ftcn', 'inpr', 'diap']||background
3||Thus, daytime nap studies have been used as screening tests.||['dsyn', 'gngm', 'lbpr', 'qnco', 'hlca', 'ftcn', 'tmco']||background
4||If the findings of a nap study are normal or mildly abnormal, should ONP be performed?||['dsyn', 'fish', 'gngm', 'sosy', 'lbpr', 'qlco', 'fndg', 'bodm', 'qnco', 'geoa', 'ftcn', 'resa']||background
5||Do specific abnormalities in nap studies predict abnormal findings in ONP?||['phsu', 'clna', 'dsyn', 'cgab', 'fish', 'gngm', 'horm', 'sosy', 'qlco', 'fndg', 'qnco', 'ftcn', 'tmco', 'strd']||background
6||To answer these questions, we conducted this study.||['resa', 'clna', 'dsyn', 'gngm', 'elii', 'inbe', 'qnco', 'geoa', 'inpr']||other
7||DESIGN.||[]||other
8||Retrospective chart review.||['spco', 'blor', 'clna', 'qnco', 'hlca', 'inpr']||other
9||SETTING.||[]||other
10||Children's hospital.||['prog', 'hcro', 'gngm', 'mnob', 'blor']||other
11||PARTICIPANTS.||[]||other
12||One hundred forty-three children with suspected obstructive sleep apnea syndrome secondary to isolated adenotonsillar hypertrophy, who had normal or mildly abnormal nap studies, and underwent ONP.||['idcn', 'tmco', 'dsyn', 'clna', 'fish', 'gngm', 'orgf', 'lbpr', 'qlco', 'fndg', 'aggp', 'geoa', 'qnco', 'prog', 'ftcn', 'inpr', 'patf']||population
13||MEASUREMENTS AND RESULTs:||[]||other
14||We compared daytime nap and overnight polysomnograms in 143 children (52 girls; mean [+/- SD] age, 5.6 +/- 3.1 years).||['bpoc', 'acty', 'gngm', 'dsyn', 'tmco', 'orga', 'spco', 'fndg', 'qnco', 'prog', 'inpr', 'diap', 'aggp']||other
15||Total sleep time was 1 h in daytime nap, and 5.1 +/- 1.3 h in ONP.||['gngm', 'tmco', 'orgf', 'qnco']||other
16||The interval between the two studies was 5.9 +/- 4.8 months.||['geoa', 'qlco', 'tmco', 'gngm', 'qnco']||outcome
17||The findings of 59% of the nap studies were mildly abnormal, while 66% of overnight studies were abnormal.||['spco', 'fish', 'gngm', 'sosy', 'lbpr', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
18||No individual nap study parameter (including short obstructive apneas, hypopneas, hypoxemia, hypoventilation, snoring, paradoxical breathing, gasping, retractions) had good sensitivity at predicting abnormal overnight polysomnograms, but most had good specificity and positive predictive value.||['popg', 'dsyn', 'fish', 'gngm', 'horm', 'elii', 'sosy', 'bacs', 'aapp', 'qnco', 'inpr', 'phsu', 'acty', 'spco', 'blor', 'phsf', 'mamm', 'geoa', 'ftcn', 'diap', 'clna', 'topp', 'fndg', 'patf', 'bpoc', 'resa', 'qlco', 'tmco', 'strd']||outcome
19||CONCLUSIONS.||[]||outcome
20||We conclude that individual nap study parameters are not very sensitive in predicting abnormal ONP findings.||['phsu', 'spco', 'popg', 'dsyn', 'clna', 'fish', 'gngm', 'horm', 'sosy', 'medd', 'inpr', 'qlco', 'fndg', 'bodm', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'resa', 'strd']||outcome
21||However, when nap study parameters are abnormal, the chance of obstructive sleep apnea syndrome is high.||['spco', 'medd', 'clna', 'fish', 'geoa', 'orgf', 'qlco', 'fndg', 'qnco', 'prog', 'ftcn', 'resa', 'gngm', 'dsyn']||outcome
