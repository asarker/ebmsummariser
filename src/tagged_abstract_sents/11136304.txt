1||BACKGROUND.||[]||other
2||This paper is one of a series of eight systematic reviews that aim to identify effective interventions for chronic wounds.||['inpr', 'spco', 'clna', 'inpo', 'gngm', 'elii', 'idcn', 'qlco', 'geoa', 'qnco', 'hlca', 'ftcn', 'mnob', 'tmco']||background
3||Here, antimicrobial agents are evaluated.||['bpoc', 'gngm', 'dsyn', 'orga', 'lbpr', 'qlco', 'mamm', 'qnco', 'ftcn', 'chvf']||other
4||METHODS.||[]||other
5||Electronic databases and other relevant sources were accessed to identify published and unpublished material.||['ocac', 'dsyn', 'clna', 'cgab', 'fish', 'gngm', 'idcn', 'qlco', 'fndg', 'mamm', 'spco', 'sbst', 'qnco', 'geoa', 'mnob', 'inpr', 'chvf']||other
6||Studies were eligible for inclusion if they used concurrent controls, recruited participants with chronic wounds, evaluated an intervention designed to prevent or treat chronic wounds, and incorporated an objective assessment of wound healing.||['popg', 'inpo', 'gngm', 'idcn', 'qnco', 'inpr', 'evnt', 'phsu', 'acty', 'spco', 'dsyn', 'fndg', 'mamm', 'geoa', 'ftcn', 'clna', 'bpoc', 'orgf', 'qlco', 'phob', 'hlca', 'tmco']||population
7||All included studies were assessed against a comprehensive checklist for methodological quality.||['acty', 'cnce', 'dsyn', 'gngm', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'inpr', 'tmco']||other
8||A narrative overview was conducted.||['spco', 'clna', 'dsyn', 'inbe', 'qnco', 'inpr']||other
9||RESULTS.||[]||other
10||Thirty trials were included, 25 of randomized design.||['acty', 'dsyn', 'idcn', 'fndg', 'mamm', 'qnco', 'geoa', 'resa']||outcome
11||Small sample size and other methodological problems meant that findings were often difficult to interpret.||['inpr', 'spco', 'dsyn', 'gngm', 'sosy', 'idcn', 'qlco', 'fndg', 'sbst', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
12||Results do not support the routine use of systemic antibiotics for leg ulcers or diabetic foot ulcers without acute infection, but they may be useful as an adjunct to surgery for pilonidal sinuses.||['bpoc', 'tmco', 'blor', 'medd', 'cgab', 'bmod', 'gngm', 'clna', 'idcn', 'qlco', 'fndg', 'mamm', 'dsyn', 'fish', 'qnco', 'geoa', 'anab', 'ftcn', 'chvf', 'patf', 'antb']||outcome
13||Several topical preparations may be helpful, including dimethyl sulphoxide, silver sulphadiazine, benzoyl peroxide, oxyquinoline and gentamicin.||['phsu', 'spco', 'ftcn', 'qlco', 'gngm', 'elii', 'idcn', 'carb', 'irda', 'mamm', 'orch', 'qnco', 'hlca', 'geoa', 'tmco', 'antb']||outcome
14||CONCLUSION.||[]||outcome
15||Most of this research requires replication in larger, well designed studies to establish both clinical and cost effectiveness.||['bpoc', 'acty', 'hcro', 'resa', 'dsyn', 'clna', 'fish', 'gngm', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'mnob', 'inpr', 'genf', 'bsoj', 'blor']||outcome
