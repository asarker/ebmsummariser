1||STUDY OBJECTIVE.||[]||other
2||Acute cardiac ischemia (ACI) encompasses the diagnoses of unstable angina pectoris and acute myocardial infarction (AMI).||['bpoc', 'clna', 'dsyn', 'fndg', 'patf', 'geoa', 'tmco']||background
3||Accurate diagnosis and triage of patients with ACI in the emergency department should increase survival for these patients and reduce unnecessary hospital admissions.||['phsu', 'acty', 'hcro', 'spco', 'popg', 'dsyn', 'podg', 'orga', 'topp', 'hlca', 'fndg', 'orch', 'geoa', 'ftcn', 'mnob', 'orgt']||background
4||METHODS.||[]||other
5||We conducted a systematic review of the English-language literature published between 1966 and December 1998 on the accuracy and clinical effect of diagnostic technologies for ACI.||['lang', 'tmco', 'clna', 'ocac', 'orga', 'qlco', 'fndg', 'geoa', 'ftcn', 'inpr', 'dsyn']||other
6||We evaluated prospective and retrospective studies of adult patients who presented to the ED with symptoms suggesting ACI.||['dsyn', 'clna', 'podg', 'idcn', 'qlco', 'aggp', 'qnco', 'geoa', 'ftcn', 'resa']||population
7||Outcomes were diagnostic performance (test sensitivity and specificity) and measures of clinical effect.||['gngm', 'clna', 'orga', 'lbpr', 'qlco', 'fndg', 'bodm', 'qnco']||intervention
8||Meta-analyses were performed when appropriate.||['ftcn', 'bodm', 'dsyn', 'qlco']||other
9||A decision and cost-effectiveness analysis was conducted that investigated various diagnostic strategies used in the diagnosis of ACI in the ED. RESULTS.||['acty', 'dsyn', 'clna', 'gngm', 'menp', 'lbpr', 'qlco', 'fndg', 'geoa', 'qnco', 'irda', 'ftcn']||other
10||We screened 6,667 abstracts, reviewed 407 full articles, and included 106 articles articles in the main analysis.||['lbpr', 'dsyn', 'clna', 'idcn', 'qlco', 'geoa', 'inpr']||other
11||Single measurements of biomarkers at presentation to the ED have low sensitivity for AMI, although they have high specificity.||['popg', 'clna', 'gngm', 'idcn', 'qlco', 'qnco', 'geoa']||other
12||Serial measurements greatly increase the sensitivity for AMI while maintaining their excellent specificity.||['popg', 'gngm', 'qlco', 'qnco', 'geoa', 'ftcn']||other
13||Diagnostic technologies to evaluate ACI in selected populations, such as electrocardiography, sestamibi perfusion imaging, and stress ECG, may have very good to excellent sensitivity; however, they have not been sufficiently studied.||['phsu', 'dsyn', 'clna', 'gngm', 'topp', 'qlco', 'fndg', 'orch', 'qnco', 'geoa', 'ftcn', 'diap']||other
14||The Goldman Chest Pain Protocol has good sensitivity (about 90%) for AMI but has not been shown to result in any differences in hospitalization rate, length of stay, or estimated costs in the single clinical effect study performed.||['resa', 'acty', 'hcro', 'tmco', 'popg', 'dsyn', 'clna', 'orga', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr']||background
15||Its applicability to patients with unstable angina pectoris has not been evaluated.||['geoa', 'ftcn', 'clna', 'podg', 'dsyn']||background
16||The use of an Acute Cardiac Ischemia-Time-Insensitive Predictive Instrument led to the appropriate triage of 97% of patients with ACI presenting to the ED and reduced unnecessary hospitalizations.||['phsu', 'hcro', 'popg', 'dsyn', 'clna', 'podg', 'orga', 'topp', 'idcn', 'qlco', 'hlca', 'orch', 'qnco', 'geoa', 'ftcn', 'mnob']||background
17||CONCLUSION.||[]||other
18||Many of the current technologies remain underevaluated, especially regarding their clinical effect.||['dsyn', 'clna', 'gngm', 'qlco', 'geoa', 'ftcn', 'tmco']||outcome
19||The extent to which combinations of tests may provide better accuracy than any single test needs further study.||['spco', 'popg', 'dsyn', 'food', 'lbpr', 'qlco', 'geoa', 'mnob', 'resa']||outcome
