1||OBJECTIVE.||[]||other
2||To determine whether recurrences of urinary tract infection can be prevented with cranberry-lingonberry juice or with Lactobacillus GG drink.||['bpoc', 'phsu', 'dsyn', 'clna', 'acty', 'food', 'gngm', 'elii', 'prog', 'bact', 'qlco', 'orch', 'qnco', 'inch', 'geoa', 'tmco', 'phpr', 'evnt']||background
3||Design:||['fndg']||other
4||Open, randomised controlled 12 month follow up trial.||['inpr', 'clna', 'dsyn', 'gngm', 'lbpr', 'qlco', 'tmco', 'mamm', 'qnco', 'geoa', 'ftcn', 'resa']||other
5||SETTING.||[]||other
6||Health centres for university students and staff of university hospital.||['bpoc', 'hcro', 'blor', 'gngm', 'qlco', 'fndg', 'geoa', 'qnco', 'prog', 'mnob', 'orgt']||other
7||PARTICIPANTS.||[]||other
8||150 women with urinary tract infection caused by Escherichia coli randomly allocated into three groups.||['bpoc', 'acty', 'cnce', 'popg', 'dsyn', 'clna', 'gngm', 'elii', 'topp', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'ftcn', 'inpr', 'resa']||population
9||Interventions: 50 ml of cranberry-lingonberry juice concentrate daily for six months or 100 ml of lactobacillus drink five days a week for one year, or no intervention.||['phsu', 'acty', 'clna', 'qlco', 'food', 'gngm', 'bpoc', 'elii', 'hlca', 'bact', 'bodm', 'prog', 'orch', 'qnco', 'inch', 'geoa', 'tmco', 'mamm']||other
10||Main outcome measure:||['idcn', 'geoa', 'qnco', 'ftcn']||other
11||First recurrence of symptomatic urinary tract infection, defined as bacterial growth &gt;/=10(5 )colony forming units/ml in a clean voided midstream urine specimen.||['popg', 'dsyn', 'gngm', 'elii', 'idcn', 'bact', 'qnco', 'acty', 'bdsu', 'mamm', 'sbst', 'geoa', 'ftcn', 'cnce', 'clna', 'bodm', 'bpoc', 'orgf', 'mobd', 'lbpr', 'qlco', 'plnt', 'phpr']||other
12||RESULTS.||[]||other
13||The cumulative rate of first recurrence of urinary tract infection during the 12 month follow up differed significantly between the groups (P=0.048).||['bpoc', 'acty', 'spco', 'popg', 'dsyn', 'clna', 'tmco', 'gngm', 'elii', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'inpr', 'phpr']||outcome
14||At six months, eight (16%) women in the cranberry group, 19 (39%) in the lactobacillus group, and 18 (36%) in the control group had had at least one recurrence.||['phsu', 'acty', 'popg', 'clna', 'food', 'gngm', 'idcn', 'qlco', 'bact', 'grup', 'orch', 'qnco', 'geoa', 'tmco', 'phpr']||outcome
15||This is a 20% reduction in absolute risk in the cranberry group compared with the control group (95% confidence interval 3% to 36%, P=0.023, number needed to treat=5, 95% confidence interval 3 to 34).||['phsu', 'acty', 'popg', 'dsyn', 'clna', 'chvs', 'fish', 'gngm', 'menp', 'elii', 'idcn', 'qlco', 'fndg', 'grup', 'food', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'npop']||outcome
16||CONCLUSION.||[]||outcome
17||Regular drinking of cranberry juice but not lactobacillus seems to reduce the recurrence of urinary tract infection.||['bpoc', 'acty', 'dsyn', 'clna', 'food', 'gngm', 'elii', 'prog', 'bact', 'fndg', 'orch', 'qnco', 'inch', 'geoa', 'ftcn', 'inbe', 'phpr', 'phsu']||outcome
