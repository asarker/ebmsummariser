1||OBJECTIVE.||[]||other
2||Our goal was to evaluate whether screening patients with diabetes for microalbuminuria (MA) is effective according to the criteria developed by Frame and Carlson and those of the US Preventive Services Task Force. STUDY DESIGN.||['clna', 'dsyn', 'podg', 'hlca', 'lbpr', 'qlco', 'fndg', 'geoa', 'ftcn', 'inpr', 'evnt']||background
3||We searched the MEDLINE database (1966-present) and bibliographies of relevant articles.||['acty', 'inpr', 'dsyn', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco', 'chvf', 'bsoj']||background
4||OUTCOMES MEASURED.||[]||other
5||We evaluated the impact of MA screening using published criteria for periodic health screening tests.||['lbpr', 'ocac', 'dsyn', 'clna', 'idcn', 'hlca', 'tmco', 'geoa', 'ftcn', 'inpr']||other
6||The effect of the correlation between repeated tests on the accuracy of a currently recommended testing strategy was analyzed.||['acty', 'spco', 'popg', 'dsyn', 'clna', 'menp', 'lbpr', 'qlco', 'tmco', 'idcn', 'geoa', 'ftcn', 'resa', 'famg']||outcome
7||RESULTS.||[]||outcome
8||Quantitative tests have reported sensitivities from 56% to 100% and specificities from 81% to 98%.||['dsyn', 'gngm', 'lbpr', 'qlco', 'fndg', 'inpr']||outcome
9||Semiquantitative tests for MA have reported sensitivities from 51% to 100% and specificities from 21% to 100%.||['dsyn', 'gngm', 'lbpr', 'qlco', 'fndg', 'qnco', 'inpr']||outcome
10||First morning, morning, or random urine sampling appear feasible.||['resa', 'tmco', 'gngm', 'diap', 'bdsu']||outcome
11||Assuming an individual test sensitivity of 90%, a specificity of 90%, and a 10% prevalence of MA, the correlation between tests would have to be lower than 0.1 to achieve a positive predictive value for repeated testing of 75%.||['lbpr', 'tmco', 'popg', 'dsyn', 'clna', 'gngm', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'inpr', 'famg']||outcome
12||CONCLUSIONS.||[]||other
13||Screening for MA meets only 4 of 6 Frame and Carlson criteria for evaluating screening tests.||['hlca', 'lbpr', 'inpr', 'ftcn']||outcome
14||The recommended strategies to overcome diagnostic uncertainty by using repeated testing are based on expert opinion, are difficult to follow in primary care settings, do not improve diagnostic accuracy sufficiently, and have not been tested in a controlled trial.||['resa', 'acty', 'spco', 'popg', 'dsyn', 'gngm', 'menp', 'lbpr', 'qlco', 'fndg', 'idcn', 'hlca', 'ftcn', 'inpr', 'chvf']||outcome
15||Although not advocated by the American Diabetes Association, semiquantitative MA screening tests using random urine sampling have acceptable accuracy but may not be reliable in all settings.||['lbpr', 'gngm', 'hlca', 'dsyn', 'ocdi', 'orga', 'menp', 'bdsu', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'resa', 'diap']||outcome
