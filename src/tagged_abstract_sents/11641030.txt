1||The aim of the study was to determine the effect of a low oral dose of erythromycin on whole gastrointestinal transit time [WGTT].||['bpoc', 'idcn', 'spco', 'blor', 'clna', 'tmco', 'gngm', 'sosy', 'lbpr', 'qlco', 'neop', 'fndg', 'orch', 'qnco', 'geoa', 'resa', 'diap', 'antb']||population
2||Erythromycin [EM] [1.5 mg/kg, 6 hourly] or placebo was given first over 7 days in a double blind randomized crossover study of 21 preterm infants with feed intolerance.||['bpoc', 'spco', 'resa', 'clna', 'dsyn', 'gngm', 'orgf', 'topp', 'qlco', 'neop', 'fndg', 'aggp', 'geoa', 'qnco', 'hlca', 'ftcn', 'tmco', 'blor']||intervention
3||Median [range] birth weight was 1420 [690, 2200] g and postconceptual age 32. 5 [20, 36.4] weeks.||['spco', 'blor', 'dsyn', 'orga', 'orgf', 'idcn', 'genf', 'mamm', 'qnco', 'geoa', 'tmco']||other
4||WGTT was assessed on day 3 of each treatment, by timing the transit of carmine red through the gut.||['acty', 'bdsy', 'tmco', 'popg', 'blor', 'dsyn', 'gngm', 'qlco', 'fndg', 'mamm', 'orch', 'qnco', 'irda', 'geoa', 'ftcn']||other
5||Treatments were compared using Student's paired t test.||['bpoc', 'acty', 'popg', 'dsyn', 'gngm', 'lbpr', 'qlco', 'fndg', 'sbst', 'qnco', 'geoa', 'ftcn']||other
6||RESULTS:||[]||other
7||WGTT was significantly shorter following EM treatment as compared to placebo: mean [SD] 10.16 [4.6] h vs. 15. 9 [7.2] h, p&lt;0.01.||['acty', 'spco', 'popg', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'inpr', 'genf']||outcome
8||CONCLUSION:||[]||outcome
9||Oral low-dose EM significantly shortens WGTT of feed-intolerant preterm infants.||['bpoc', 'clna', 'dsyn', 'gngm', 'idcn', 'qlco', 'fndg', 'aggp', 'qnco', 'hlca', 'ftcn', 'tmco', 'genf', 'blor']||outcome
