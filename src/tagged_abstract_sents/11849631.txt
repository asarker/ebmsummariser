1||OBJECTIVE.||[]||other
2||To present efficacy and cycle control data pooled from three pivotal studies of the contraceptive patch (Ortho Evra/Evra).||['phsu', 'cnce', 'popg', 'dsyn', 'gngm', 'bdsu', 'idcn', 'qlco', 'resa', 'qnco', 'geoa', 'ftcn', 'tmco']||background
3||DESIGN.||[]||other
4||Three multicenter, open-label, contraceptive studies that included up to 13 treatment cycles.Setting: 183 centers.||['phsu', 'spco', 'popg', 'dsyn', 'fish', 'gngm', 'idcn', 'geoa', 'inpr', 'qnco', 'inch', 'ftcn', 'tmco']||other
5||PATIENT(S). 3,319 women.Intervention(s): Three consecutive 7-day patches (21 days) with 1 patch-free week per cycle.||['popg', 'dsyn', 'gngm', 'bdsu', 'qlco', 'bodm', 'qnco', 'geoa', 'tmco']||other
6||MAIN OUTCOME MEASURE(S).||[]||other
7||Contraceptive efficacy and cycle control.||['resa', 'qlco', 'tmco', 'popg', 'qnco']||other
8||RESULT(S).||[]||other
9||Overall and method failure life-table estimates of contraceptive failure through 13 cycles were 0.8% (95% CI, 0.3%-1.3%) and 0.6% (95% CI, 0.2%-0.9%), respectively.||['phsu', 'tmco', 'popg', 'socb', 'clna', 'fish', 'orga', 'idcn', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr']||outcome
10||Corresponding Pearl indices were 0.88 (95% CI, 0.44-1.33) and 0.7 (95% CI, 0.31-1.10).||['inch', 'ftcn', 'bhvr', 'clna', 'qnco']||outcome
11||Contraceptive failure among women with a body weight &lt; 90 kg (&lt;198 lb) was low and uniformly distributed across the weight range.||['orga', 'popg', 'dsyn', 'gngm', 'anst', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn']||outcome
12||A subgroup of women with body weight &gt; or = 90 kg (&gt; or = 198 lb) may have increased risk of pregnancy.||['orga', 'popg', 'dsyn', 'orgf', 'gngm', 'anst', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'clas']||outcome
13||The incidence of breakthrough bleeding was low and decreased over time. CONCLUSION(S).||['spco', 'dsyn', 'gngm', 'qlco', 'fndg', 'qnco', 'geoa', 'tmco']||outcome
14||In contraceptive patch users, the overall annual probability of pregnancy was 0.8% and the method failure probability was 0.6%.||['phsu', 'spco', 'popg', 'ftcn', 'fish', 'gngm', 'orgf', 'bdsu', 'idcn', 'tmco', 'orga', 'qnco', 'geoa', 'humn', 'inpr']||outcome
15||The efficacy of the patch was high and similar across age and racial groups.||['orga', 'popg', 'gngm', 'bdsu', 'idcn', 'qlco', 'mamm', 'geoa', 'resa']||outcome
16||Among women &lt; 90 kg (&lt;198 lb), contraceptive failure was low and uniformly distributed across the range of body weights.||['phsu', 'gngm', 'popg', 'dsyn', 'orga', 'anst', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn']||outcome
17||In women &gt; or = 90 kg (&gt; or = 198 lb), contraceptive failures may be increased.||['phsu', 'popg', 'dsyn', 'gngm', 'qnco', 'geoa', 'ftcn']||outcome
18||Efficacy and cycle control have been shown to be comparable to an established oral contraceptive.||['phsu', 'spco', 'popg', 'dsyn', 'fish', 'gngm', 'inpr', 'qlco', 'orga', 'qnco', 'tmco']||outcome
