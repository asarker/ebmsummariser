1||BACKGROUND.||[]||other
2||Intraamniotic infection is associated with maternal morbidity and neonatal sepsis, pneumonia and death.||['dsyn', 'clna', 'gngm', 'orgf', 'qlco', 'aggp', 'qnco', 'geoa']||background
3||Although antibiotic treatment is accepted as the standard of care, few studies have been conducted to examine the effectiveness of different antibiotic regimens for this infection and whether to administer antibiotics intrapartum or postpartum.||['spco', 'popg', 'clna', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'tmco', 'antb']||background
4||OBJECTIVES.||[]||other
5||To study the effects of different maternal antibiotic regimens for intraamniotic infection on maternal and perinatal morbidity and mortality.||['spco', 'popg', 'dsyn', 'clna', 'gngm', 'topp', 'qlco', 'tmco', 'qnco', 'geoa', 'resa', 'antb']||other
6||SEARCH STRATEGY.||[]||other
7||We searched the Cochrane Pregnancy and Childbirth Group trials register (May 2002) and the Cochrane Controlled Trials Register (The Cochrane Library, Issue 2, 2002).||['acty', 'dsyn', 'orgf', 'idcn', 'geoa', 'resa', 'bsoj']||other
8||SELECTION CRITERIA.||[]||other
9||Trials where there was a randomized comparison of different antibiotic regimens to treat women with a diagnosis of intraamniotic infection were included.||['acty', 'spco', 'popg', 'clna', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'geoa', 'ftcn', 'resa', 'antb']||other
10||The primary outcome was perinatal morbidity.||['qlco', 'tmco', 'gngm', 'ftcn', 'qnco']||other
11||DATA COLLECTION AND ANALYSIS.||[]||other
12||Data were extracted from each publication independently by the authors.||['bpoc', 'spco', 'dsyn', 'clna', 'geoa', 'medd', 'idcn', 'qlco', 'prog', 'inpr', 'mnob', 'topp']||other
13||MAIN RESULTS.||[]||other
14||Two eligible trials (181 women) were included in this review.||['inpr', 'popg', 'dsyn', 'idcn', 'geoa', 'resa']||other
15||No trials were identified that compared antibiotic treatment with no treatment.||['acty', 'popg', 'dsyn', 'qlco', 'geoa', 'ftcn', 'resa', 'antb']||outcome
16||Intrapartum treatment with antibiotics for intraamniotic infection was associated with a reduction in neonatal sepsis (relative risk (RR) 0.08; 95% confidence interval (CI) 0.00, 1.44) and pneumonia (RR 0.15; CI 0.01, 2.92) compared with treatment given immediately postpartum, but these results did not reach statistical significance (number of women studied = 45).||['phsu', 'acty', 'spco', 'popg', 'clna', 'dsyn', 'ocdi', 'qnco', 'idcn', 'qlco', 'fndg', 'famg', 'aggp', 'orch', 'npop', 'geoa', 'ftcn', 'tmco', 'gngm', 'antb']||outcome
17||There was no difference in the incidence of maternal bacteremia (RR 2.19; CI 0.25, 19.48).||['geoa', 'qlco', 'gngm', 'clna', 'qnco']||outcome
18||There was no difference in the outcomes of neonatal sepsis (RR 2.16; CI 0.20, 23.21) or neonatal death (RR 0.72; CI 0.12, 4.16) between a regimen with and without anaerobic activity (number of women studied = 133).||['acty', 'popg', 'clna', 'dsyn', 'qnco', 'orgf', 'topp', 'qlco', 'fndg', 'aggp', 'patf', 'geoa', 'ftcn']||outcome
19||There was a trend towards a decrease in the incidence of post-partum endometritis in women who received treatment with ampicillin, gentamicin and clindamycin compared with ampicillin and gentamicin alone, but this did not reach statistical significance (RR 0.54; CI 0.19, 1.49).||['acty', 'spco', 'popg', 'dsyn', 'ocdi', 'gngm', 'carb', 'qlco', 'fndg', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'antb']||outcome
20||REVIEWER'S CONCLUSIONS.||[]||other
21||The conclusions that can be drawn from this meta-analysis are limited due to the small number of studies.||['idcn', 'dsyn', 'euka', 'gngm', 'lbpr', 'qnco', 'geoa', 'ftcn']||outcome
22||For none of the outcomes was a statistically significant difference seen between the different interventions.||['acty', 'hlca', 'ocdi', 'gngm', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn']||outcome
23||Current consensus is for the intrapartum administration of antibiotics when the diagnosis of intraamniotic infection is made; however, the results of this review neither support nor refute this although there was a trend towards improved neonatal outcomes when antibiotics were administered intrapartum.||['spco', 'medd', 'clna', 'tmco', 'gngm', 'idcn', 'qlco', 'fndg', 'antb', 'aggp', 'qnco', 'geoa', 'ftcn', 'inpr', 'ocac', 'dsyn', 'socb']||outcome
24||No recommendations can be made on the most appropriate antimicrobial regimen to choose to treat intraamniotic infection.||['acty', 'spco', 'popg', 'dsyn', 'clna', 'lbpr', 'gngm', 'topp', 'idcn', 'qlco', 'geoa', 'ftcn']||outcome
