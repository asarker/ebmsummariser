1||OBJECTIVE.||[]||other
2||To determine, for a woman aged &gt;45 years, whether selected hormonal, health status, and demographic measures are related to the time to final menstrual period (FMP) from a point where 6 weeks separate the shortest and longest cycles experienced to date.||['bpoc', 'acty', 'orga', 'popg', 'dsyn', 'clna', 'menp', 'tmco', 'gngm', 'orgf', 'lbpr', 'qlco', 'spco', 'mamm', 'orch', 'idcn', 'geoa', 'inpr', 'genf', 'qnco']||background
3||DESIGN.||[]||other
4||Cohort study.||['genf', 'resa', 'qnco']||other
5||SETTING.||[]||other
6||Volunteers in an academic research environment.||['phsu', 'bpoc', 'spco', 'popg', 'gngm', 'elii', 'topp', 'idcn', 'qlco', 'bsoj', 'inpr', 'resa', 'orgt', 'bacs']||other
7||PATIENT(S).||[]||other
8||Ninety-nine menstruating women aged 46 years to 55 years on entry completed menstrual diaries, gave annual blood samples, and were interviewed annually.||['bpoc', 'acty', 'spco', 'popg', 'clna', 'dsyn', 'orga', 'orgf', 'qlco', 'tmco', 'mamm', 'tisu', 'sbst', 'qnco', 'geoa', 'ftcn', 'inpr', 'gngm']||other
9||They were observed for a mean period of 1.5 years.||['bpoc', 'dsyn', 'qnco', 'geoa', 'ftcn', 'tmco']||other
10||Seventy-seven reached FMP during observation.||['spco', 'dsyn', 'gngm', 'elii', 'qlco', 'qnco', 'resa', 'evnt']||other
11||INTERVENTION(S).||[]||other
12||None.||['qnco']||other
13||MAIN OUTCOME MEASURE(S).||[]||other
14||Time to reach FMP from the date of a marker event-the difference between the longest and shortest of recent cycles reaching 6 weeks.||['tmco', 'clna', 'dsyn', 'gngm', 'orgf', 'sosy', 'lbpr', 'qlco', 'qnco', 'geoa', 'inpr', 'genf', 'evnt']||other
15||RESULT(S).||[]||other
16||Women self-rating as in the transition have a greater "hazard" or probability of FMP at any time after the marker event.||['acty', 'orga', 'popg', 'clna', 'fish', 'gngm', 'elii', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'tmco', 'comd', 'evnt']||outcome
17||Allowing for this and other covariates, the hazard is estimated to increase by 30% (confidence interval [CI]: 10%, 60%) for each year of age and 50% (CI: 7%, 118%) for each unit increase in log FSH, measured at the time of the marker event. CONCLUSION(S).||['bpoc', 'idcn', 'gngm', 'dsyn', 'clna', 'tmco', 'orga', 'menp', 'lbpr', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'inpr', 'evnt']||outcome
18||For women aged &gt;45 years, the time remaining in the menopausal transition from the day on which &gt;or=6 weeks separate the longest and shortest recent cycles is related to self-rating of menopausal status and to serum FSH level.||['bpoc', 'acty', 'orga', 'popg', 'clna', 'dsyn', 'gngm', 'elii', 'bdsu', 'comd', 'lbpr', 'qlco', 'phsf', 'mamm', 'spco', 'orch', 'qnco', 'geoa', 'tmco', 'genf', 'fndg']||outcome
19||The median number of months remaining ranges from 11 for those with FSH of &gt;20 IU/L and who see themselves as in transition to 21 months for those with lower FSH and who notice little evidence of being in transition.||['clna', 'acty', 'spco', 'medd', 'dsyn', 'gngm', 'elii', 'mobd', 'inpr', 'qlco', 'fndg', 'mamm', 'geoa', 'qnco', 'inch', 'ftcn', 'tmco', 'comd', 'blor']||outcome
