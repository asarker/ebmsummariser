1||AIM.||[]||other
2||Estimate pneumococcal vaccine effectiveness in preventing Streptococcus pneumoniae illness in the elderly.||['phsu', 'popg', 'clna', 'dsyn', 'cgab', 'gngm', 'topp', 'qlco', 'bact', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'tmco', 'imft', 'evnt']||other
3||DESIGN.||[]||other
4||Systematic review and meta-analysis.||['bpoc', 'spco', 'euka', 'gngm', 'mobd', 'lbpr', 'qnco', 'inpr', 'plnt']||other
5||DATA SOURCE.||[]||other
6||MEDLINE, years 1964 to the 2000; EMBASE, from 1988 to the 2000; Cochrane Library, identified studies and previously published systematic reviews citations peruse, and contacts with field experts.||['popg', 'dsyn', 'gngm', 'elii', 'bacs', 'aapp', 'qnco', 'inpr', 'acty', 'spco', 'ocac', 'geoa', 'ftcn', 'cnce', 'clna', 'topp', 'bpoc', 'mobd', 'qlco', 'genf', 'tmco', 'plnt']||other
7||STUDY SELECTION.||[]||other
8||Clinical trials, cohort and case-control studies, published in Spanish, English or French, that estimated pneumococcal disease rates in vaccinated or not vaccinated elderly.||['acty', 'resa', 'popg', 'ocac', 'dsyn', 'cgab', 'gngm', 'topp', 'lbpr', 'qlco', 'bact', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'inpr', 'genf']||other
9||DATA EXTRACTION.||[]||other
10||The studies were valued independently by four investigators with predefined criteria of validity, such as results comparing rates of disease caused by serotypes included in the vaccine, random allocation, double blind design, included subjects pertaining to the same study base, and losses of less than 10% in clinical trials and 20% in observational studies.||['dsyn', 'gngm', 'horm', 'elii', 'idcn', 'grup', 'qnco', 'resa', 'imft', 'phsu', 'acty', 'spco', 'blor', 'medd', 'inpr', 'fndg', 'mamm', 'geoa', 'ftcn', 'chvf', 'diap', 'hcro', 'cnce', 'clna', 'cgab', 'prog', 'topp', 'inch', 'mnob', 'bpoc', 'lbpr', 'qlco', 'genf', 'tmco', 'strd']||outcome
11||RESULTS.||[]||outcome
12||Eight clinical trials considered the relative risk (RR) of pneumococcal pneumonia, three did not make estimations on pneumonia originated by serotypes included in the vaccine and only one study fulfilled all the inclusion criteria.||['dsyn', 'gngm', 'elii', 'idcn', 'bact', 'qnco', 'inpr', 'imft', 'famg', 'phsu', 'acty', 'spco', 'menp', 'fndg', 'mamm', 'geoa', 'ftcn', 'hcro', 'clna', 'cgab', 'topp', 'mnob', 'bpoc', 'resa', 'lbpr', 'qlco', 'tmco']||outcome
13||Vaccinated versus not vaccinated pneumococcal pneumonia RR was 0.86 (95%CI, 0.24 to 2.99).||['dsyn', 'cgab', 'gngm', 'topp', 'idcn', 'bact', 'fndg', 'mamm', 'ftcn']||outcome
14||Vaccine effectiveness was 14% (95%CI, -199 to 76%).||['qnco', 'qlco', 'gngm', 'clna', 'cgab']||outcome
15||Ten studies performed estimations on the effectiveness of the vaccine on invasive disease by vaccine serotypes.||['bpoc', 'phsu', 'dsyn', 'clna', 'cgab', 'gngm', 'menp', 'elii', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn', 'inpr', 'imft', 'diap']||outcome
16||Of these, two clinical trials and two observational studies fulfilled the required quality criteria.||['acty', 'hcro', 'resa', 'clna', 'dsyn', 'elii', 'lbpr', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr', 'diap']||outcome
17||RR of invasive disease was of 0.68 (95%CI, 0.39-1.18); vaccine effectiveness was 32% (95%CI, 18-61%).||['phsu', 'clna', 'dsyn', 'cgab', 'gngm', 'qlco', 'qnco', 'inpr', 'imft', 'diap']||outcome
18||CONCLUSIONS.||[]||other
19||No evidence was found supporting pneumococcal vaccine effectiveness to reduce or avoid S. pneumoniae disease in the elderly.||['phsu', 'spco', 'popg', 'medd', 'dsyn', 'cgab', 'gngm', 'clna', 'topp', 'idcn', 'qlco', 'bact', 'fndg', 'mamm', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr', 'imft', 'genf']||outcome
