1||OBJECTIVE.||[]||other
2||To determine if administration of albuterol by a metered-dose inhaler with a spacer device is as efficacious as administration of albuterol by nebulizer to treat wheezing in children aged 2 years and younger.||['phsu', 'bpoc', 'spco', 'medd', 'dsyn', 'tmco', 'orga', 'ocac', 'sosy', 'ftcn', 'prog', 'fndg', 'aggp', 'orch', 'qnco', 'inch', 'geoa', 'inpr', 'gngm', 'blor']||background
3||DESIGN.||[]||other
4||Double-blind, randomized, placebo-controlled clinical trial.||['resa', 'hcro', 'spco', 'dsyn', 'topp', 'qlco', 'mamm', 'qnco', 'ftcn', 'mnob', 'inpr']||other
5||SETTING.||[]||other
6||Pediatric emergency department.||['hcro', 'spco', 'popg', 'dsyn', 'gngm', 'qlco', 'mamm', 'orgt', 'geoa', 'mnob', 'phpr']||other
7||PATIENTS.||[]||other
8||From a convenience sample of wheezing children aged 2 to 24 months, 85 patients were enrolled in the nebulizer group and 83 in the spacer group.||['bpoc', 'orga', 'popg', 'medd', 'dsyn', 'podg', 'tmco', 'gngm', 'sosy', 'idcn', 'qlco', 'spco', 'aggp', 'sbst', 'qnco', 'prog', 'geoa', 'inpr', 'clas']||population
9||INTERVENTIONS.||[]||other
10||The nebulizer group received a placebo metered-dose inhaler with a spacer followed by nebulized albuterol.||['phsu', 'spco', 'popg', 'medd', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'orch', 'qnco', 'geoa', 'inpr', 'blor']||intervention
11||The spacer group received albuterol by a metered-dose inhaler with a spacer followed by nebulized isotonic sodium chloride solution.||['phsu', 'spco', 'popg', 'medd', 'dsyn', 'sbst', 'gngm', 'elii', 'idcn', 'qlco', 'fndg', 'bacs', 'orch', 'qnco', 'inch', 'geoa', 'inpr', 'blor']||other
12||Treatments were given every 20 minutes by a single investigator blinded to group assignment.||['bpoc', 'tmco', 'popg', 'dsyn', 'food', 'gngm', 'idcn', 'qlco', 'fndg', 'qnco', 'prog', 'ftcn', 'resa', 'clas']||other
13||MAIN OUTCOME MEASURES.||[]||other
14||The primary outcome was admission rate.||['hlca', 'qlco', 'cnce', 'ftcn', 'acty']||other
15||Pulmonary Index score and oxygen saturation were measured initially and 10 minutes after each treatment.||['phsu', 'spco', 'popg', 'dsyn', 'food', 'moft', 'gngm', 'elii', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'tmco', 'phpr']||other
16||RESULTS.||[]||outcome
17||The nebulizer group had a significantly higher mean (SD) initial Pulmonary Index score compared with the spacer group (7.6 [2.5] vs 6.6 [2.0]; P =.002).||['acty', 'spco', 'popg', 'medd', 'dsyn', 'gngm', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'tmco']||outcome
18||With the initial Pulmonary Index score controlled, children in the spacer group were admitted less (5% vs 20%; P =.05).||['spco', 'popg', 'medd', 'dsyn', 'gngm', 'idcn', 'qlco', 'tmco', 'mamm', 'prog', 'geoa', 'qnco', 'hlca', 'ftcn', 'inpr', 'aggp']||outcome
19||Analyses also revealed an interaction between group and initial Pulmonary Index score; lower admission rates in the spacer group were found primarily in children having a more severe asthma exacerbation.||['inpr', 'acty', 'cnce', 'popg', 'dsyn', 'clna', 'geoa', 'medd', 'idcn', 'qlco', 'hlca', 'mamm', 'spco', 'qnco', 'prog', 'ftcn', 'tmco', 'gngm', 'clas', 'aggp']||outcome
20||CONCLUSION.||[]||other
21||Our data suggest that metered-dose inhalers with spacers may be as efficacious as nebulizers for the emergency department treatment of wheezing in children aged 2 years or younger.||['popg', 'dsyn', 'gngm', 'sosy', 'idcn', 'qnco', 'inpr', 'spco', 'blor', 'medd', 'fndg', 'geoa', 'ftcn', 'hcro', 'prog', 'mnob', 'bpoc', 'orga', 'orgt', 'qlco', 'aggp', 'tmco', 'phpr']||outcome
