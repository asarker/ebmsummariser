1||BACKGROUND.||[]||other
2||Type 2 diabetes mellitus is associated with a heavy burden of suffering.||['dsyn', 'fish', 'gngm', 'mobd', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa']||background
3||Screening for diabetes is controversial.||['fish', 'gngm', 'mamm', 'spco', 'dsyn']||background
4||PURPOSE.||[]||other
5||To examine the evidence that screening and earlier treatment are effective in reducing morbidity and mortality associated with diabetes.||['phsu', 'popg', 'clna', 'dsyn', 'fish', 'gngm', 'hlca', 'lbpr', 'qlco', 'mamm', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr']||background
6||DATA SOURCES.||[]||other
7||MEDLINE, the Cochrane Library, reviews, and experts, all of which addressed key questions about screening.||['spco', 'dsyn', 'fish', 'gngm', 'lbpr', 'hlca', 'qnco', 'geoa', 'inpr']||other
8||STUDY SELECTION.||[]||other
9||Studies that provided information about the existence and length of an asymptomatic phase of diabetes; studies that addressed the accuracy and reliability of screening tests; and randomized, controlled trials with health outcomes for various treatment strategies were selected.||['popg', 'dsyn', 'fish', 'gngm', 'idcn', 'aapp', 'qnco', 'irda', 'inpr', 'imft', 'phsu', 'acty', 'spco', 'biof', 'menp', 'fndg', 'mamm', 'geoa', 'ftcn', 'clna', 'cgab', 'food', 'bodm', 'resa', 'orga', 'lbpr', 'qlco', 'hlca', 'tmco']||other
10||DATA EXTRACTION.||[]||other
11||Two reviewers abstracted relevant information using standardized abstraction forms and graded articles according to U.S.||['bpoc', 'phsu', 'spco', 'biof', 'clna', 'dsyn', 'acty', 'fish', 'cgab', 'topp', 'idcn', 'qlco', 'bodm', 'aapp', 'qnco', 'ftcn', 'mnob', 'inpr', 'imft', 'clas']||other
12||Preventive Services Task Force criteria.||['gngm', 'mamm', 'qnco', 'inch', 'inpr', 'evnt']||other
13||DATA SYNTHESIS.||[]||other
14||No randomized, controlled trial of screening for diabetes has been performed.||['dsyn', 'fish', 'gngm', 'lbpr', 'qlco', 'bodm', 'mamm', 'hlca', 'ftcn', 'resa']||other
15||Type 2 diabetes mellitus includes an asymptomatic preclinical phase; the length of this phase is unknown.||['hcro', 'dsyn', 'fish', 'gngm', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco']||outcome
16||Screening tests can detect diabetes in its preclinical phase.||['hcro', 'dsyn', 'clna', 'fish', 'gngm', 'lbpr', 'qlco', 'mamm', 'qnco', 'mnob', 'tmco']||outcome
17||Over the 10 to 15 years after clinical diagnosis, tight glycemic control probably reduces the risk for blindness and end-stage renal disease, and aggressive control of hypertension, lipid therapy, and aspirin use reduce cardiovascular events.||['lipd', 'dsyn', 'fish', 'gngm', 'idcn', 'qnco', 'resa', 'evnt', 'phsu', 'bdsy', 'spco', 'fndg', 'mamm', 'geoa', 'ftcn', 'hcro', 'mnob', 'bpoc', 'orga', 'mobd', 'qlco', 'orch', 'tmco']||outcome
18||The magnitude of the benefit is larger for cardiovascular risk reduction than for tight glycemic control.||['phsu', 'bdsy', 'dsyn', 'clna', 'gngm', 'qlco', 'fndg', 'qnco', 'orch', 'npop', 'geoa']||outcome
19||The additional benefit of starting these treatments in the preclinical phase, after detection by screening, is uncertain but is probably also greater for cardiovascular risk reduction.||['phsu', 'idcn', 'bdsy', 'hlca', 'popg', 'clna', 'dsyn', 'fish', 'gngm', 'hcro', 'topp', 'lbpr', 'qlco', 'fndg', 'qnco', 'orch', 'npop', 'geoa', 'ftcn', 'mnob', 'tmco']||outcome
20||CONCLUSIONS.||[]||other
21||The interventions that are most clearly beneficial during the preclinical phase are those that affect the risk for cardiovascular disease.||['hcro', 'dsyn', 'clna', 'gngm', 'menp', 'bdsy', 'hlca', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'mnob', 'tmco']||outcome
22||The magnitude of additional benefit of initiating tight glycemic control during the preclinical phase is uncertain but probably small.||['hcro', 'dsyn', 'fish', 'gngm', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco']||outcome
