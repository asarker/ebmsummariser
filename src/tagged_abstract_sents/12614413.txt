1||BACKGROUND.||[]||other
2||Shave excision is a simple and quick procedure that is widely used for removal of benign naevi.||['phsu', 'spco', 'clna', 'dsyn', 'euka', 'gngm', 'elii', 'topp', 'qlco', 'aapp', 'bacs', 'orch', 'geoa', 'ftcn', 'inpr', 'comd', 'diap']||background
3||Limited published data are available on patient acceptability of this procedure or its potential cosmetic outcomes.||['dsyn', 'fish', 'gngm', 'idcn', 'bacs', 'aapp', 'qnco', 'phsu', 'spco', 'blor', 'ocac', 'mamm', 'geoa', 'ftcn', 'diap', 'clna', 'cgab', 'euka', 'topp', 'mnob', 'orga', 'mobd', 'qlco', 'orch']||background
4||OBJECTIVES.||[]||other
5||To assess the patient's satisfaction with the procedure, to assess the risk of recurrence, and to determine the patient's perception of the scar.||['phsu', 'acty', 'dsyn', 'clna', 'gngm', 'menp', 'elii', 'topp', 'qlco', 'mamm', 'phpr', 'aapp', 'qnco', 'geoa', 'ftcn', 'tmco', 'orch', 'diap', 'bacs']||other
6||METHODS.||[]||other
7||Questionnaires were sent to 93 consecutive patients who had shave excision of benign facial naevi.||['bpoc', 'tmco', 'blor', 'dsyn', 'podg', 'euka', 'gngm', 'elii', 'topp', 'ftcn', 'qlco', 'mamm', 'sbst', 'qnco', 'hlca', 'geoa', 'inpr']||other
8||RESULTS.||[]||other
9||Seventy-six patients (82%) with a total of 83 naevi responded.||['bpoc', 'spco', 'dsyn', 'podg', 'gngm', 'qlco', 'qnco', 'geoa', 'inpr', 'evnt']||outcome
10||Twenty-eight percent of naevi were reported to have recurred 12 months after shave excision.||['spco', 'dsyn', 'clna', 'euka', 'gngm', 'elii', 'topp', 'inpr', 'qlco', 'fndg', 'geoa', 'qnco', 'hlca', 'tmco', 'genf', 'phpr']||outcome
11||A significantly higher recurrence rate was found with hairy naevi (41%, P= 0.04).||['bpoc', 'acty', 'clna', 'gngm', 'idcn', 'qlco', 'fndg', 'mamm', 'sbst', 'geoa', 'ftcn', 'inpr', 'phpr']||outcome
12||More than half of the patients reported no scar or had a white and flat scar.||['bpoc', 'spco', 'popg', 'dsyn', 'podg', 'gngm', 'qlco', 'qnco', 'geoa', 'ftcn', 'inpr', 'genf']||outcome
13||Nineteen percent of scars were depressed, and 15% were raised; 7% were pigmented.||['popg', 'dsyn', 'gngm', 'qlco', 'fndg', 'qnco', 'geoa', 'tmco', 'chvf', 'patf']||outcome
14||The majority of patients were satisfied with the results.||['bpoc', 'socb', 'dsyn', 'podg', 'gngm', 'menp', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn']||outcome
15||CONCLUSIONS.||[]||other
16||Despite a high recurrence rate, most patients were satisfied with the cosmetic outcomes after shave excision of benign facial naevi.||['dsyn', 'gngm', 'elii', 'neop', 'qnco', 'inpr', 'acty', 'blor', 'menp', 'mamm', 'geoa', 'ftcn', 'clna', 'euka', 'topp', 'mnob', 'bpoc', 'podg', 'mobd', 'qlco', 'hlca', 'phpr']||outcome
17||The results of this study have helped us to provide our patients with more accurate information regarding cosmetic outcomes.||['dsyn', 'gngm', 'elii', 'idcn', 'aapp', 'qnco', 'inpr', 'imft', 'phsu', 'acty', 'resa', 'biof', 'blor', 'mamm', 'geoa', 'ftcn', 'cgab', 'food', 'bodm', 'mnob', 'bpoc', 'podg', 'mobd', 'lbpr', 'qlco']||outcome
