1||BACKGROUND.||[]||other
2||An accurate initial biopsy of the deepest portion of the melanoma is vital to the management of patients with melanomas.||['bpoc', 'acty', 'orga', 'popg', 'dsyn', 'ocac', 'cgab', 'podg', 'gngm', 'elii', 'lbpr', 'qlco', 'neop', 'spco', 'mamm', 'qnco', 'geoa', 'inpr', 'tmco', 'genf', 'diap']||background
3||OBJECTIVE.||[]||other
4||Our goal was to evaluate the accuracy of preliminary biopsies performed by a group of predominantly experienced dermatologists (n = 46/72).||['phsu', 'inpr', 'popg', 'dsyn', 'cgab', 'menp', 'gngm', 'horm', 'idcn', 'qlco', 'fndg', 'bodm', 'prog', 'geoa', 'ftcn', 'tmco', 'diap', 'strd']||other
5||METHODS.||[]||other
6||A total of 145 cases of cutaneous melanoma were examined retrospectively.||['phsu', 'spco', 'dsyn', 'clna', 'hlca', 'qlco', 'neop', 'fndg', 'orch', 'qnco', 'geoa', 'ftcn', 'blor']||other
7||We compared Breslow depth on preliminary biopsy with Breslow depth on subsequent excision.||['acty', 'spco', 'dsyn', 'euka', 'gngm', 'elii', 'topp', 'idcn', 'qlco', 'fndg', 'geoa', 'tmco', 'diap']||other
8||Was the initial diagnostic biopsy performed on the deepest part of the melanoma?||['bpoc', 'spco', 'blor', 'dsyn', 'tmco', 'gngm', 'mobd', 'genf', 'neop', 'fndg', 'bodm', 'qnco', 'geoa', 'mamm', 'ftcn', 'diap']||other
9||RESULTS.||[]||outcome
10||Of nonexcisional initial shave and punch biopsies, 88% were accurate, with Breslow depth greater than or equal to subsequent excision Breslow depth.||['qnco', 'acty', 'spco', 'dsyn', 'medd', 'cgab', 'euka', 'gngm', 'elii', 'topp', 'lbpr', 'qlco', 'mamm', 'geoa', 'idcn', 'hlca', 'ftcn', 'tmco', 'diap']||outcome
11||Both superficial and deep shave biopsies were more accurate than punch biopsy for melanomas less than 1 mm.||['acty', 'spco', 'clna', 'medd', 'cgab', 'gngm', 'lbpr', 'qlco', 'neop', 'mamm', 'geoa', 'idcn', 'hlca', 'diap']||outcome
12||Excisional biopsy was found to be the most accurate method of biopsy.||['acty', 'blor', 'dsyn', 'cgab', 'euka', 'gngm', 'elii', 'lbpr', 'qlco', 'neop', 'mamm', 'geoa', 'inpr', 'diap']||outcome
13||CONCLUSIONS.||[]||outcome
14||Deep shave biopsy is preferable to superficial shave or punch biopsy for thin and intermediate depth (&lt;2 mm) melanomas when an initial sample is taken for diagnosis instead of complete excision.||['dsyn', 'fish', 'gngm', 'elii', 'idcn', 'neop', 'qnco', 'inpr', 'acty', 'spco', 'blor', 'medd', 'fndg', 'mamm', 'sbst', 'geoa', 'diap', 'cnce', 'clna', 'food', 'euka', 'topp', 'orga', 'qlco', 'hlca', 'tmco']||outcome
15||We found that a group of predominantly experienced dermatologists accurately assessed the depth of invasive melanoma by use of a variety of initial biopsy types.||['popg', 'dsyn', 'gngm', 'horm', 'idcn', 'neop', 'qnco', 'inpr', 'phsu', 'acty', 'spco', 'menp', 'fndg', 'mamm', 'prog', 'ftcn', 'diap', 'cnce', 'cgab', 'geoa', 'bpoc', 'lbpr', 'qlco', 'tmco', 'strd']||outcome
