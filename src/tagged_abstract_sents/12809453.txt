1||BACKGROUND.||[]||other
2||There is no consensus on the effectiveness of inhaled corticosteroids for the treatment of chronic obstructive pulmonary disease (COPD).||['phsu', 'bpoc', 'popg', 'clna', 'dsyn', 'orgf', 'horm', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'strd', 'socb']||background
3||PURPOSE.||[]||other
4||To evaluate the long-term effects of inhaled corticosteroids on the rate of FEV1 decline in patients with COPD.||['phsu', 'acty', 'clna', 'dsyn', 'podg', 'orgf', 'horm', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'strd']||population
5||DATA SOURCES.||[]||other
6||MEDLINE, EMBASE, CISCOM, and AMED databases and the Cochrane Library (1966 to December 2002), reference lists from identified articles, and consultation with experts.||['hlca', 'dsyn', 'idcn', 'qlco', 'geoa', 'inpr']||other
7||Searches were not limited to the English language.||['lang', 'ftcn', 'orga', 'dsyn', 'geoa']||other
8||STUDY SELECTION.||[]||other
9||Randomized, placebo-controlled trials that examined the rate of FEV1 decline as a primary outcome in patients with COPD.||['acty', 'dsyn', 'podg', 'topp', 'qlco', 'fndg', 'geoa', 'ftcn', 'resa']||other
10||DATA EXTRACTION.||[]||other
11||Two reviewers independently extracted the data by using predetermined criteria.||['clna', 'spco', 'medd', 'dsyn', 'topp', 'idcn', 'qlco', 'inpr', 'geoa', 'ftcn', 'tmco']||other
12||DATA SYNTHESIS.||[]||other
13||For the six studies that met the inclusion criteria, the summary estimate for the difference in FEV1 decline between the placebo and treatment groups was -5.0 +/- 3.2 mL/y (95% CI, -11.2 to 1.2 mL/y; P = 0.11).||['popg', 'topp', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
14||CONCLUSIONS.||[]||outcome
15||The use of inhaled corticosteroids was not associated with the rate of FEV1 decline in 3571 patients followed for 24 to 54 months.||['phsu', 'acty', 'dsyn', 'podg', 'orgf', 'gngm', 'horm', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'strd']||outcome
