1||BACKGROUND &amp; AIMS.||[]||other
2||Studies of antidepressants and psychological treatments in functional bowel disorders (FBD) are methodologically limited.||['bpoc', 'phsu', 'popg', 'clna', 'dsyn', 'topp', 'phsf', 'geoa', 'ftcn', 'inpr']||background
3||The aim of this study was to assess the clinical efficacy and safety of cognitive-behavioral therapy (CBT) against education (EDU) and desipramine (DES) against placebo (PLA) in female patients with moderate to severe FBD (irritable bowel syndrome, functional abdominal pain, painful constipation, and unspecified FBD).||['bpoc', 'acty', 'spco', 'clna', 'dsyn', 'edac', 'podg', 'orga', 'sosy', 'topp', 'idcn', 'qlco', 'phsf', 'inbe', 'hcpp', 'geoa', 'ftcn', 'resa', 'gngm', 'orch', 'phsu']||background
4||We also evaluated the amenability of clinically meaningful subgroups to these treatments.||['popg', 'topp', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'clas']||background
5||METHODS.||[]||other
6||This randomized, comparator-controlled, multicenter trial enrolled 431 adults from the University of North Carolina and the University of Toronto with moderate to severe symptoms of FBD.||['acty', 'spco', 'qlco', 'aggp', 'geoa', 'ftcn', 'mnob', 'resa']||population
7||Participants received psychological (CBT vs. EDU) or antidepressant (DES vs. PLA) treatment for 12 weeks.||['phsu', 'popg', 'gngm', 'qlco', 'ftcn', 'tmco']||intervention
8||Clinical, physiologic, and psychosocial assessments were performed before and at the end of treatment.||['acty', 'spco', 'popg', 'gngm', 'geoa', 'hlca', 'ftcn']||other
9||RESULTS.||[]||outcome
10||The intention-to-treat analysis showed CBT as significantly more effective than EDU (P = 0.0001; responder rate, 70% CBT vs. 37% EDU; number needed to treat [NNT ], 3.1).||['acty', 'clna', 'qnco', 'menp', 'lbpr', 'qlco', 'idcn', 'geoa', 'ftcn', 'inpr']||outcome
11||DES did not show significant benefit over PLA in the intention-to-treat analysis (P = 0.16; responder rate, 60% DES vs. 47% PLA; NNT, 8.1) but did show a statistically significant benefit in the per-protocol analysis (P = 0.01; responder rate, 73% DES vs. 49% PLA; NNT, 5.2), especially when participants with nondetectable blood levels of DES were excluded (P = 0.002).||['qnco', 'acty', 'spco', 'clna', 'ocdi', 'gngm', 'menp', 'lbpr', 'idcn', 'geoa', 'ftcn', 'inpr']||outcome
12||Improvement was best gauged by satisfaction with treatment.||['popg', 'clna', 'menp', 'qlco', 'geoa', 'ftcn']||outcome
13||Subgroup analyses showed that DES was beneficial over PLA for moderate more than severe symptoms, abuse history, no depression, and diarrhea-predominant symptoms; CBT was beneficial over EDU for all subgroups except for depression.||['acty', 'clna', 'ocdi', 'gngm', 'mobd', 'idcn', 'qlco', 'fndg', 'geoa', 'ftcn', 'inpr', 'clas']||outcome
14||CONCLUSIONS.||[]||outcome
15||For female patients with moderate to severe FBD, CBT is effective and DES may be effective when taken adequately.||['acty', 'cnce', 'clna', 'podg', 'orga', 'qlco', 'geoa']||outcome
16||Certain clinical subgroups are more or less amenable to these treatments.||['popg', 'topp', 'idcn', 'qlco', 'geoa', 'ftcn', 'clas']||outcome
