1||OBJECTIVES.||[]||other
2||To quantify the risk of Alzheimer's disease in users of all non-steroidal anti-inflammatory drugs (NSAIDs) and users of aspirin and to determine any influence of duration of use.||['phsu', 'tmco', 'ftcn', 'dsyn', 'gngm', 'elii', 'idcn', 'qlco', 'mamm', 'orch', 'qnco', 'prog', 'geoa', 'inpr', 'strd', 'humn']||background
3||DESIGN.||[]||other
4||Systematic review and meta-analysis of observational studies published between 1966 and October 2002 that examined the role of NSAID use in preventing Alzheimer's disease.||['dsyn', 'gngm', 'elii', 'qnco', 'inpr', 'evnt', 'phsu', 'spco', 'socb', 'ocac', 'fndg', 'geoa', 'ftcn', 'diap', 'clna', 'euka', 'bpoc', 'resa', 'mobd', 'lbpr', 'qlco', 'orch', 'hlca', 'tmco', 'plnt']||other
5||Studies identified through Medline, Embase, International Pharmaceutical Abstracts, and the Cochrane Library.||['bpoc', 'acty', 'spco', 'dsyn', 'clna', 'gngm', 'elii', 'mobd', 'topp', 'qlco', 'nnon', 'qnco', 'geoa', 'chvf', 'orgt', 'blor']||other
6||RESULTS.||[]||other
7||Nine studies looked at all NSAIDs in adults aged &gt; 55 years.||['bpoc', 'dsyn', 'orga', 'aggp', 'qnco', 'tmco']||outcome
8||Six were cohort studies (total of 13 211 participants), and three were case-control studies (1443 participants).||['bpoc', 'spco', 'popg', 'dsyn', 'gngm', 'mobd', 'qlco', 'qnco', 'geoa', 'ftcn', 'genf']||outcome
9||The pooled relative risk of Alzheimer's disease among users of NSAIDs was 0.72 (95% confidence interval 0.56 to 0.94).||['cnce', 'clna', 'dsyn', 'tmco', 'gngm', 'menp', 'idcn', 'qlco', 'spco', 'humn', 'qnco', 'geoa', 'ftcn', 'inpr', 'famg']||outcome
10||The risk was 0.95 (0.70 to 1.29) among short term users (&lt; 1 month) and 0.83 (0.65 to 1.06) and 0.27 (0.13 to 0.58) among intermediate term (mostly &lt; 24 months) and long term (mostly &gt; 24 months) users, respectively.||['spco', 'clna', 'dsyn', 'ftcn', 'idcn', 'qlco', 'neop', 'tmco', 'humn', 'socb', 'qnco', 'geoa', 'mamm', 'inpr', 'genf', 'blor']||outcome
11||The pooled relative risk in the eight studies of aspirin users was 0.87 (0.70 to 1.07).||['phsu', 'cnce', 'clna', 'dsyn', 'gngm', 'idcn', 'qlco', 'spco', 'humn', 'orch', 'qnco', 'geoa', 'ftcn', 'famg']||outcome
12||CONCLUSIONS.||[]||other
13||NSAIDs offer some protection against the development of Alzheimer's disease.||['popg', 'clna', 'dsyn', 'gngm', 'elii', 'qlco', 'phsf', 'fndg', 'qnco', 'geoa', 'inpr']||outcome
14||The appropriate dosage and duration of drug use and the ratios of risk to benefit are still unclear.||['phsu', 'bpoc', 'gngm', 'clna', 'dsyn', 'orga', 'elii', 'lbpr', 'qlco', 'mamm', 'geoa', 'qnco', 'prog', 'ftcn', 'tmco', 'blor']||outcome
