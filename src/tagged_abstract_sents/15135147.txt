1||OBJECTIVE.||[]||other
2||To test the hypothesis that glucose intolerance does not occur when healthy adults consume normal, recommended dosages of glucosamine sulfate.||['popg', 'dsyn', 'gngm', 'idcn', 'bacs', 'qnco', 'phsu', 'spco', 'fndg', 'inch', 'ftcn', 'cnce', 'topp', 'geoa', 'carb', 'orga', 'orgf', 'lbpr', 'qlco', 'aggp', 'orch', 'tmco']||background
3||METHODS.||[]||other
4||Healthy adults (N=19) ingested 1500 mg of glucosamine sulfate or placebo (double blind) each day for 12 weeks.||['phsu', 'spco', 'biof', 'dsyn', 'inch', 'orga', 'topp', 'idcn', 'geoa', 'tmco', 'aggp', 'orch', 'carb', 'ftcn', 'resa']||intervention
5||Three-hour oral glucose tolerance tests (OGTT) were performed using 75 g of dextrose.||['phsu', 'spco', 'dsyn', 'diap', 'lbpr', 'geoa', 'phsf', 'bodm', 'fndg', 'carb', 'ftcn', 'tmco', 'plnt', 'bacs']||population
6||These occurred before the start of supplementation, at 6 weeks, and at the completion of supplementation (12 weeks).||['cnce', 'popg', 'dsyn', 'clna', 'food', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'tmco']||other
7||RESULTS.||[]||outcome
8||There were no significant differences between fasted levels of serum insulin or blood glucose.||['phsu', 'dsyn', 'gngm', 'horm', 'bdsu', 'idcn', 'qlco', 'fndg', 'tisu', 'aapp', 'carb', 'geoa', 'bacs']||outcome
9||Glucosamine sulfate supplementation did not alter serum insulin or plasma glucose during the OGTT.||['phsu', 'idcn', 'spco', 'popg', 'aapp', 'dsyn', 'qlco', 'food', 'inch', 'orga', 'horm', 'bdsu', 'topp', 'lbpr', 'geoa', 'bacs', 'orch', 'qnco', 'carb', 'ftcn', 'gngm']||outcome
10||There were no significant differences within or between treatments, ages or gender.||['bpoc', 'gngm', 'popg', 'orga', 'topp', 'idcn', 'qlco', 'fndg', 'spco', 'geoa', 'ftcn']||outcome
11||Glycated hemoglobin measurements at the three time points showed no significant change over time, within or between treatments, ages or gender.||['bpoc', 'gngm', 'popg', 'dsyn', 'tmco', 'orga', 'topp', 'idcn', 'qlco', 'fndg', 'spco', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
12||The lack of significant changes may have been due to large standard deviations in the data.||['spco', 'dsyn', 'gngm', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
13||CONCLUSION.||[]||other
14||The data suggests that glucosamine supplementation, with normal recommended dosages, does not cause glucose intolerance in healthy adults.||['phsu', 'cnce', 'popg', 'ftcn', 'dsyn', 'food', 'orga', 'orgf', 'topp', 'idcn', 'qlco', 'spco', 'bacs', 'fndg', 'orch', 'qnco', 'carb', 'geoa', 'tmco', 'gngm', 'aggp']||outcome
15||This cannot be determined conclusively, however, until further studies are conducted using alternative types of testing.||['cnce', 'clna', 'dsyn', 'lbpr', 'qlco', 'nnon', 'spco', 'inbe', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
