1||OBJECTIVE.||[]||other
2||To investigate the relationship between breast-feeding and obesity in childhood.||['bpoc', 'tmco', 'socb', 'dsyn', 'gngm', 'elii', 'qlco', 'fndg', 'aggp', 'geoa', 'qnco', 'hlca', 'inpr', 'famg']||background
3||DESIGN.||[]||other
4||Systematic review and meta-analysis of published epidemiological studies (cohort, case-control or cross-sectional studies) comparing early feeding-mode and adjusting for potential confounding factors.||['popg', 'dsyn', 'bmod', 'gngm', 'qnco', 'resa', 'phsu', 'acty', 'spco', 'blor', 'ocac', 'inpr', 'mamm', 'geoa', 'ftcn', 'chvf', 'clna', 'euka', 'inch', 'bpoc', 'lbpr', 'qlco', 'hlca', 'tmco', 'plnt']||other
5||Electronic databases were searched and reference lists of relevant articles were checked.||['bpoc', 'acty', 'cnce', 'dsyn', 'clna', 'fish', 'gngm', 'idcn', 'qlco', 'qnco', 'geoa', 'mnob', 'inpr', 'chvf', 'bsoj']||other
6||Calculations of pooled estimates were conducted in fixed- and random-effects models.||['cnce', 'clna', 'dsyn', 'gngm', 'elii', 'qlco', 'fndg', 'inbe', 'qnco', 'geoa', 'ftcn', 'resa', 'comd']||other
7||Heterogeneity was tested by Q-test.||['lbpr', 'gngm', 'dsyn']||other
8||Publication bias was assessed from funnel plots and by a linear regression method.||['bpoc', 'acty', 'spco', 'dsyn', 'qnco', 'idcn', 'qlco', 'mamm', 'patf', 'geoa', 'inpr']||other
9||OUTCOME MEASURES.||[]||other
10||Odds ratio (OR) for obesity in childhood defined as body mass index (BMI) percentiles.||['cnce', 'dsyn', 'gngm', 'anst', 'inpr', 'qlco', 'fndg', 'aggp', 'qnco', 'geoa', 'ftcn', 'tmco', 'diap']||outcome
11||RESULTS.||[]||outcome
12||Nine studies with more than 69,000 participants met the inclusion criteria.||['bpoc', 'spco', 'popg', 'gngm', 'qlco', 'mamm', 'qnco', 'geoa', 'inpr']||outcome
13||The meta-analysis showed that breast-feeding reduced the risk of obesity in childhood significantly.||['bpoc', 'idcn', 'tmco', 'dsyn', 'euka', 'gngm', 'lbpr', 'qlco', 'fndg', 'aggp', 'orch', 'phsu', 'hlca', 'geoa', 'inpr']||outcome
14||The adjusted odds ratio was 0.78, 95% CI (0.71, 0.85) in the fixed model.||['dsyn', 'comd', 'fndg', 'qnco', 'geoa', 'ftcn', 'chvf']||outcome
15||The assumption of homogeneity of results of the included studies could not be refuted (Q-test for heterogeneity, P&gt;0.3), stratified analyses showed no differences regarding different study types, age groups, definition of breast-feeding or obesity and number of confounding factors adjusted for.||['bpoc', 'acty', 'orga', 'popg', 'clna', 'dsyn', 'gngm', 'hlca', 'lbpr', 'qlco', 'qnco', 'resa', 'aggp', 'geoa', 'idcn', 'inch', 'ftcn', 'inpr', 'chvf', 'fndg', 'phsu']||outcome
16||A dose-dependent effect of breast-feeding duration on the prevalence of obesity was reported in four studies.||['bpoc', 'spco', 'dsyn', 'clna', 'gngm', 'medd', 'inpr', 'qlco', 'fndg', 'geoa', 'qnco', 'hlca', 'tmco', 'blor']||outcome
17||Funnel plot regression gave no indication of publication bias.||['patf', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr']||outcome
18||CONCLUSION.||[]||other
19||Breast-feeding seems to have a small but consistent protective effect against obesity in children.||['acty', 'clna', 'medd', 'gngm', 'idcn', 'qlco', 'fndg', 'prog', 'geoa', 'qnco', 'hlca', 'inpr', 'dsyn', 'aggp']||outcome
