1||BACKGROUND.||[]||other
2||This paper is a systematic review of metformin versus clomiphene citrate (CC) in women with polycystic ovary syndrome (PCOS).||['phsu', 'bpoc', 'spco', 'popg', 'orch', 'dsyn', 'acty', 'gngm', 'inpr', 'idcn', 'geoa', 'bodm', 'mamm', 'bacs', 'aapp', 'qnco', 'prog', 'anab', 'mnob', 'ftcn', 'plnt']||background
3||METHODS.||[]||other
4||Meta-analysis Of Observational Studies in Epidemiology (MOOSE) and QUality Of Reporting Of Meta-analyses (QUOROM) guidelines were followed.||['bpoc', 'spco', 'dsyn', 'gngm', 'elii', 'comd', 'lbpr', 'qlco', 'geoa', 'qnco', 'prog', 'ftcn', 'inpr', 'genf', 'nusq']||other
5||A systematic computerized literature search was done of seven bibliographic databases.||['bpoc', 'acty', 'tmco', 'dsyn', 'fish', 'gngm', 'idcn', 'mamm', 'bsoj', 'qnco', 'ftcn', 'mnob', 'inpr', 'chvf', 'plnt']||other
6||Inclusion criteria included cohort and randomized controlled trials (RCT) of women with PCOS and the following medications: metformin versus placebo; metformin versus CC; metformin plus CC versus placebo plus CC.||['phsu', 'spco', 'popg', 'resa', 'dsyn', 'chvs', 'gngm', 'elii', 'topp', 'idcn', 'qlco', 'bodm', 'mamm', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr', 'genf']||other
7||Rev-man 4.1 and Metaview 4.0 were used to analyse data.||['bpoc', 'gngm', 'dsyn', 'orga', 'idcn', 'spco', 'ftcn', 'inpr']||other
8||Relative risk (RR) estimates were presented.||['dsyn', 'gngm', 'idcn', 'qlco', 'qnco', 'ftcn', 'tmco']||other
9||A chi2-test determined the significance of the association.||['inpr', 'dsyn', 'gngm', 'menp', 'elii', 'topp', 'lbpr', 'fndg', 'mamm', 'qnco', 'geoa', 'tmco']||other
10||Heterogeneity was determined by the Cochran Q-test.||['dsyn', 'gngm', 'topp', 'lbpr', 'geoa', 'tmco']||other
11||RESULTS.||[]||outcome
12||Metformin was 50% better than placebo for ovulation induction in infertile PCOS patients [RR 1.50; 95% confidence interval (CI) 1.13, 1.99].||['bpoc', 'spco', 'dsyn', 'clna', 'podg', 'menp', 'gngm', 'orgf', 'elii', 'topp', 'qlco', 'tmco', 'bodm', 'qnco', 'geoa', 'ortf', 'ftcn', 'genf']||outcome
13||Metformin was also of benefit in non-infertile (i.e. patients with PCOS who were not complaining of infertility) PCOS patients for cycle regulation compared to placebo (RR 1.45; CI 1.11, 1.90).||['bpoc', 'acty', 'spco', 'dsyn', 'gora', 'podg', 'gngm', 'orgf', 'elii', 'topp', 'qlco', 'fndg', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco', 'genf']||outcome
14||Metformin was not of confirmed benefit versus placebo for achievement of pregnancy (RR 1.07; CI 0.20, 5.74).||['inpr', 'bhvr', 'spco', 'popg', 'dsyn', 'gngm', 'orgf', 'topp', 'idcn', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
15||Metformin plus CC may be 3-4-fold superior to CC alone for ovulation induction (RR 3.04; CI 1.77, 5.24) and pregnancy (RR 3.65; CI 1.11, 11.99) in women with PCOS.||['spco', 'popg', 'dsyn', 'clna', 'tmco', 'gngm', 'orgf', 'elii', 'idcn', 'fndg', 'bodm', 'qnco', 'geoa', 'ortf', 'ftcn']||outcome
16||CONCLUSIONS.||[]||outcome
17||Metformin is effective for ovulation induction and cycle regulation in this group of patients.||['bpoc', 'ortf', 'popg', 'dsyn', 'clna', 'qlco', 'podg', 'gngm', 'elii', 'idcn', 'gora', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
18||Metformin plus CC appears to be very effective for achievement of pregnancy compared to CC alone.||['bpoc', 'acty', 'bhvr', 'inpr', 'popg', 'clna', 'dsyn', 'gngm', 'orgf', 'qlco', 'fndg', 'bodm', 'qnco', 'geoa', 'tmco']||outcome
19||No RCTs directly compare metformin to CC but the need for such a trial exists.||['phsu', 'acty', 'clna', 'dsyn', 'gngm', 'idcn', 'qlco', 'mamm', 'orch', 'geoa', 'bodm', 'resa']||outcome
