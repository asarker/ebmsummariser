1||OBJECTIVE.||[]||other
2||To compare by randomized prospective clinical trial the outcome of labours which are managed with the intention to leave the membranes intact, compared with the practice of elective artificial rupture of the membranes (ARM) in early established labour.||['resa', 'acty', 'hcro', 'ocac', 'clna', 'dsyn', 'fish', 'gngm', 'menp', 'qlco', 'orga', 'tisu', 'qnco', 'geoa', 'ftcn', 'inpo', 'tmco', 'mnob']||background
3||DESIGN.||[]||other
4||Prospective randomized controlled trial of low risk women admitted in spontaneous labour, with intact membranes.||['acty', 'popg', 'clna', 'dsyn', 'fish', 'gngm', 'qlco', 'tisu', 'geoa', 'hlca', 'ftcn', 'resa']||other
5||SETTING.||[]||other
6||The labour ward of St. James's University Hospital, Leeds, UK. SUBJECTS.||['hcro', 'mnob', 'fish', 'dsyn']||other
7||362 women in spontaneous labour with intact membranes and no evidence of fetal distress, between 37 and 42 weeks gestation.||['acty', 'popg', 'clna', 'orgf', 'fish', 'gngm', 'menp', 'emst', 'qlco', 'fndg', 'tisu', 'geoa', 'ftcn', 'tmco']||outcome
8||During the course of the trial it was found that some randomization cards could not be accounted for and a system of daily checks was instituted.||['acty', 'tmco', 'dsyn', 'gngm', 'idcn', 'qnco', 'geoa', 'ftcn', 'resa']||outcome
9||The results were analysed for all recorded women (n = 362) and after institution of the more rigorous system (n = 120).||['bpoc', 'hcro', 'popg', 'dsyn', 'gngm', 'idcn', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr']||outcome
10||MAIN OUTCOMES MEASURED.||[]||outcome
11||The duration of each phase of labour, epidural rate, prevalence of an abnormal cardiotocograph (CTG) (assessed blind), method of delivery and neonatal outcome.||['bpoc', 'acty', 'resa', 'dsyn', 'fish', 'gngm', 'topp', 'qlco', 'tmco', 'aggp', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
12||RESULTS.||[]||outcome
13||178 of the 183 women (97%) in the ARM group had their membranes ruptured in early labour, and 83 (46%) of the 179 women allocated to non-intervention had ARM performed at some stage.||['orga', 'popg', 'inpo', 'dsyn', 'fish', 'gngm', 'idcn', 'hlca', 'bodm', 'tisu', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
14||A significant decrease in the duration of labour (mean 8.3, SD 4.1 h vs mean 9.7, SD 4.8 h, n = 156; P = 0.05) was found amongst primigravidae allocated to ARM when compared with non intervention.||['acty', 'hlca', 'dsyn', 'fish', 'gngm', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
15||The duration of the second stage of labour was unaffected.||['tmco', 'dsyn', 'clna', 'fish', 'orga', 'menp', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
16||In the ARM group the epidural rate was higher and labour was more often complicated by CTG abnormalities.||['acty', 'popg', 'dsyn', 'cgab', 'fish', 'gngm', 'idcn', 'qlco', 'geoa', 'ftcn', 'tmco']||outcome
17||There were no differences in the method of delivery, fetal condition at birth (cord blood lactate, Apgar score) or postpartum pyrexia between the ARM and non-intervention groups.||['bpoc', 'acty', 'geoa', 'spco', 'popg', 'ftcn', 'clna', 'tmco', 'gngm', 'orgf', 'topp', 'idcn', 'qlco', 'fndg', 'tisu', 'orch', 'qnco', 'hlca', 'inpr', 'emst']||outcome
18||The same trends were observed when analysis was confined to women entered into the trial after the system of rigour was instituted.||['idcn', 'spco', 'popg', 'dsyn', 'gngm', 'lbpr', 'qlco', 'tmco', 'qnco', 'geoa', 'ftcn', 'resa']||outcome
19||CONCLUSION.||[]||other
20||Routine ARM results in labour that is slightly shorter than if the membranes are allowed to rupture spontaneously but more epidurals are used suggesting that labour is more painful.||['inpo', 'dsyn', 'fish', 'gngm', 'sosy', 'idcn', 'qlco', 'tisu', 'qnco', 'geoa', 'ftcn']||outcome
21||There are fewer fetal heart rate abnormalities if the membranes are left intact but amniotomy has no effect on fetal condition at birth.||['bpoc', 'acty', 'spco', 'clna', 'cgab', 'fish', 'orgf', 'topp', 'qlco', 'tisu', 'geoa', 'ftcn', 'emst']||outcome
