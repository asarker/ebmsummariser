1||BACKGROUND.||[]||other
2||When a woman has had a previous caesarean birth, there are two options for her care in a subsequent pregnancy: planned elective repeat caesarean or planned vaginal birth.||['popg', 'clna', 'menp', 'orga', 'orgf', 'topp', 'fndg', 'qnco', 'geoa', 'ftcn', 'tmco']||background
3||While there are risks and benefits for both planned elective repeat caesarean birth and planned vaginal birth after caesarean, current sources of information are limited to non-randomised cohort studies.||['phsu', 'tmco', 'popg', 'clna', 'menp', 'orgf', 'idcn', 'qlco', 'bodm', 'aapp', 'qnco', 'geoa', 'ftcn', 'resa', 'imft']||background
4||Studies designed in this way have significant potential for bias and consequently conclusions based on these results are limited in their reliability and should be interpreted with caution.||['acty', 'dsyn', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn']||background
5||OBJECTIVES.||[]||other
6||To assess, using the best available evidence, the benefits and harms of a policy of planned elective repeat caesarean section with a policy of planned vaginal birth after caesarean section for women with a previous caesarean birth.||['acty', 'tmco', 'popg', 'blor', 'clna', 'menp', 'orgf', 'topp', 'qlco', 'qnco', 'geoa', 'ftcn', 'inpr']||other
7||SEARCH STRATEGY.||[]||other
8||We searched the Cochrane Pregnancy and Childbirth Group trials register (24 June 2004), the Cochrane Central Register of Controlled Trials (The Cochrane Library, Issue 1, 2004), and PubMed (1966 to 24 June 2004).||['geoa', 'idcn', 'resa', 'orgf', 'acty']||other
9||SELECTION CRITERIA.||[]||other
10||Randomised controlled trials with reported data that compared outcomes in mothers and babies who planned a repeat elective caesarean section with outcomes in women who planned a vaginal birth, where a previous birth had been by caesarean.||['inpr', 'acty', 'resa', 'popg', 'clna', 'menp', 'orgf', 'topp', 'idcn', 'qlco', 'geoa', 'ftcn', 'tmco', 'famg']||other
11||DATA COLLECTION AND ANALYSIS.||[]||other
12||Two reviewers independently assessed trial quality and extracted data.||['acty', 'spco', 'clna', 'topp', 'idcn', 'qlco', 'inpr', 'resa']||other
13||MAIN RESULTS.||[]||other
14||There were no randomised controlled trials identified.||['qlco', 'resa', 'ftcn']||other
15||REVIEWERS' CONCLUSIONS.||[]||other
16||Planned elective repeat caesarean section and planned vaginal birth after caesarean section for women with a prior caesarean birth are both associated with benefits and harms.||['popg', 'blor', 'clna', 'menp', 'gngm', 'orgf', 'topp', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
17||Evidence for these care practices is drawn from non-randomised studies, associated with potential bias.||['clna', 'dsyn', 'gngm', 'menp', 'idcn', 'qlco', 'fndg', 'geoa', 'resa']||outcome
18||Any results and conclusions must therefore be interpreted with caution.||['geoa', 'ftcn', 'dsyn', 'idcn']||outcome
19||Randomised controlled trials are required to provide the most reliable evidence regarding the benefits and harms of both planned elective repeat caesarean section and planned vaginal birth for women with a previous caesarean birth.||['tmco', 'popg', 'blor', 'clna', 'food', 'menp', 'orgf', 'topp', 'qlco', 'qnco', 'geoa', 'ftcn', 'resa']||outcome
