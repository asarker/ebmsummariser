1||OBJECTIVE.||[]||other
2||To provide a critical and comprehensive review of the literature, specifically case reports and observational studies used to support the concept of cross-reactivity between sulfonylarylamines and non-sulfonylarylamines.||['inpr', 'acty', 'resa', 'clna', 'medd', 'food', 'gngm', 'idcn', 'qlco', 'spco', 'mamm', 'dsyn', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'diap']||background
3||DATA SOURCES.||[]||other
4||A list of medications was formulated from several different review articles.||['phsu', 'cnce', 'dsyn', 'tmco', 'gngm', 'qlco', 'spco', 'bodm', 'qnco', 'geoa', 'inpr']||other
5||A MEDLINE/PubMed search was conducted (1966-March 2004) using the individual medications and the MeSH terms of drug hypersensitivity/etiology, sulfonamides/adverse effects, and/or cross-reaction.||['phsu', 'acty', 'inpr', 'popg', 'dsyn', 'clna', 'ftcn', 'gngm', 'idcn', 'qlco', 'fndg', 'qnco', 'orch', 'patf', 'prog', 'geoa', 'tmco', 'inbe', 'bsoj']||other
6||STUDY SELECTION AND DATA EXTRACTION.||[]||other
7||A critical review of the methodology and conclusions for each article found in the search was conducted.||['spco', 'dsyn', 'clna', 'gngm', 'idcn', 'qlco', 'inbe', 'qnco', 'geoa', 'inpr', 'bsoj']||other
8||The manufacturer's package insert (MPI) for each drug was examined for a statement concerning possible cross-reactivity in patients with a sulfonamide allergy.||['phsu', 'acty', 'orga', 'popg', 'dsyn', 'clna', 'podg', 'bmod', 'gngm', 'idcn', 'qlco', 'fndg', 'inpr', 'orch', 'qnco', 'prog', 'geoa', 'ftcn']||other
9||If indicated, the manufacturers were contacted to obtain any clinical data supporting the statement.||['acty', 'hcro', 'gngm', 'popg', 'medd', 'clna', 'orga', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr', 'dsyn']||other
10||DATA SYNTHESIS.||[]||other
11||A total of 33 medications were identified.||['phsu', 'qlco', 'gngm', 'dsyn', 'qnco']||other
12||Seventeen (51.5%) of the MPIs contained statements of varying degrees concerning use in patients with a "sulfonamide" allergy; 21 case series, case reports, and other articles were found.||['phsu', 'acty', 'popg', 'dsyn', 'podg', 'bmod', 'gngm', 'idcn', 'qlco', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr', 'evnt']||outcome
13||CONCLUSIONS.||[]||outcome
14||After a thorough critique of the literature, it appears that the dogma of sulfonylarylamine cross-reactivity with non-sulfonylarylamines is not supported by the data.||['inpr', 'acty', 'resa', 'medd', 'clna', 'idcn', 'cnce', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'dsyn']||outcome
15||While many of the case reports on the surface support the concept of cross-reactivity, on closer examination the level of evidence in many of the cases does not conclusively support either a connection or an association between the observed cause and effect.||['inpr', 'acty', 'cnce', 'blor', 'clna', 'orga', 'menp', 'medd', 'idcn', 'qlco', 'spco', 'mamm', 'orgt', 'qnco', 'geoa', 'ftcn', 'topp', 'gngm', 'dsyn']||outcome
