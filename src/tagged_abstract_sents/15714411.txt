1||BACKGROUND.||[]||other
2||A major difficulty associated with the use of standard therapy with isoniazid for latent tuberculosis infection is poor patient adherence to therapy because of the prolonged course required.||['phsu', 'cnce', 'clna', 'dsyn', 'grpa', 'gngm', 'qlco', 'fndg', 'orch', 'geoa', 'ftcn', 'tmco']||background
3||Shorter courses of therapy involving &gt; or =2 drugs have been proposed as an alternative to standard therapy, but they have not undergone enough testing.||['phsu', 'lbpr', 'cnce', 'idcn', 'qnco', 'geoa', 'ftcn', 'tmco']||background
4||METHODS.||[]||other
5||We performed a meta-analysis to determine the equivalence of daily short-course therapy with rifampin plus isoniazid for 3 months and standard therapy with isoniazid for 6-12 months.||['phsu', 'lbpr', 'bodm', 'orch', 'geoa', 'ftcn', 'tmco', 'antb']||other
6||The end points that were evaluated were development of active tuberculosis, severe adverse drug reactions, and death.||['phsu', 'spco', 'popg', 'inpo', 'dsyn', 'orgf', 'clna', 'qlco', 'phsf', 'geoa', 'ftcn']||other
7||We searched published information in the Cochrane Library, MEDLINE, and Embase databases, as well as unpublished information in the Cambridge Scientific Abstracts Internet database, Conference Papers Index, AIDS and Cancer Research Abstracts, and ClinicalTrials.gov.||['phsu', 'acty', 'clna', 'ocac', 'gngm', 'idcn', 'qlco', 'bodm', 'aapp', 'geoa', 'inpr', 'imft']||other
8||We also scanned the reference lists of articles.||['geoa', 'idcn', 'diap']||other
9||We only included trials in which individuals were randomly allocated to receive treatment.||['qlco', 'resa', 'popg', 'ftcn']||other
10||Two reviewers independently applied the criteria for trial selection, assessed trial quality, and extracted data.||['acty', 'spco', 'clna', 'geoa', 'topp', 'idcn', 'qlco', 'hlca', 'inpr', 'resa', 'genf']||other
11||RESULTS.||[]||outcome
12||Five trials comprising 1926 adults from Hong Kong, Spain, and Uganda were identified.||['qlco', 'resa', 'aggp']||outcome
13||The mean duration of follow-up varied from 13 to 37 months.||['geoa', 'qlco', 'tmco', 'qnco']||outcome
14||Overall, development of active tuberculosis was equivalent in association with both regimens (pooled risk difference, 0%; 95% confidence interval [CI], -1% to 2%; percentage of total variation across the studies that is the result of heterogeneity rather than chance [I2], 0%; P=.86).||['orga', 'popg', 'clna', 'dsyn', 'phsf', 'gngm', 'menp', 'topp', 'qlco', 'cnce', 'qnco', 'geoa', 'ftcn']||outcome
15||Severe adverse effects were reported with a similar frequency for both regimens (pooled risk difference, -1%; 95% CI, -7% to 5%) but with statistically significant heterogeneity detected (I2, 78%; P=.001).||['cnce', 'popg', 'clna', 'ocdi', 'topp', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
16||However, a subanalysis of high-quality trials (including 74% of the sample size) suggested that both regimens were equally safe.||['idcn', 'spco', 'popg', 'topp', 'lbpr', 'qlco', 'sbst', 'geoa', 'resa']||outcome
17||In 3 trials (comprising 1390 patients) that provided data on mortality, the regimens showed equivalence (pooled risk difference, -1%; 95% CI, -4% to 2%; I2, 2.7%; P=.38).||['cnce', 'popg', 'food', 'topp', 'idcn', 'qlco', 'podg', 'qnco', 'geoa', 'inpr', 'resa']||outcome
18||CONCLUSION.||[]||other
19||Short-course therapy with rifampin plus isoniazid was equivalent to standard therapy with isoniazid in terms of efficacy, the proportion of severe side effects that occurred, and mortality.||['phsu', 'cnce', 'clna', 'qlco', 'tmco', 'orch', 'qnco', 'geoa', 'ftcn', 'resa', 'antb']||outcome
