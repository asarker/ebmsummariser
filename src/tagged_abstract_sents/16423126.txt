1||OBJECTIVE.||[]||other
2||To evaluate Breslau's 7-item screen for posttraumatic stress disorder (PTSD) for use in primary care.||['inpr', 'inpo', 'dsyn', 'gngm', 'mobd', 'lbpr', 'qlco', 'fndg', 'geoa', 'qnco', 'hlca', 'ftcn', 'enty', 'tmco', 'blor']||background
3||DESIGN.||[]||other
4||One hundred and thirty-four patients were recruited from primary care clinics at a large medical center.||['bpoc', 'hcro', 'spco', 'dsyn', 'cgab', 'podg', 'gngm', 'qlco', 'fndg', 'prog', 'geoa', 'qnco', 'hlca', 'ftcn', 'mnob', 'inpr', 'genf']||population
5||Participants completed the self-administered 7-item PTSD screen.||['bpoc', 'dsyn', 'gngm', 'mobd', 'lbpr', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'enty', 'genf']||other
6||Later, psychologists blinded to the results of the screen-interviewed patients using the Clinician Administered PTSD Scale (CAPS).||['bpoc', 'acty', 'spco', 'resa', 'dsyn', 'podg', 'gngm', 'mobd', 'lbpr', 'qlco', 'fndg', 'mamm', 'geoa', 'idcn', 'prog', 'ftcn', 'inpr', 'qnco']||other
7||Sensitivity, specificity, and likelihood ratios (LR) were calculated using the CAPS as the criterion for PTSD.||['acty', 'inpr', 'blor', 'dsyn', 'gngm', 'elii', 'qlco', 'fndg', 'mamm', 'bacs', 'aapp', 'qnco', 'geoa', 'ftcn', 'tmco']||other
8||RESULTS.||[]||other
9||The screen appears to have test-retest reliability (r=.84), and LRs range from 0.04 to 13.4.||['bpoc', 'lbpr', 'gngm', 'fish', 'orga', 'idcn', 'mamm', 'qnco', 'geoa']||outcome
10||CONCLUSIONS.||[]||other
11||Screening for PTSD in primary care is time efficient and has the potential to increase the detection of previously unrecognized PTSD.||['inpr', 'dsyn', 'clna', 'gngm', 'elii', 'topp', 'qlco', 'fndg', 'mamm', 'geoa', 'qnco', 'hlca', 'ftcn', 'tmco']||outcome
