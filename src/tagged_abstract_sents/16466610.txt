1||OBJECTIVE.||[]||other
2||This trial evaluated the efficacy and safety of pregabalin dosed twice daily (BID) for relief of neuro-pathic pain associated with postherpetic neuralgia (PHN).||['phsu', 'anim', 'ftcn', 'dsyn', 'fish', 'gngm', 'sosy', 'topp', 'lbpr', 'qlco', 'fndg', 'mamm', 'aapp', 'hcpp', 'inch', 'geoa', 'inpr', 'resa', 'tmco', 'qnco', 'blor']||background
3||RESEARCH DESIGN AND METHODS.||[]||other
4||The 13-week, double-blind, placebo-controlled study randomized 370 patients with PHN to pregabalin (150, 300, or 600 mg/day BID) or placebo.||['bpoc', 'phsu', 'spco', 'dsyn', 'podg', 'fish', 'gngm', 'topp', 'qlco', 'tmco', 'aapp', 'qnco', 'geoa', 'ftcn', 'resa']||intervention
5||MAIN OUTCOME MEASURES.||[]||other
6||Primary efficacy measure was endpoint mean pain score from daily pain diaries.||['resa', 'gngm', 'sosy', 'idcn', 'qlco', 'spco', 'mamm', 'qnco', 'geoa', 'inpr', 'tmco']||outcome
7||Secondary efficacy measures included endpoint mean sleep-interference score from daily sleep diaries and Patient Global Impression of Change (PGIC).||['acty', 'tmco', 'dsyn', 'gngm', 'orgf', 'idcn', 'qlco', 'spco', 'mamm', 'qnco', 'geoa', 'inpr', 'resa']||outcome
8||Safety evaluations included adverse events (AEs), physical and neurologic examinations, 12-lead ECG, vital signs, and laboratory testing.||['bpoc', 'lbpr', 'hlca', 'dsyn', 'fish', 'gngm', 'orgf', 'elii', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr', 'bmod', 'orgt', 'evnt']||outcome
9||RESULTS.||[]||outcome
10||Pregabalin provided significant, dose-proportional pain relief at endpoint: difference from placebo in mean pain score, 150 mg/day, -0.88, p = 0.0077; 300 mg/day, -1.07, p = 0.0016; 600 mg/day, -1.79, p = 0.0003.||['spco', 'blor', 'dsyn', 'food', 'fish', 'gngm', 'sosy', 'topp', 'idcn', 'qlco', 'fndg', 'mamm', 'qnco', 'geoa', 'inpr', 'tmco']||outcome
11||Weekly mean pain scores significantly improved as early as week 1.||['bpoc', 'spco', 'dsyn', 'gngm', 'sosy', 'idcn', 'qlco', 'fndg', 'qnco', 'geoa', 'tmco']||outcome
12||Sleep interference in all pregabalin groups was also significantly improved at endpoint, compared with placebo (p &lt; 0.001), beginning at week 1 (p &lt; 0.01).||['phsu', 'acty', 'spco', 'popg', 'dsyn', 'fish', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'aapp', 'qnco', 'geoa', 'tmco']||outcome
13||At study termination, patients in the 150 (p = 0.02) and 600 mg/day (p = 0.003) groups were more likely to report global improvement than were those in the placebo group.||['bpoc', 'spco', 'popg', 'blor', 'medd', 'podg', 'gngm', 'topp', 'idcn', 'qlco', 'resa', 'qnco', 'geoa', 'inpr', 'tmco', 'orgt']||outcome
14||Most AEs were mild or moderate.||['ftcn', 'tmco', 'blor', 'qlco', 'acty']||outcome
15||Among pregabalin-treated patients, 13.5% withdrew due to AEs, most commonly for dizziness (16 patients, 5.8%), somnolence (8, 2.9%), or ataxia (7, 2.5%).||['bpoc', 'phsu', 'tmco', 'blor', 'dsyn', 'podg', 'fish', 'gngm', 'sosy', 'qlco', 'fndg', 'mamm', 'aapp', 'qnco', 'prog', 'geoa', 'ftcn']||outcome
16||CONCLUSIONS.||[]||outcome
17||Pregabalin, dosed BID, reduced neuropathic pain associated with PHN and was well tolerated.||['phsu', 'acty', 'blor', 'dsyn', 'fish', 'gngm', 'sosy', 'topp', 'lbpr', 'qlco', 'fndg', 'mamm', 'orch', 'qnco', 'geoa', 'tmco']||outcome
18||It also reduced the extent to which pain interfered with sleep.||['phsu', 'spco', 'dsyn', 'gngm', 'orgf', 'sosy', 'qlco', 'fndg', 'orch', 'geoa']||outcome
19||Pregabalin's effects were seen as early as week 1 and were sustained throughout the 13-week study.||['phsu', 'acty', 'inpr', 'dsyn', 'clna', 'fish', 'gngm', 'bpoc', 'idcn', 'qlco', 'tmco', 'orch', 'qnco', 'geoa', 'ftcn', 'resa', 'diap']||outcome
