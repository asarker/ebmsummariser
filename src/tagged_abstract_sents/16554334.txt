1||OBJECTIVE.||[]||other
2||To estimate the number of deaths and readmissions associated with delay in operation after femoral fracture.||['bpoc', 'acty', 'cnce', 'dsyn', 'clna', 'gngm', 'orgf', 'topp', 'qlco', 'fndg', 'dora', 'spco', 'geoa', 'qnco', 'hlca', 'mamm', 'inpo', 'tmco']||background
3||DESIGN.||[]||other
4||Analysis of inpatient hospital episode statistics.||['hcro', 'ocdi', 'podg', 'gngm', 'qlco', 'qnco', 'mnob']||other
5||SETTING.||[]||other
6||NHS hospital trusts in England with at least 100 admissions for fractured neck of femur during the study period.||['bpoc', 'acty', 'hcro', 'spco', 'resa', 'dsyn', 'clna', 'gngm', 'inpr', 'geoa', 'cnce', 'inbe', 'qnco', 'hlca', 'inpo', 'tmco', 'mnob', 'blor']||population
7||Patients People aged &gt; or = 65 admitted from home with fractured neck of femur and discharged between April 2001 and March 2004.||['bpoc', 'acty', 'orga', 'dsyn', 'clna', 'gngm', 'bdsu', 'qlco', 'cnce', 'geoa', 'qnco', 'hlca', 'inpo', 'inpr', 'bsoj', 'blor']||intervention
8||MAIN OUTCOME MEASURES.||[]||other
9||In hospital mortality and emergency readmission within 28 days.||['hcro', 'cnce', 'gngm', 'geoa', 'dora', 'qnco', 'hlca', 'mnob', 'tmco', 'phpr']||other
10||RESULTS.||[]||other
11||There were 129,522 admissions for fractured neck of femur in 151 trusts with 18,508 deaths in hospital (14.3%).||['bpoc', 'acty', 'hcro', 'cnce', 'clna', 'dsyn', 'gngm', 'orgf', 'geoa', 'fndg', 'inbe', 'qnco', 'hlca', 'inpo', 'inpr', 'mnob', 'blor']||outcome
12||Delay in operation was associated with an increased risk of death in hospital, which was reduced but persisted after adjustment for comorbidity.||['phsu', 'hcro', 'popg', 'orch', 'dsyn', 'gngm', 'orgf', 'topp', 'idcn', 'qlco', 'mamm', 'inbe', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco', 'chvf']||outcome
13||For all deaths in hospital, the odds ratio for more than one day's delay relative to one day or less was 1.27 (95% confidence interval 1.23 to 1.32) after adjustment for comorbidity.||['hcro', 'popg', 'menp', 'gngm', 'orgf', 'idcn', 'qlco', 'fndg', 'inbe', 'qnco', 'geoa', 'mnob', 'tmco', 'chvf', 'famg']||outcome
14||The proportion with more than two days' delay ranged from 1.1% to 62.4% between trusts.||['spco', 'dsyn', 'gngm', 'inbe', 'qnco', 'geoa', 'tmco']||outcome
15||If death rates in patients with at most one day's delay had been repeated throughout all 151 trusts in this study, there would have been an average of 581 (478 to 683) fewer total deaths per year (9.4% of the total).||['bpoc', 'acty', 'tmco', 'dsyn', 'podg', 'menp', 'orga', 'orgf', 'qlco', 'fndg', 'inbe', 'qnco', 'geoa', 'ftcn', 'resa', 'gngm']||outcome
16||There was little evidence of an association between delay and emergency readmission.||['cnce', 'ftcn', 'gngm', 'menp', 'topp', 'inpr', 'hlca', 'mamm', 'qnco', 'geoa', 'dora', 'tmco', 'phpr']||outcome
17||CONCLUSIONS.||[]||outcome
18||Delay in operation is associated with an increased risk of death but not readmission after a fractured neck of femur, even with adjustment for comorbidity, and there is wide variation between trusts.||['popg', 'inpo', 'gngm', 'idcn', 'qnco', 'inpr', 'acty', 'spco', 'dsyn', 'mamm', 'geoa', 'ftcn', 'chvf', 'cnce', 'clna', 'topp', 'dora', 'blor', 'bpoc', 'orgf', 'qlco', 'inbe', 'hlca', 'tmco']||outcome
