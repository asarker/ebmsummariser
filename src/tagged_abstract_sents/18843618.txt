1||BACKGROUND.||[]||other
2||Carpal tunnel syndrome results from entrapment of the median nerve in the wrist.||['bpoc', 'spco', 'popg', 'blor', 'dsyn', 'gngm', 'idcn', 'geoa', 'fndg', 'mamm', 'qnco', 'prog', 'ftcn', 'inpr']||background
3||Common symptoms are tingling, numbness, and pain in the hand that may radiate to the forearm or shoulder.||['bpoc', 'blor', 'gngm', 'sosy', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn']||background
4||Most symptomatic cases are treated non-surgically.||['idcn', 'ftcn', 'qnco', 'dsyn', 'topp']||background
5||OBJECTIVES.||[]||other
6||The objective is to compare the efficacy of surgical treatment of carpal tunnel syndrome with non-surgical treatment.||['acty', 'inpr', 'popg', 'dsyn', 'clna', 'gngm', 'topp', 'idcn', 'geoa', 'qnco', 'prog', 'ftcn', 'resa']||other
7||SEARCH STRATEGY.||[]||other
8||We searched the Cochrane Neuromuscular Disease Group Trials Register (January 2008), MEDLINE (January 1966 to January 2008), EMBASE (January 1980 to January 2008) and LILACS (January 1982 to January 2008).||['bpoc', 'acty', 'dsyn', 'topp', 'idcn', 'mamm', 'geoa', 'inpr', 'bsoj']||other
9||We checked bibliographies in papers and contacted authors for information about other published or unpublished studies.||['phsu', 'acty', 'ocac', 'popg', 'dsyn', 'clna', 'fish', 'gngm', 'biof', 'idcn', 'prog', 'bodm', 'aapp', 'qnco', 'inch', 'geoa', 'mnob', 'inpr', 'imft']||other
10||SELECTION CRITERIA.||[]||other
11||We included all randomised and quasi-randomised controlled trials comparing any surgical and any non-surgical therapies.||['spco', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'tmco', 'mamm', 'qnco', 'geoa', 'ftcn', 'resa']||other
12||DATA COLLECTION AND ANALYSIS.||[]||other
13||Two authors independently assessed the eligibility of the trials.||['phsu', 'acty', 'spco', 'medd', 'dsyn', 'qlco', 'gngm', 'idcn', 'hlca', 'mamm', 'prog', 'geoa', 'qnco', 'inch', 'ftcn', 'resa']||other
14||MAIN RESULTS.||[]||other
15||In this update we found four randomised controlled trials involving 317 participants in total.||['resa', 'popg', 'dsyn', 'tmco', 'gngm', 'qlco', 'spco', 'mamm', 'qnco', 'geoa', 'ftcn', 'inpr']||other
16||Three of them including 295 participants, 148 allocated to surgery and 147 to non-surgical treatment reported information on our primary outcome (improvement at three months of follow-up).||['phsu', 'spco', 'popg', 'ftcn', 'dsyn', 'bmod', 'gngm', 'topp', 'idcn', 'qlco', 'bodm', 'aapp', 'qnco', 'geoa', 'biof', 'inpr', 'tmco', 'imft']||outcome
17||The pooled estimate favoured surgery (RR 1.23, 95% CI 1.04 to 1.46).||['cnce', 'dsyn', 'bmod', 'idcn', 'qnco', 'geoa']||outcome
18||Two trials including 245 participants described outcome at six month follow-up, also favouring surgery (RR 1.19, 95% CI 1.02 to 1.39).Two trials reported clinical improvement at one year follow-up.||['bpoc', 'hcro', 'tmco', 'popg', 'inpr', 'dsyn', 'bmod', 'gngm', 'idcn', 'qlco', 'spco', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'resa']||outcome
19||They included 198 patients favouring surgery (RR 1.27, 95% CI 1.05 to 1.53).||['spco', 'dsyn', 'podg', 'bmod', 'gngm', 'idcn', 'qnco', 'geoa']||outcome
20||The only trial describing changes in neurophysiological parameters in both groups also favoured surgery (RR 1.44, 95% CI 1.05 to 1.97).||['resa', 'spco', 'popg', 'medd', 'dsyn', 'bmod', 'gngm', 'idcn', 'fndg', 'mamm', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
21||Two trials described need for surgery during follow-up, including 198 patients.||['resa', 'spco', 'dsyn', 'podg', 'bmod', 'gngm', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'mnob', 'inpr']||outcome
22||The pooled estimate for this outcome indicates that a significant proportion of people treated medically will require surgery while the risk of re-operation in surgically treated people is low (RR 0.04 favouring surgery, 95% CI 0.01 to 0.17).||['spco', 'popg', 'dsyn', 'bmod', 'gngm', 'topp', 'idcn', 'qlco', 'cnce', 'fndg', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
23||Complications of surgery and medical treatment were described by two trials with 226 participants.||['spco', 'popg', 'dsyn', 'bmod', 'gngm', 'idcn', 'mamm', 'qnco', 'geoa', 'ftcn', 'mnob', 'resa']||outcome
24||Although the incidence of complications was high in both groups, they were significantly more common in the surgical arm (RR 1.38, 95% CI 1.08 to 1.76).||['qnco', 'popg', 'blor', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'patf', 'geoa']||outcome
25||AUTHORS' CONCLUSIONS.||[]||other
26||Surgical treatment of carpal tunnel syndrome relieves symptoms significantly better than splinting.||['popg', 'medd', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'geoa', 'qnco', 'prog', 'ftcn']||other
27||Further research is needed to discover whether this conclusion applies to people with mild symptoms and whether surgical treatment is better than steroid injection.||['bpoc', 'acty', 'spco', 'popg', 'dsyn', 'clna', 'gngm', 'topp', 'idcn', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn', 'resa', 'bsoj', 'strd']||other
