1||OBJECTIVE.||[]||other
2||To assess the relations between early introduction of solid food and infant weight, gastrointestinal illness, and allergic illnesses during the first two years of life.||['bpoc', 'acty', 'spco', 'socb', 'clna', 'food', 'orga', 'sosy', 'lbpr', 'qlco', 'fndg', 'qnco', 'aggp', 'geoa', 'idcn', 'hlca', 'ftcn', 'tmco', 'gngm', 'dsyn', 'famg']||background
3||DESIGN.||[]||other
4||Prospective observational study of infants followed up for 24 months after birth.||['resa', 'dsyn', 'clna', 'orgf', 'qlco', 'tmco', 'aggp', 'qnco', 'geoa', 'inpr', 'diap']||other
5||SETTING.||[]||other
6||Community setting in Dundee.||['ftcn', 'inpr', 'menp', 'qnco']||other
7||PATIENTS.||[]||other
8||671 newborn infants, of whom 455 were still available for study at 2 years of age.||['resa', 'fish', 'orga', 'aggp', 'qnco', 'ftcn', 'tmco']||population
9||MAIN OUTCOME MEASURES.||[]||other
10||Infants' diet, weight, and incidence of gastrointestinal illness, respiratory illness, napkin dermatitis, and eczema at 2 weeks and 2, 3, 4, 6, 9, 12, 15, 18, 21, and 24 months of age.||['bpoc', 'spco', 'dsyn', 'food', 'orga', 'sosy', 'lbpr', 'fndg', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco', 'gngm']||other
11||RESULTS.||[]||outcome
12||The infants given solid food at an early age (at &lt; 8 weeks or 8-12 weeks) were heavier than those introduced to solids later (after 12 weeks) at 4, 8, 13, and 26 weeks of age (p &lt; 0.01) but not at 52 and 104 weeks.||['gngm', 'dsyn', 'food', 'orga', 'qlco', 'tmco', 'bodm', 'aggp', 'geoa', 'ftcn', 'inpr']||outcome
13||At their first solid feed those given solids early were heavier than infants of similar age who had not yet received solids.||['gngm', 'dsyn', 'orga', 'qlco', 'tmco', 'bodm', 'aggp', 'geoa', 'qnco', 'hlca', 'ftcn', 'inpr']||outcome
14||The incidence of gastrointestinal illness, wheeze, and nappy dermatitis was not related to early introduction of solids.||['bpoc', 'spco', 'clna', 'dsyn', 'tmco', 'gngm', 'sosy', 'lbpr', 'qlco', 'fndg', 'bodm', 'orch', 'qnco', 'hlca', 'geoa', 'mnob', 'ftcn']||outcome
15||There was a significant but less than twofold increase in respiratory illness at 14-26 weeks of age and persistent cough at 14-26 and 27-39 weeks of age among the infants given solids early.||['spco', 'dsyn', 'orga', 'sosy', 'idcn', 'qlco', 'fndg', 'bodm', 'aggp', 'qnco', 'geoa', 'ftcn', 'tmco', 'gngm']||outcome
16||The incidence of eczema was increased in the infants who received solids at 8-12 weeks of age.||['gngm', 'dsyn', 'orga', 'qlco', 'bodm', 'aggp', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
17||CONCLUSION.||[]||outcome
18||Early introduction of solid food to infants is less harmful than was previously reported.||['tmco', 'dsyn', 'clna', 'food', 'geoa', 'qlco', 'aggp', 'qnco', 'hlca', 'inpr', 'blor']||outcome
19||Longer follow up is needed, but, meanwhile, a more relaxed approach to early feeding with solids should be considered.||['spco', 'hlca', 'dsyn', 'gngm', 'idcn', 'qlco', 'tmco', 'bodm', 'qnco', 'geoa', 'inpr']||outcome
