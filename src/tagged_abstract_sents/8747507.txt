1||OBJECTIVE.||[]||other
2||Macrocytosis is considered to be an important sign of disease, although it often seems to be ignored.||['acty', 'spco', 'blor', 'dsyn', 'gngm', 'topp', 'idcn', 'qlco', 'fndg', 'bacs', 'aapp', 'geoa', 'inpr', 'tmco', 'genf']||background
3||The aim of the present study was to assess the quality of the work done by general practitioners using macrocytosis as an indicator.||['resa', 'acty', 'spco', 'ocac', 'clna', 'ftcn', 'gngm', 'elii', 'topp', 'idcn', 'qlco', 'mamm', 'bacs', 'aapp', 'qnco', 'prog', 'geoa', 'tmco', 'bird', 'dsyn', 'blor']||other
4||METHODS.||[]||other
5||All consecutive outpatients at Tampere City Health Center who had blood counts taken during 8 months in 1990 and were found to have macrocytosis (MCV &gt; or = 100 femtoliters) that had not earlier been examined, were included in this study.||['dsyn', 'gngm', 'idcn', 'bacs', 'aapp', 'qnco', 'resa', 'acty', 'spco', 'blor', 'fndg', 'geoa', 'ftcn', 'cnce', 'topp', 'bpoc', 'podg', 'lbpr', 'qlco', 'tisu', 'orch', 'hlca', 'tmco']||population
6||The further examinations undertaken were analyzed.||['bpoc', 'acty', 'cnce', 'dsyn', 'gngm', 'elii', 'geoa', 'spco', 'qnco', 'hlca', 'resa', 'genf', 'orgt']||other
7||RESULTS.||[]||other
8||Of the 9,527 blood counts, previously unexamined macrocytosis was found in 3% (n = 287, 154 men and 133 women).||['bpoc', 'acty', 'tmco', 'popg', 'blor', 'dsyn', 'gngm', 'topp', 'lbpr', 'qlco', 'fndg', 'tisu', 'aapp', 'qnco', 'hlca', 'geoa', 'inpr', 'orch', 'bacs']||outcome
9||Further examination because of macrocytosis was undertaken in 65 (42%) men and in 48 (36%) women.||['acty', 'cnce', 'popg', 'blor', 'dsyn', 'gngm', 'elii', 'topp', 'geoa', 'bacs', 'aapp', 'qnco', 'hlca', 'ftcn', 'genf', 'orgt']||outcome
10||Alcohol abuse was the most common disease present in 52 (80%) men and in 22 (46%) women.||['inpr', 'popg', 'blor', 'dsyn', 'fish', 'gngm', 'mobd', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
11||No cause for macrocytosis was found in seven men and in 17 women.||['cnce', 'popg', 'blor', 'dsyn', 'gngm', 'topp', 'bacs', 'aapp', 'qnco', 'geoa', 'ftcn']||outcome
12||No difference between the examined and unexamined groups was found concerning medical history, medications, symptoms, or blood count values, including hemoglobin and MCV.||['phsu', 'acty', 'popg', 'ftcn', 'dsyn', 'ocdi', 'chvs', 'gngm', 'elii', 'lbpr', 'qlco', 'fndg', 'tisu', 'orch', 'qnco', 'hlca', 'geoa', 'idcn']||outcome
13||CONCLUSIONS.||[]||outcome
14||Evaluation of macrocytosis, when undertaken, was well done by general practitioners.||['acty', 'cnce', 'dsyn', 'clna', 'gngm', 'elii', 'topp', 'qlco', 'spco', 'bacs', 'aapp', 'qnco', 'prog', 'tmco', 'genf', 'blor']||outcome
15||However, it was performed too seldom and, thus, several diseases, especially alcohol abuse, may have been overlooked.||['phsu', 'spco', 'dsyn', 'fish', 'gngm', 'mobd', 'qlco', 'tmco', 'bodm', 'orch', 'ftcn', 'geoa', 'mamm', 'inpr']||outcome
