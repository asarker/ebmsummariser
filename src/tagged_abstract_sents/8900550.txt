1||OBJECTIVES.||[]||other
2||This study evaluated different symptoms, signs, and blood tests in the diagnostic process of patients with a clinical diagnosis of acute sinusitis.||['bpoc', 'hcro', 'resa', 'dsyn', 'podg', 'lbpr', 'qlco', 'fndg', 'anab', 'bacs', 'aapp', 'geoa', 'ftcn', 'mnob', 'tmco']||background
3||METHODS.||[]||other
4||A total of 201 primary care patients in southern Norway aged 15 years or older with a clinical diagnosis of acute sinusitis were evaluated.||['hcro', 'spco', 'hlca', 'dsyn', 'podg', 'orga', 'qlco', 'fndg', 'anab', 'geoa', 'ftcn', 'mnob', 'tmco']||population
5||Computed tomography (CT) was used as a reference standard to divide the patients into two groups: one with and one without confirmed sinusitis.||['dsyn', 'podg', 'idcn', 'qlco', 'anab', 'qnco', 'geoa', 'ftcn']||other
6||Fluid level or total opacification of any sinus on CT were used as hallmarks of confirmed sinusitis.||['dsyn', 'gngm', 'idcn', 'qlco', 'anab', 'ftcn']||other
7||Blood tests that included erythrocyte sedimentation rate (ESR), C-reactive protein, and white blood count were taken.||['acty', 'cnce', 'popg', 'clna', 'dsyn', 'lbpr', 'fndg', 'bacs', 'aapp', 'idcn', 'geoa', 'ftcn']||other
8||The patients were evaluated in a standardized way for the medical history and the clinical investigation.||['hcro', 'dsyn', 'ocdi', 'podg', 'geoa', 'qlco', 'fndg', 'hlca', 'ftcn', 'mnob']||other
9||RESULTS.||[]||outcome
10||A total of 127 (63%) patients had fluid level or total opacification in one or more sinus regions.||['podg', 'gngm', 'bdsu', 'qlco', 'anab', 'qnco']||outcome
11||"Double sickening," purulent rhinorrhoea, purulent secretion in cavum nasi, and ESR &gt; 10 had the highest likelihood ratios and were independently associated with acute sinusitis.||['spco', 'biof', 'clna', 'dsyn', 'gngm', 'sosy', 'medd', 'idcn', 'qlco', 'anab', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
12||CONCLUSIONS.||[]||other
13||This study confirms the uncertainty of the clinical diagnosis of acute sinusitis in primary care, based on the clinical evaluation alone.||['hcro', 'resa', 'hlca', 'dsyn', 'idcn', 'qlco', 'fndg', 'anab', 'qnco', 'geoa', 'ftcn', 'mnob', 'tmco', 'chvf']||outcome
14||Only four symptoms and signs had a high likelihood ratio and were independently associated with acute sinusitis.||['spco', 'dsyn', 'medd', 'gngm', 'idcn', 'qlco', 'fndg', 'anab', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
15||A combination of at least three of these four symptoms and signs gave a specificity of .81 and a sensitivity of .66.||['gngm', 'qlco', 'fndg', 'qnco', 'geoa', 'ftcn']||outcome
