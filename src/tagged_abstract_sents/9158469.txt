1||OBJECTIVE.||[]||other
2||To compare the efficacy of physiotherapy, manipulation, and corticosteroid injection for treating patients with shoulder complaints in general practice.||['phsu', 'acty', 'spco', 'blor', 'clna', 'podg', 'menp', 'bmod', 'orga', 'horm', 'topp', 'fndg', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco', 'resa', 'strd']||background
3||DESIGN.||[]||background
4||Randomised, single blind study.||['resa', 'popg', 'dsyn', 'qnco']||other
5||SETTING.||[]||other
6||Seven general practices in the Netherlands.||['spco', 'clna', 'bmod', 'menp', 'geoa', 'tmco']||other
7||SUBJECTS.||[]||other
8||198 patients with shoulder complaints, of whom 172 were divided, on the basis of physical examination, into two diagnostic groups: a shoulder girdle group (n = 58) and a synovial group (n = 114).||['bpoc', 'blor', 'dsyn', 'podg', 'idcn', 'hlca', 'fndg', 'qnco', 'geoa', 'ftcn']||population
9||INTERVENTIONS.||[]||other
10||Patients in the shoulder girdle group were randomised to manipulation or physiotherapy, and patients in the synovial group were randomised to corticosteroid injection, manipulation, or physiotherapy.||['bpoc', 'phsu', 'resa', 'blor', 'dsyn', 'podg', 'orga', 'horm', 'clna', 'topp', 'idcn', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco', 'strd']||intervention
11||MAIN OUTCOME MEASURES.||[]||other
12||Duration of shoulder complaints analysed by survival analysis.||['acty', 'blor', 'dsyn', 'qnco', 'lbpr', 'fndg', 'idcn']||other
13||RESULTS.||[]||outcome
14||In the shoulder girdle group duration of complaints was significantly shorter after manipulation compared with physiotherapy (P &lt; 0.001).||['bpoc', 'acty', 'blor', 'dsyn', 'orga', 'topp', 'idcn', 'fndg', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
15||Also the number of patients reporting treatment failure was less with manipulation.||['popg', 'podg', 'orga', 'topp', 'geoa', 'qnco', 'hlca', 'ftcn', 'inpr']||outcome
16||In the synovial group duration of complaints was shortest after corticosteroid injection compared with manipulation and physiotherapy (P &lt; 0.001).||['phsu', 'acty', 'clna', 'dsyn', 'lbpr', 'orga', 'horm', 'bpoc', 'topp', 'idcn', 'fndg', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco', 'strd']||outcome
17||Drop out due to treatment failure was low in the injection group (17%) and high in the manipulation group (59%) and physiotherapy group (51%).||['popg', 'clna', 'orga', 'topp', 'idcn', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
18||CONCLUSIONS.||[]||other
19||For treating shoulder girdle disorders, manipulation seems to be the preferred treatment.||['bpoc', 'acty', 'inpr', 'popg', 'blor', 'dsyn', 'orga', 'clna', 'topp', 'idcn', 'geoa', 'ftcn', 'tmco']||outcome
20||For the synovial disorders, corticosteroid injection seems the best treatment.||['phsu', 'acty', 'popg', 'clna', 'dsyn', 'horm', 'bpoc', 'qlco', 'bodm', 'qnco', 'geoa', 'ftcn', 'inpr', 'strd']||outcome
