1||OBJECTIVE.||[]||other
2||To evaluate the ability of a novel topical antimicrobial gel containing cetrimide, bacitracin, and polymyxin B sulfate to prevent infections of minor wounds.||['popg', 'inpo', 'fish', 'gngm', 'elii', 'idcn', 'bacs', 'aapp', 'qnco', 'evnt', 'phsu', 'acty', 'spco', 'dsyn', 'mamm', 'geoa', 'ftcn', 'antb', 'clna', 'inch', 'bodm', 'orga', 'lbpr', 'qlco', 'orch', 'tmco']||background
3||DESIGN.||[]||other
4||A clinical trial compared the test preparation with placebo and a povidone iodine antiseptic cream.||['phsu', 'acty', 'hcro', 'tmco', 'dsyn', 'food', 'gngm', 'elii', 'topp', 'lbpr', 'qlco', 'fndg', 'mamm', 'spco', 'geoa', 'qnco', 'hlca', 'inpr', 'mnob', 'resa']||other
5||SETTING.||[]||other
6||Five primary schools in Sydney, Australia, participated in the study over a 6-week spring/summer school term.||['spco', 'dsyn', 'gngm', 'idcn', 'qlco', 'tmco', 'mamm', 'qnco', 'geoa', 'mnob', 'resa', 'orgt']||other
7||SUBJECTS.||[]||other
8||Children aged 5-12 years with parental consent were eligible for study participation.||['bpoc', 'spco', 'dsyn', 'orga', 'idcn', 'qlco', 'tmco', 'geoa', 'qnco', 'prog', 'ftcn', 'resa', 'gngm', 'famg']||population
9||Accidental injuries occurring at school were treated in a standardized manner by nurses at each site.||['spco', 'dsyn', 'clna', 'orga', 'orgf', 'topp', 'qlco', 'cnce', 'fndg', 'qnco', 'prog', 'ftcn', 'inpo', 'gngm', 'mnob', 'orgt']||other
10||OUTCOME MEASURES.||[]||other
11||Wounds were evaluated by the medical practitioner after 3 days of topical treatment.||['acty', 'spco', 'popg', 'dsyn', 'clna', 'gngm', 'qlco', 'geoa', 'qnco', 'prog', 'ftcn', 'tmco']||other
12||The clinical outcome was classified as resolution or suspected infection.||['hcro', 'clna', 'dsyn', 'idcn', 'qlco', 'sbst', 'qnco', 'ftcn', 'mnob', 'inpr', 'clas']||other
13||If a clinical infection was suspected, the injury was swabbed for microbiologic evaluation.||['idcn', 'hcro', 'dsyn', 'clna', 'fish', 'lbpr', 'qlco', 'grup', 'qnco', 'geoa', 'ftcn', 'inpo', 'inpr', 'mnob', 'bmod']||other
14||Growth of a dominant microorganism was classified as a microbiologic infection.||['bpoc', 'dsyn', 'clna', 'orgm', 'bmod', 'gngm', 'lbpr', 'qlco', 'geoa', 'qnco', 'prog', 'ftcn', 'inpr', 'clas']||other
15||RESULTS.||[]||other
16||Of the 177 injuries treated, there were nine clinical infections.||['hcro', 'dsyn', 'clna', 'gngm', 'elii', 'qlco', 'qnco', 'geoa', 'ftcn', 'inpo', 'inpr', 'mnob']||outcome
17||A comparison of these showed a significant difference among treatment groups (p &lt; 0.05).||['acty', 'popg', 'dsyn', 'gngm', 'idcn', 'qlco', 'fndg', 'geoa', 'ftcn', 'inpr']||outcome
18||This difference was associated with the test preparation and placebo; the test preparation reduced the incidence of clinical infection from 12.5% to 1.6% (p &lt; 0.05; 95% CI, 0.011 to 0.207).||['phsu', 'hcro', 'spco', 'clna', 'dsyn', 'gngm', 'topp', 'lbpr', 'qlco', 'fndg', 'mamm', 'orch', 'qnco', 'hlca', 'geoa', 'inpr', 'tmco', 'mnob']||outcome
19||A comparison of microbiologic infections showed no significant differences among treatment groups (p &gt; 0.05).||['qnco', 'acty', 'popg', 'dsyn', 'clna', 'bmod', 'gngm', 'elii', 'lbpr', 'qlco', 'fndg', 'idcn', 'geoa', 'ftcn', 'inpr']||outcome
20||CONCLUSIONS.||[]||other
21||The novel gel preparation containing cetrimide, bacitracin, and polymyxin B sulfate showed therapeutic action and reduced the incidence of clinical infections in minor accidental wounds.||['popg', 'inpo', 'gngm', 'elii', 'idcn', 'bacs', 'aapp', 'qnco', 'inpr', 'antb', 'phsu', 'acty', 'dsyn', 'fndg', 'geoa', 'ftcn', 'hcro', 'clna', 'cgab', 'topp', 'inch', 'bodm', 'mnob', 'orga', 'qlco', 'orch', 'hlca', 'tmco', 'phpr']||outcome
22||It may be a suitable product for first aid prophylaxis.||['bpoc', 'gngm', 'dsyn', 'clna', 'fish', 'orga', 'topp', 'idcn', 'geoa', 'qnco', 'enty', 'mnob', 'inpr']||outcome
