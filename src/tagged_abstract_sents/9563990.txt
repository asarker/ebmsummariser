1||OBJECTIVES.||[]||other
2||To investigate the claim that 90% of episodes of low back pain that present to general practice have resolved within one month.||['acty', 'spco', 'dsyn', 'clna', 'bmod', 'gngm', 'menp', 'sosy', 'idcn', 'qlco', 'fndg', 'mobd', 'geoa', 'qnco', 'inch', 'ftcn', 'cnce', 'tmco', 'blor']||background
3||DESIGN.||[]||other
4||Prospective study of all adults consulting in general practice because of low back pain over 12 months with follow up at 1 week, 3 months, and 12 months after consultation.||['dsyn', 'bmod', 'gngm', 'elii', 'sosy', 'idcn', 'qnco', 'inpr', 'resa', 'acty', 'spco', 'blor', 'menp', 'fndg', 'geoa', 'ftcn', 'cnce', 'clna', 'inch', 'mobd', 'qlco', 'aggp', 'hlca', 'tmco']||population
5||SETTING.||[]||other
6||Two general practices in south Manchester.||['acty', 'spco', 'blor', 'clna', 'bmod', 'gngm', 'menp', 'mobd', 'geoa', 'orch', 'qnco', 'inch', 'tmco']||other
7||490 subjects (203 men, 287 women) aged 18-75 years.||['bpoc', 'gngm', 'popg', 'clna', 'dsyn', 'orga', 'idcn', 'grup', 'geoa', 'tmco']||other
8||MAIN OUTCOME MEASURES.||[]||other
9||Proportion of patients who have ceased to consult with low back pain after 3 months; proportion of patients who are free of pain and back related disability at 3 and 12 months.||['bpoc', 'orga', 'orch', 'dsyn', 'podg', 'fish', 'gngm', 'elii', 'sosy', 'idcn', 'qlco', 'fndg', 'spco', 'geoa', 'qnco', 'hlca', 'tmco', 'inpr', 'genf', 'blor']||other
10||RESULTS.||[]||other
11||Annual cumulative consultation rate among adults in the practices was 6.4%.||['acty', 'dsyn', 'clna', 'qlco', 'gngm', 'menp', 'elii', 'mobd', 'idcn', 'hlca', 'mamm', 'aggp', 'geoa', 'qnco', 'inch', 'inpr']||outcome
12||Of the 463 patients who consulted with a new episode of low back pain, 275 (59%) had only a single consultation, and 150 (32%) had repeat consultations confined to the 3 months after initial consultation.||['bpoc', 'popg', 'blor', 'dsyn', 'podg', 'gngm', 'elii', 'sosy', 'idcn', 'qlco', 'fndg', 'mamm', 'geoa', 'qnco', 'hlca', 'ftcn', 'tmco', 'clna']||outcome
13||However, of those interviewed at 3 and 12 months follow up, only 39/188 (21%) and 42/170 (25%) respectively had completely recovered in terms of pain and disability.||['acty', 'orga', 'dsyn', 'clna', 'socb', 'fish', 'gngm', 'sosy', 'medd', 'inpr', 'qlco', 'fndg', 'spco', 'qnco', 'geoa', 'ftcn', 'tmco', 'blor']||outcome
14||CONCLUSIONS.||[]||other
15||The results are consistent with the interpretation that 90% of patients with low back pain in primary care will have stopped consulting with symptoms within three months.||['bpoc', 'tmco', 'blor', 'dsyn', 'podg', 'gngm', 'elii', 'sosy', 'medd', 'idcn', 'qlco', 'fndg', 'geoa', 'qnco', 'hlca', 'ftcn', 'inpr']||outcome
16||However most will still be experiencing low back pain and related disability one year after consultation.||['bpoc', 'orga', 'blor', 'dsyn', 'fish', 'gngm', 'elii', 'sosy', 'idcn', 'qlco', 'fndg', 'orch', 'qnco', 'hlca', 'geoa', 'tmco', 'inpr']||outcome
