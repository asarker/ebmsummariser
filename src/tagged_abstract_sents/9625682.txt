1||BACKGROUND.||[]||other
2||Empirical scores, computerized ST-segment measurements, and equations have been proposed as tools for improving the diagnostic performance of the exercise test.||['idcn', 'spco', 'popg', 'dsyn', 'bodm', 'orga', 'lbpr', 'qlco', 'fndg', 'mamm', 'dora', 'inbe', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr', 'gngm', 'diap']||background
3||OBJECTIVE.||[]||other
4||To compare the diagnostic utility of these scores, measurements, and equations with that of visual ST-segment measurements in patients with reduced workup bias.||['phsu', 'acty', 'spco', 'popg', 'dsyn', 'ocac', 'podg', 'gngm', 'idcn', 'qlco', 'fndg', 'mamm', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr']||background
5||DESIGN.||[]||other
6||Prospective analysis.||['bpoc', 'lbpr', 'clna', 'qnco']||other
7||SETTING.||[]||other
8||12 university-affiliated Veterans Affairs Medical Centers.||['dsyn', 'gngm', 'qlco', 'mamm', 'sbst', 'mnob', 'tmco', 'orgt']||other
9||PATIENTS.||[]||other
10||814 consecutive patients who presented with angina pectoris and agreed to undergo both exercise testing and coronary angiography.||['bpoc', 'lbpr', 'clna', 'dsyn', 'podg', 'gngm', 'sosy', 'idcn', 'qlco', 'dora', 'qnco', 'geoa', 'ftcn', 'tmco', 'diap']||population
11||MEASUREMENTS.||[]||other
12||Digital electrocardiographic recorders and angiographic calipers were used for testing at each site, and test results were sent to core laboratories.||['bpoc', 'lbpr', 'spco', 'clna', 'dsyn', 'fish', 'gngm', 'orgf', 'medd', 'idcn', 'bacs', 'aapp', 'qnco', 'ftcn', 'mnob', 'inpr', 'orgt']||other
13||RESULTS.||[]||other
14||Although 25% of patients had previously had testing, workup bias was reduced, as shown by comparison with a pilot study group.||['resa', 'acty', 'inpr', 'popg', 'dsyn', 'ocac', 'podg', 'lbpr', 'gngm', 'idcn', 'qlco', 'mamm', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'phsu']||outcome
15||This reduction resulted in a sensitivity of 45% and a specificity of 85% for visual analysis.||['bpoc', 'idcn', 'clna', 'dsyn', 'gngm', 'lbpr', 'qlco', 'qnco', 'orch', 'npop', 'geoa', 'ftcn', 'inpr', 'phsu']||outcome
16||Computerized measurements and visual analysis had similar diagnostic power.||['bpoc', 'idcn', 'popg', 'dsyn', 'hcpp', 'gngm', 'lbpr', 'qlco', 'fndg', 'mamm', 'qnco', 'ftcn', 'inpr']||outcome
17||Equations incorporating nonelectrocardiographic variables and either visual or computerized ST-segment measurement had similar discrimination and were superior to single ST-segment measurements.||['inpr', 'acty', 'spco', 'popg', 'clna', 'dsyn', 'fish', 'gngm', 'menp', 'idcn', 'qlco', 'orga', 'bacs', 'aapp', 'qnco', 'geoa', 'mamm', 'mnob', 'ftcn']||outcome
18||These equations correctly classified 5 more patients of every 100 tested (areas under the receiver-operating characteristic curve, 0.80 for equations and 0.68 for visual analysis; P &lt; 0.001) in this population with a 50% prevalence of disease.||['bpoc', 'acty', 'spco', 'dsyn', 'clna', 'podg', 'tmco', 'gngm', 'menp', 'lbpr', 'qlco', 'qnco', 'idcn', 'geoa', 'ftcn', 'inpr', 'clas']||outcome
19||CONCLUSIONS.||[]||other
20||Standard exercise tests had lower sensitivity but higher specificity in this population with reduced work-up bias than in previous studies.||['phsu', 'lbpr', 'tmco', 'dsyn', 'ocac', 'ftcn', 'gngm', 'idcn', 'qlco', 'dora', 'orch', 'qnco', 'geoa', 'mamm', 'inpr', 'diap']||outcome
21||Computerized ST-segment measurements were similar to visual ST-segment measurements made by cardiologists.||['popg', 'dsyn', 'gngm', 'idcn', 'qlco', 'prog', 'qnco', 'geoa', 'ftcn', 'inpr']||outcome
22||Considering more than ST-segment measurements can enhance the diagnostic power of the exercise test.||['idcn', 'acty', 'spco', 'popg', 'ftcn', 'dsyn', 'qnco', 'lbpr', 'fndg', 'mamm', 'hcpp', 'geoa', 'dora', 'gngm', 'diap']||outcome
