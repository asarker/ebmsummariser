1||INTRODUCTION.||[]||other
2||The Parkinson's Disease Research Group of the United Kingdom (PDRG-UK) reported increased mortality in PD patients treated with levodopa plus selegiline compared with those treated with levodopa alone.||['phsu', 'acty', 'spco', 'aapp', 'dsyn', 'podg', 'gngm', 'idcn', 'fndg', 'nsba', 'orch', 'qnco', 'geoa', 'ftcn', 'inpr', 'bsoj', 'blor']||background
3||METHODS.||[]||background
4||We performed a meta-analysis on five long-term, prospective, randomized trials of selegiline in patients with untreated PD.||['bpoc', 'phsu', 'spco', 'resa', 'clna', 'dsyn', 'podg', 'euka', 'gngm', 'lbpr', 'qlco', 'bodm', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'mamm', 'blor']||other
5||Included in the analysis were four randomized, double-blind, placebo-controlled studies and one randomized, double-blind, placebo-controlled study of 2 years' duration followed by long-term, open follow-up.||['bpoc', 'inpr', 'resa', 'dsyn', 'gngm', 'topp', 'lbpr', 'qlco', 'spco', 'qnco', 'geoa', 'ftcn', 'tmco']||other
6||RESULTS.||[]||other
7||The mean duration of follow-up was 4.1 +/- 1.8 years.||['inpr', 'dsyn', 'qlco', 'qnco', 'geoa', 'tmco']||outcome
8||There were 14 deaths in 297 selegiline-treated patients (4.7%) and 17 deaths in 292 non-selegiline-treated patients (5.8%).||['phsu', 'spco', 'blor', 'dsyn', 'podg', 'gngm', 'orgf', 'fndg', 'orch', 'qnco', 'geoa', 'ftcn']||outcome
9||The hazard ratio for mortality was 1.02 (95% CI 0.44 to 2.37; p = 0.96).||['geoa', 'qnco']||outcome
10||An analysis restricted to patients receiving only levodopa with or without selegiline noted 11 deaths in 257 levodopa/selegiline-treated patients (4.3%) and 11 deaths in 254 patients treated with levodopa alone (4.3%).||['phsu', 'bpoc', 'spco', 'clna', 'dsyn', 'podg', 'gngm', 'orgf', 'lbpr', 'fndg', 'dora', 'aapp', 'qnco', 'geoa', 'ftcn', 'inpr', 'orch', 'nsba', 'blor']||outcome
11||The hazard ratio was 1.06 (95% CI 0.44 to 2.55; p = 0.90).||['qnco']||outcome
12||Death rate per 1,000 patient years was 11.4 in the selegiline group and 14.2 in the nonselegiline group.||['phsu', 'acty', 'spco', 'popg', 'blor', 'gngm', 'idcn', 'orch', 'qnco', 'geoa', 'ftcn', 'tmco', 'clas']||outcome
13||Kaplan-Meier survival curves reflecting pooled survival data showed no significant difference in duration of survival.||['acty', 'cnce', 'clna', 'dsyn', 'gngm', 'menp', 'idcn', 'qlco', 'fndg', 'spco', 'qnco', 'geoa', 'inpr', 'tmco']||outcome
14||The hazard ratio was 0.84 (95% CI 0.41 to 1.70; p = 0.63) for selegiline- versus non-selegiline-treated patients and 1.05 (95% CI 0.46 to 2.43; p = 0.91) for selegiline/levodopa- versus levodopa-treated patients.||['phsu', 'spco', 'aapp', 'dsyn', 'podg', 'gngm', 'idcn', 'orch', 'qnco', 'geoa', 'ftcn', 'nsba', 'blor']||outcome
15||CONCLUSION.||[]||other
16||These results contrast with those of the PDRG-UK study and demonstrate no increase in mortality associated with selegiline treatment whether or not patients also received levodopa.||['phsu', 'acty', 'spco', 'popg', 'aapp', 'dsyn', 'podg', 'gngm', 'idcn', 'qlco', 'mamm', 'orch', 'qnco', 'irda', 'geoa', 'resa', 'ftcn', 'nsba', 'blor']||outcome
