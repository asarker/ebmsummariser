1||OBJECTIVE.||[]||other
2||To document changes in the prevalence of resistance of Propionibacterium acnes to antibiotics used for treating acne.||['acty', 'popg', 'dsyn', 'clna', 'cgab', 'orgf', 'gngm', 'menp', 'topp', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'clas', 'antb']||background
3||DATA SOURCES.||[]||other
4||MEDLINE and EMBASE were searched for publications on P. acnes resistance to systemic antibiotics.||['bpoc', 'acty', 'dsyn', 'cgab', 'chvs', 'gngm', 'menp', 'elii', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'mnob', 'inpr', 'bsoj', 'antb']||other
5||The search strategy mapped "acne" or "acne vulgaris" with the terms "antibiotic resistance" or "drug resistance, microbial".||['phsu', 'acty', 'biof', 'dsyn', 'orgm', 'qnco', 'menp', 'bpoc', 'lbpr', 'qlco', 'mamm', 'geoa', 'idcn', 'prog', 'tmco', 'bsoj', 'antb']||other
6||Only papers published in English during 1976 to 1997 were included in the search.||['bpoc', 'spco', 'ocac', 'dsyn', 'gngm', 'idcn', 'geoa', 'mnob', 'bsoj']||other
7||STUDY SELECTION.||[]||other
8||53 publications met the search criteria.||['bpoc', 'chvs', 'gngm', 'elii', 'idcn', 'mamm', 'qnco', 'geoa', 'mnob', 'inpr', 'bsoj']||other
9||The search output was refined by selecting papers that specifically addressed P. acnes resistance patterns.||['bpoc', 'acty', 'spco', 'dsyn', 'clna', 'gngm', 'menp', 'idcn', 'qlco', 'cnce', 'bacs', 'aapp', 'qnco', 'prog', 'geoa', 'mnob', 'ftcn', 'bsoj']||other
10||Additional studies (not included in the search output) were identified from review articles and references of the retrieved articles.||['bpoc', 'acty', 'cnce', 'dsyn', 'gngm', 'idcn', 'qlco', 'spco', 'qnco', 'geoa', 'ftcn', 'inpr', 'bsoj']||other
11||Twelve articles were reviewed.||['qlco', 'inpr', 'spco', 'dsyn', 'qnco']||other
12||DATA EXTRACTION.||[]||other
13||Data on the prevalence of antibiotic-resistant propionibacteria, the incidence of individual resistance phenotypes, mixed resistance, and correlation between poor therapeutic response and resistant propionibacteria were extracted.||['bpoc', 'acty', 'inpr', 'popg', 'dsyn', 'clna', 'famg', 'grpa', 'orga', 'menp', 'topp', 'idcn', 'qlco', 'bact', 'mamm', 'qnco', 'geoa', 'ftcn', 'tmco', 'gngm', 'antb']||other
14||DATA SYNTHESIS.||[]||other
15||Research since 1978 has suggested an association between poor therapeutic response and antibiotic-resistant propionibacteria.||['bpoc', 'acty', 'dsyn', 'clna', 'grpa', 'gngm', 'menp', 'topp', 'idcn', 'qlco', 'bact', 'mamm', 'qnco', 'geoa', 'ftcn', 'tmco', 'bsoj', 'antb']||outcome
16||The overall incidence of P. acnes antibiotic resistance has increased from 20% in 1978 to 62% in 1996.||['spco', 'dsyn', 'gngm', 'menp', 'inpr', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'antb']||outcome
17||Resistance to specific antibiotics varied and was most commonly reported with erythromycin and clindamycin, tetracycline and doxcycline, and trimethoprim.||['spco', 'blor', 'dsyn', 'cgab', 'gngm', 'inpr', 'qlco', 'orch', 'qnco', 'carb', 'geoa', 'ftcn', 'antb']||outcome
18||Resistance to minocycline is rare.||['tmco', 'topp', 'spco', 'orch', 'geoa', 'inpr', 'antb']||outcome
19||CONCLUSIONS.||[]||other
20||In many patients with acne, continued treatment with antibiotics can be inappropriate or ineffective.||['bpoc', 'gngm', 'popg', 'dsyn', 'clna', 'cgab', 'podg', 'orga', 'idcn', 'qlco', 'mamm', 'qnco', 'geoa', 'ftcn', 'antb']||outcome
21||It is important to recognise therapeutic failure and alter treatment accordingly.||['bpoc', 'popg', 'dsyn', 'cgab', 'gngm', 'topp', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
22||The use of long-term rotational antibiotics is outdated and will only exacerbate antibiotic resistance.||['spco', 'dsyn', 'cgab', 'menp', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco', 'antb']||outcome
