1||OBJECTIVE.||[]||other
2||To compare the effectiveness of biofeedback/relaxation, exercise, and a combined program for the treatment of fibromyalgia.||['acty', 'cnce', 'popg', 'clna', 'dsyn', 'gngm', 'topp', 'qlco', 'dora', 'geoa', 'ftcn', 'mnob']||background
3||METHODS.||[]||other
4||Subjects (n = 119) were randomly assigned to one of 4 groups: 1) biofeedback/relaxation training, 2) exercise training, 3) a combination treatment, or 4) an educational/attention control program.||['phsu', 'acty', 'cnce', 'popg', 'clna', 'dsyn', 'edac', 'gngm', 'menp', 'topp', 'idcn', 'qlco', 'fndg', 'dora', 'orch', 'qnco', 'ftcn', 'resa', 'inpr']||intervention
5||RESULTS.||[]||other
6||All 3 treatment groups produced improvements in self-efficacy for function relative to the control condition.||['popg', 'clna', 'dsyn', 'idcn', 'qlco', 'phsf', 'geoa', 'ftcn', 'resa', 'famg']||outcome
7||In addition, all treatment groups were significantly different from the control group on tender point index scores, reflecting a modest deterioration by the attention control group rather than improvements by the treatment groups.||['spco', 'popg', 'dsyn', 'clna', 'menp', 'idcn', 'qlco', 'fndg', 'grup', 'qnco', 'geoa', 'ftcn', 'inpr', 'phpr']||outcome
8||The exercise and combination groups also resulted in modest improvements on a physical activity measure.||['acty', 'popg', 'clna', 'dsyn', 'gngm', 'idcn', 'qlco', 'dora', 'qnco', 'hlca', 'ftcn']||outcome
9||The combination group best maintained benefits across the 2-year period.||['dsyn', 'idcn', 'qlco', 'qnco', 'geoa', 'ftcn', 'tmco']||outcome
10||CONCLUSION.||[]||other
11||This study demonstrates that these 3 treatment interventions result in improved self-efficacy for physical function which was best maintained by the combination group.||['acty', 'popg', 'clna', 'dsyn', 'gngm', 'hlca', 'idcn', 'qlco', 'phsf', 'geoa', 'ftcn', 'resa']||outcome
